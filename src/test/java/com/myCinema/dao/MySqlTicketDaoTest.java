package com.myCinema.dao;

import com.myCinema.dao.ticketDao.MySqlTicketDao;
import com.myCinema.dao.ticketDao.TicketDao;
import com.myCinema.model.Ticket;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MySqlTicketDaoTest {

    @Mock
    Connection mockConn;
    @Mock
    PreparedStatement mockPreparedStmt;
    @Mock
    ResultSet mockResultSet;

    @Before
    public void setUp() throws SQLException {
        when(mockConn.prepareStatement(anyString())).thenReturn(mockPreparedStmt);
        doNothing().when(mockPreparedStmt).setInt(anyInt(), anyInt());
        when(mockPreparedStmt.executeUpdate()).thenReturn(1);
        when(mockPreparedStmt.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(Boolean.FALSE);
    }

    @Test
    public void testInsertTicket() throws SQLException {
        TicketDao dao = new MySqlTicketDao();
        dao.insertTicket(mockConn, new Ticket(1, 4, 55));

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(3)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).executeUpdate();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testFindTicketsByAccountId() throws SQLException {
        TicketDao dao = new MySqlTicketDao();
        dao.findTicketsByAccountId(mockConn, 1);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).executeQuery();
        verify(mockResultSet).next();
        verify(mockResultSet, times(1)).close();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testFindTicketsBySessionId() throws SQLException {
        TicketDao dao = new MySqlTicketDao();
        dao.findTicketsBySessionId(mockConn, 1);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).executeQuery();
        verify(mockResultSet).next();
        verify(mockResultSet, times(1)).close();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test (expected = SQLException.class)
    public void testThrowsExceptionWhenInsertTicket() throws SQLException {
        when(mockPreparedStmt.executeUpdate()).thenThrow(new SQLException());

        TicketDao dao = new MySqlTicketDao();
        dao.insertTicket(mockConn, new Ticket(1, 4, 55));
    }

    @Test (expected = SQLException.class)
    public void testThrowsExceptionWhenFindTicketByAccountId() throws SQLException {
        when(mockPreparedStmt.executeQuery()).thenThrow(new SQLException());

        TicketDao dao = new MySqlTicketDao();
        dao.findTicketsByAccountId(mockConn, 1);
    }

    @Test (expected = SQLException.class)
    public void testThrowsExceptionWhenFindTicketBySessionId() throws SQLException {
        when(mockPreparedStmt.executeQuery()).thenThrow(new SQLException());

        TicketDao dao = new MySqlTicketDao();
        dao.findTicketsBySessionId(mockConn, 1);
    }
}
