package com.myCinema.dao;

import com.myCinema.dao.movieDao.MovieDao;
import com.myCinema.dao.movieDao.MySqlMovieDao;
import com.myCinema.model.Movie;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MySqlMovieDaoTest {

    Movie m = new Movie(1, "Aaa", "poster1.png", "dgenre",
            "russian", "USA", 130, "", "");

    @Mock
    Connection mockConn;
    @Mock
    PreparedStatement mockPreparedStmt;
    @Mock
    ResultSet mockResultSet;

    @Before
    public void setUp() throws SQLException {
        when(mockConn.prepareStatement(anyString())).thenReturn(mockPreparedStmt);
        doNothing().when(mockPreparedStmt).setInt(anyInt(), anyInt());
        doNothing().when(mockPreparedStmt).setString(anyInt(), anyString());
        when(mockPreparedStmt.executeUpdate()).thenReturn(1);
        when(mockPreparedStmt.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(Boolean.FALSE);
    }

    @Test
    public void testFindAllMovies() throws SQLException {
        MovieDao dao = new MySqlMovieDao();
        dao.findAllMovies(mockConn);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).executeQuery();
        verify(mockResultSet, times(1)).next();
        verify(mockResultSet, times(1)).close();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testFindMovieById() throws SQLException {
        MovieDao dao = new MySqlMovieDao();
        dao.findMovieById(mockConn, 1);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).executeQuery();
        verify(mockResultSet, times(1)).next();
        verify(mockResultSet, times(1)).close();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testInsertMovie() throws SQLException {
        MovieDao dao = new MySqlMovieDao();
        dao.insertMovie(mockConn, m);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(7)).setString(anyInt(), anyString());
        verify(mockPreparedStmt, times(1)).executeUpdate();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testDeleteMovieById() throws SQLException {
        MovieDao dao = new MySqlMovieDao();
        dao.deleteMovieById(mockConn, 1);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).executeUpdate();
        verify(mockPreparedStmt, times(1)).close();
    }

}
