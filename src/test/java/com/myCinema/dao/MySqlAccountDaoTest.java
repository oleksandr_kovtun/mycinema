package com.myCinema.dao;

import com.myCinema.dao.accountDao.AccountDao;
import com.myCinema.dao.accountDao.MySqlAccountDao;
import com.myCinema.model.Account;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MySqlAccountDaoTest {

    Account acc = new Account("login", "password", "email");

    @Mock
    Connection mockConn;
    @Mock
    PreparedStatement mockPreparedStmt;
    @Mock
    ResultSet mockResultSet;

    @Before
    public void setUp() throws SQLException {
        when(mockConn.prepareStatement(anyString())).thenReturn(mockPreparedStmt);
        when(mockConn.prepareStatement(anyString(), anyInt())).thenReturn(mockPreparedStmt);
        doNothing().when(mockPreparedStmt).setInt(anyInt(), anyInt());
        doNothing().when(mockPreparedStmt).setString(anyInt(), anyString());
        when(mockPreparedStmt.executeUpdate()).thenReturn(1);
        when(mockPreparedStmt.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(Boolean.FALSE);

        acc.setName("name");
        acc.setSurname("surname");
        acc.setPhoneNumber("8798709");
        acc.setId(0);
    }

    @Test
    public void testFindAccountById() throws SQLException {
        AccountDao dao = new MySqlAccountDao();
        dao.findAccountById(mockConn, 3);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).executeQuery();
        verify(mockResultSet, times(1)).next();
        verify(mockResultSet, times(1)).close();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testFindFullAccById() throws SQLException {
        AccountDao dao = new MySqlAccountDao();
        dao.findFullAccountById(mockConn, 3);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).executeQuery();
        verify(mockResultSet, times(1)).next();
        verify(mockResultSet, times(1)).close();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testFindAccountByLogin() throws SQLException {
        AccountDao dao = new MySqlAccountDao();
        dao.findAccountByLogin(mockConn, "asdf");

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setString(anyInt(), anyString());
        verify(mockPreparedStmt, times(1)).executeQuery();
        verify(mockResultSet, times(1)).next();
        verify(mockResultSet, times(1)).close();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testInsertAccount() throws SQLException {
        when(mockResultSet.getInt(anyInt())).thenReturn(1);
        when(mockPreparedStmt.getGeneratedKeys()).thenReturn(mockResultSet);

        AccountDao dao = new MySqlAccountDao();
        dao.insertAccount(mockConn, acc);

        verify(mockConn, times(1)).prepareStatement(anyString(), anyInt());
        verify(mockPreparedStmt, times(3)).setString(anyInt(), anyString());
        verify(mockPreparedStmt, times(1)).executeUpdate();
        verify(mockPreparedStmt, times(1)).getGeneratedKeys();
        verify(mockResultSet, times(1)).next();
        verify(mockResultSet, times(1)).getInt(1);
        verify(mockResultSet, times(1)).close();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testInsertAccountInfo() throws SQLException {
        AccountDao dao = new MySqlAccountDao();
        dao.insertAccountInfo(mockConn, acc);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(3)).setString(anyInt(), anyString());
        verify(mockPreparedStmt, times(1)).executeUpdate();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testUpdatePassword() throws SQLException {
        AccountDao dao = new MySqlAccountDao();
        dao.updatePassword(mockConn, acc);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).setString(anyInt(), anyString());
        verify(mockPreparedStmt, times(1)).executeUpdate();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testUpdateName() throws SQLException {
        AccountDao dao = new MySqlAccountDao();
        dao.updateName(mockConn, acc);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).setString(anyInt(), anyString());
        verify(mockPreparedStmt, times(1)).executeUpdate();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testUpdateSurname() throws SQLException {
        AccountDao dao = new MySqlAccountDao();
        dao.updateSurname(mockConn, acc);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).setString(anyInt(), anyString());
        verify(mockPreparedStmt, times(1)).executeUpdate();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testUpdateNumber() throws SQLException {
        AccountDao dao = new MySqlAccountDao();
        dao.updateNumber(mockConn, acc);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).setString(anyInt(), anyString());
        verify(mockPreparedStmt, times(1)).executeUpdate();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testFindCountByLogin() throws SQLException {
        when(mockResultSet.getInt(anyInt())).thenReturn(1);
        when(mockPreparedStmt.getResultSet()).thenReturn(mockResultSet);

        AccountDao dao = new MySqlAccountDao();
        dao.findCountByLogin(mockConn, "login");

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setString(anyInt(), anyString());
        verify(mockPreparedStmt, times(1)).executeQuery();
        verify(mockPreparedStmt, times(1)).getResultSet();
        verify(mockResultSet, times(1)).next();
        verify(mockResultSet, times(1)).getInt(anyInt());
        verify(mockResultSet, times(1)).close();
        verify(mockPreparedStmt, times(1)).close();
    }
}
