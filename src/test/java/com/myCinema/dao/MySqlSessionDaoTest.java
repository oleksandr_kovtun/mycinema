package com.myCinema.dao;

import com.myCinema.dao.sessionDao.MySqlSessionDao;
import com.myCinema.dao.sessionDao.SessionDao;
import com.myCinema.model.Movie;
import com.myCinema.model.Session;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class MySqlSessionDaoTest {

    @Mock
    Connection mockConn;
    @Mock
    PreparedStatement mockPreparedStmt;
    @Mock
    ResultSet mockResultSet;

    Movie m = new Movie(1, "Aaa", "poster1.png", "dgenre",
            "russian", "USA", 130, "", "");
    Session s = new Session(m, 1,"date", "time", 80, 90);

    @Before
    public void setUp() throws SQLException {
        when(mockConn.prepareStatement(anyString())).thenReturn(mockPreparedStmt);
        doNothing().when(mockPreparedStmt).setInt(anyInt(), anyInt());
        doNothing().when(mockPreparedStmt).setString(anyInt(), anyString());
        when(mockPreparedStmt.executeUpdate()).thenReturn(1);
        when(mockPreparedStmt.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(Boolean.FALSE);
        when(mockResultSet.getInt(anyInt())).thenReturn(10);
    }

    @Test
    public void testFindSessionById() throws SQLException {
        SessionDao dao = new MySqlSessionDao();
        dao.findSessionById(mockConn, 1);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).executeQuery();
        verify(mockResultSet, times(1)).next();
        verify(mockResultSet, times(1)).close();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testFindAllSessions() throws SQLException {
        SessionDao dao = new MySqlSessionDao();
        dao.findAllSessions(mockConn, 5, 5, "orderBy");

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(2)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).executeQuery();
        verify(mockResultSet).next();
        verify(mockResultSet, times(1)).close();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testFindAllActualSessions() throws SQLException {
        SessionDao dao = new MySqlSessionDao();
        dao.findAllActualSessions(mockConn, 5, 5, "orderBy");

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setString(anyInt(), anyString());
        verify(mockPreparedStmt, times(2)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).executeQuery();
        verify(mockResultSet).next();
        verify(mockResultSet, times(1)).close();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testFindSessionsByMovieId() throws SQLException {
        SessionDao dao = new MySqlSessionDao();
        dao.findSessionsByMovieId(mockConn, 5);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setString(anyInt(), anyString());
        verify(mockPreparedStmt, times(1)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).executeQuery();
        verify(mockResultSet).next();
        verify(mockResultSet, times(1)).close();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testFindSessionsByDate() throws SQLException {
        SessionDao dao = new MySqlSessionDao();
        dao.findSessionsByDate(mockConn, "date");

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setString(anyInt(), anyString());
        verify(mockPreparedStmt, times(1)).executeQuery();
        verify(mockResultSet).next();
        verify(mockResultSet, times(1)).close();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testFindSessionsByMonth() throws SQLException {
        SessionDao dao = new MySqlSessionDao();
        dao.findSessionsByMonth(mockConn, 2021, 03);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(2)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).executeQuery();
        verify(mockResultSet).next();
        verify(mockResultSet, times(1)).close();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testFindCountOfSessions() throws SQLException {
        SessionDao dao = new MySqlSessionDao();
        dao.findCountOfSessions(mockConn);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setString(anyInt(), anyString());
        verify(mockPreparedStmt, times(1)).executeQuery();
        verify(mockResultSet).next();
        verify(mockResultSet, times(1)).close();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testFindCountOfSessionsByMovieId() throws SQLException {
        SessionDao dao = new MySqlSessionDao();
        dao.findCountOfSessionsByMovieId(mockConn, 5);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setString(anyInt(), anyString());
        verify(mockPreparedStmt, times(1)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).executeQuery();
        verify(mockResultSet, times(1)).getInt(anyInt());
        verify(mockResultSet).next();
        verify(mockResultSet, times(1)).close();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testInsertSession() throws SQLException {
        SessionDao dao = new MySqlSessionDao();
        dao.insertSession(mockConn, 5, s);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(2)).setString(anyInt(), anyString());
        verify(mockPreparedStmt, times(2)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).executeUpdate();
        verify(mockPreparedStmt, times(1)).close();
    }

    @Test
    public void testDeleteSessionById() throws SQLException {
        SessionDao dao = new MySqlSessionDao();
        dao.deleteSessionById(mockConn, 3);

        verify(mockConn, times(1)).prepareStatement(anyString());
        verify(mockPreparedStmt, times(1)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmt, times(1)).executeUpdate();
        verify(mockPreparedStmt, times(1)).close();
    }

}
