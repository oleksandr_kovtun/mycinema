package com.myCinema.command;

import com.myCinema.command.common.LogoutCmd;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LogoutCmdTest {

    @Mock
    HttpSession mockSession;
    @Mock
    HttpServletRequest mockRequest;
    @Mock
    HttpServletResponse mockResponse;

    @Before
    public void setUp() {
        when(mockRequest.getSession()).thenReturn(mockSession);
        doNothing().when(mockRequest).removeAttribute(anyString());
        doNothing().when(mockSession).invalidate();
    }

    @Test
    public void testLogoutCmdExecute() {
        Command c = new LogoutCmd();
        c.execute(mockRequest, mockResponse);

        verify(mockRequest, times(1)).getSession();
        verify(mockSession, times(1)).invalidate();
        verify(mockRequest, times(1)).removeAttribute(anyString());
    }
}
