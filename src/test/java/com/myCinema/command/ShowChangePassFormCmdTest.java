package com.myCinema.command;

import com.myCinema.command.account.ShowChangePassFormCmd;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ShowChangePassFormCmdTest {

    @Mock
    HttpSession mockSession;
    @Mock
    HttpServletRequest mockRequest;
    @Mock
    HttpServletResponse mockResponse;

    @Before
    public void setUp() {
        when(mockRequest.getSession()).thenReturn(mockSession);
        doNothing().when(mockRequest).setAttribute(anyString(), any());
        doNothing().when(mockSession).removeAttribute(anyString());
    }

    @Test
    public void testShowAdminFormCmdExecuteCase1() {

        Command c = new ShowChangePassFormCmd();
        c.execute(mockRequest, mockResponse);

        verify(mockRequest, times(1)).setAttribute(anyString(), any());
    }
}
