package com.myCinema.command;

import com.myCinema.command.common.SetLocaleCmd;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SetLocaleCmdTest {

    @Mock
    HttpSession mockSession;
    @Mock
    HttpServletRequest mockRequest;
    @Mock
    HttpServletResponse mockResponse;

    @Before
    public void setUp() {
        when(mockRequest.getSession()).thenReturn(mockSession);
        doNothing().when(mockSession).setAttribute(anyString(), any());
        when(mockRequest.getParameter(eq("loc"))).thenReturn("ru");

    }

    @Test
    public void testSetLocale() {
        Command c = new SetLocaleCmd();
        c.execute(mockRequest, mockResponse);

        verify(mockRequest, times(1)).getParameter(anyString());
        verify(mockRequest, times(1)).getSession();
        verify(mockSession, times(1)).setAttribute(anyString(), any());
    }
}
