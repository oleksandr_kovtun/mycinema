package com.myCinema.command;

import com.myCinema.command.common.SetLocaleCmd;
import org.junit.Assert;
import org.junit.Test;

public class CommandContainerTest {

    @Test
    public void testGetCommand() {
        CommandContainer cc = new CommandContainer();
        Command c = cc.getCommand("setLocale");
        Class clazz = c.getClass();

        Assert.assertEquals(clazz, SetLocaleCmd.class);
    }
}
