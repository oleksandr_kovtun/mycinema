package com.myCinema.utility;

import org.junit.Assert;
import org.junit.Test;

public class EncryptorTest {

    @Test
    public void testHashing() {
        String input = "test";
        String expectedOutput = "098F6BCD4621D373CADE4E832627B4F6";

        String result = Encryptor.hash(input);

        Assert.assertEquals(expectedOutput, result);
    }

    @Test
    public void testLengthOfHashOutputString() {
        String input = "ASfgagaeoiyonvouegtiqutifjbiwutrqi3tiqfhownbgoqwgei";
        int expectedLength = 32;

        String result = Encryptor.hash(input);

        Assert.assertEquals(result.length(), expectedLength);
    }
}
