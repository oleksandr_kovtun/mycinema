package com.myCinema.utility;

import org.junit.Assert;
import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateManagerTest {

    @Test
    public void testFormatCurrentDataForSql() {
        String currentData = DateManager.currentDateForSql();
        Pattern p = Pattern.compile("[0-9]{4}-[0-9]{2}-[0-9]{2}");
        Matcher m = p.matcher(currentData);

        Assert.assertTrue(m.find());
    }

    @Test
    public void testDateParser() {
        String inputDate = "2021-08-24";
        String expectedOutput = "24.08.2021";

        String result = DateManager.parseDate(inputDate);

        Assert.assertEquals(result, expectedOutput);
    }

    @Test
    public void testTimeParser() {
        String inputTime = "18:35:13";
        String expectedOutput = "18:35";

        String  result = DateManager.parseTime(inputTime);

        Assert.assertEquals(result, expectedOutput);
    }
}
