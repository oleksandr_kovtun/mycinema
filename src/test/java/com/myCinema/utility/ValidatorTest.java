package com.myCinema.utility;

import org.junit.Assert;
import org.junit.Test;

public class ValidatorTest {

    @Test
    public void testCommonFilesIsValidTrue() {
        String input = "asdf";

        boolean result = Validator.commonFieldIsValid(input);
        Assert.assertTrue(result);
    }

    @Test
    public void testCommonFilesIsValidFalse() {
        String input = "qwe";

        boolean result = Validator.commonFieldIsValid(input);
        Assert.assertFalse(result);
    }

    @Test
    public void testEmailIsValidTrue() {
        String email = "someText99@something.com";

        boolean result = Validator.emailIsValid(email);
        Assert.assertTrue(result);
    }

    @Test
    public void testEmailIsValidFalse() {
        String email = "invalidEmail.com";

        boolean result = Validator.emailIsValid(email);
        Assert.assertFalse(result);
    }

    @Test
    public void testPhoneNumberIsValidTrue() {
        String phone = "+ 38 (067) 28 28 084";

        boolean result = Validator.phoneIsValid(phone);
        Assert.assertTrue(result);
    }

    @Test
    public void testPhoneNumberIsValidFalse() {
        String phone = "7951ai334";

        boolean result = Validator.phoneIsValid(phone);
        Assert.assertFalse(result);
    }

    @Test
    public void testPhoneNumberValidateIfValid() {
        String phone = "+ 38 (067) 28 28 084";
        String expected = "380672828084";

        String result = Validator.validatePhone(phone);

        Assert.assertEquals(result, expected);
    }

    @Test
    public void testPhoneNumberValidateIfNotValid() {
        String phone = "7951ai334";
        String expected = "Phone number is invalid!";

        String result = Validator.validatePhone(phone);

        Assert.assertEquals(result, expected);
    }
}
