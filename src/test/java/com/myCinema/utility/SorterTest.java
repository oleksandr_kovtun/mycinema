package com.myCinema.utility;

import com.myCinema.model.Movie;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SorterTest {
    private static List<Movie> inputList = new ArrayList<>();

    @BeforeClass
    public static void createMovies() {
        Movie movie1 = new Movie(1, "Aaa", "poster1.png", "dgenre",
                "russian", "USA", 130, "", "");
        Movie movie2 = new Movie(2, "Ccc", "poster2.png", "agenre",
                "english", "France", 130, "", "");
        Movie movie3 = new Movie(3, "Ddd", "poster3.png", "bgenre",
                "russian", "Ukraine", 130, "", "");
        Movie movie4 = new Movie(4, "Bbb", "poster4.png", "cgenre",
                "english", "USA", 130, "", "");
        movie1.setSessionsAvailable(3);
        movie2.setSessionsAvailable(2);
        movie3.setSessionsAvailable(4);
        movie4.setSessionsAvailable(1);

        inputList.add(movie1);
        inputList.add(movie2);
        inputList.add(movie3);
        inputList.add(movie4);
    }

    @Test
    public void testSortMovieByName() {
        Sorter.sortMovieByName(inputList);
        StringBuilder resultIdString = new StringBuilder();

        String expected = "1423";
        for (Movie m : inputList) {
            resultIdString.append(m.getId());
        }

        Assert.assertEquals(expected, resultIdString.toString());
    }

    @Test
    public void testSortMovieByVoiceActing() {
        Sorter.sortMovieByVoiceActing(inputList);
        StringBuilder resultIdString = new StringBuilder();

        String expected = "english_english_russian_russian_";
        for (Movie m : inputList) {
            resultIdString.append(m.getVoiceActing() + "_");
        }

        Assert.assertEquals(expected, resultIdString.toString());
    }

    @Test
    public void testSortMovieByGenre() {
        Sorter.sortMovieByGenre(inputList);
        StringBuilder resultIdString = new StringBuilder();

        String expected = "2341";
        for (Movie m : inputList) {
            resultIdString.append(m.getId());
        }

        Assert.assertEquals(expected, resultIdString.toString());
    }

    @Test
    public void testSortMovieBySessionCount() {
        Sorter.sortMovieBySessionCount(inputList);
        StringBuilder resultIdString = new StringBuilder();

        String expected = "3124";
        for (Movie m : inputList) {
            resultIdString.append(m.getId());
        }

        Assert.assertEquals(expected, resultIdString.toString());
    }
}
