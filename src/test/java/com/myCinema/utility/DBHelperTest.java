package com.myCinema.utility;

import com.myCinema.model.Account;
import com.myCinema.model.Movie;
import com.myCinema.model.Session;
import com.myCinema.model.Ticket;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DBHelperTest {

    @Mock
    ResultSet mockRS;

    @Before
    public void setUp() throws SQLException {
        when(mockRS.getInt(eq("id"))).thenReturn(5);
        when(mockRS.getString(eq("login"))).thenReturn("asdf");
        when(mockRS.getString(eq("password"))).thenReturn("ThisIsPassword");
        when(mockRS.getString(eq("email"))).thenReturn("someText@mail.com");
        when(mockRS.getString(eq("role"))).thenReturn("customer");

        when(mockRS.getString(eq("name"))).thenReturn("someName");
        when(mockRS.getString(eq("surname"))).thenReturn("someSurname");
        when(mockRS.getString(eq("phone_number"))).thenReturn("98124986");

        when(mockRS.getInt(eq("movie.id"))).thenReturn(5);
        when(mockRS.getString(eq("poster"))).thenReturn("image.png");
        when(mockRS.getString(eq("genre"))).thenReturn("someGenre");
        when(mockRS.getString(eq("voice_acting"))).thenReturn("english");
        when(mockRS.getString(eq("filmed_in"))).thenReturn("Britain");
        when(mockRS.getInt(eq("duration"))).thenReturn(200);
        when(mockRS.getString(eq("short_desc"))).thenReturn("");
        when(mockRS.getString(eq("description"))).thenReturn("");

        when(mockRS.getInt(eq("session.id"))).thenReturn(5);
        when(mockRS.getString(eq("date"))).thenReturn("2021-08-24");
        when(mockRS.getString(eq("time"))).thenReturn("18:30:00");
        when(mockRS.getInt(eq("seat_occupied"))).thenReturn(80);
        when(mockRS.getInt(eq("cost"))).thenReturn(90);

        when(mockRS.getInt("account_id")).thenReturn(10);
        when(mockRS.getInt("session_id")).thenReturn(20);
        when(mockRS.getInt("seat")).thenReturn(60);
    }


    @Test
    public void testFillAccount() throws SQLException {
        Account expectedAcc = new Account(5, "asdf", "ThisIsPassword",
                "someText@mail.com", "customer");

        Account result = DBHelper.fillAccount(mockRS);

        Assert.assertEquals(expectedAcc, result);
    }

    @Test
    public void testFillAccountFullInfo() throws SQLException {
        Account expectedAcc = new Account(5, "asdf", "ThisIsPassword",
                "someText@mail.com", "customer");
        expectedAcc.setName("someName");
        expectedAcc.setSurname("someSurname");
        expectedAcc.setPhoneNumber("98124986");

        Account result = DBHelper.fillAccountFullInfo(mockRS);

        Assert.assertEquals(expectedAcc, result);
    }

    @Test
    public void testFillMovie() throws SQLException {
        Movie expectedMovie = new Movie(5, "someName", "image.png", "someGenre",
                "english", "Britain", 200, "", "");

        Movie result = DBHelper.fillMovie(mockRS);

        Assert.assertEquals(expectedMovie, result);
    }

    @Test
    public void testFillSession() throws SQLException {
        Movie movie = new Movie(5, "someName", "image.png", "someGenre",
                "english", "Britain", 200, "", "");
        Session expectedSession = new Session(movie, 5, "24.08.2021",
                "18:30", 80, 90);

        Session result = DBHelper.fillSession(mockRS);

        Assert.assertEquals(expectedSession, result);
    }

    @Test
    public void testFillSessionWithoutMovie() throws SQLException {
        Session expectedSession = new Session(5, "24.08.2021",
                "18:30", 80, 90);

        Session result = DBHelper.fillSessionWithoutMovie(mockRS);

        Assert.assertEquals(expectedSession, result);
    }

    @Test
    public void testFillMainInfoTicket() throws SQLException {
        Ticket expectedTicket = new Ticket(10, 20, 60);

        Ticket result = DBHelper.fillMainInfoTicket(mockRS);

        Assert.assertEquals(expectedTicket, result);
    }

    @Test
    public void testFillTicket() throws SQLException {
        Ticket expectedTicket = new Ticket(10, 20, 60,
                "someName", "24.08.2021", "18:30");

        Ticket result = DBHelper.fillTicket(mockRS);

        Assert.assertEquals(expectedTicket, result);
    }
}
