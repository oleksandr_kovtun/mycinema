<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
	
	<meta charset="UTF-8">

	<c:if test="${sessionScope.locale == null}">
        <fmt:setLocale value="en"/>
    </c:if>
    <c:if test="${sessionScope.locale != null}">
        <fmt:setLocale value="${sessionScope.locale}"/>
    </c:if>
    <fmt:setBundle basename="localization" var="bundle"/>
    
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

</head>
	

<header>
	<div class="header-left"><font size="6" color="SandyBrown" face="mv boli"><b>&nbsp;&nbsp;&nbsp;MY CINEMA&nbsp;&nbsp;&nbsp;</b></font>
		<a href="controller?operation=showMovies"><button class="header-button"><fmt:message key="movies" bundle="${bundle}"/></button></a>
	  	<a href="controller?operation=showSessions&page=0&orderBy=1"><button class="header-button"><fmt:message key="sessions" bundle="${bundle}"/></button></a>
	  	
	  	<c:if test="${account.accRole==\"director\" || account.accRole==\"admin\"}">
			<a href="admin/administration.jsp"><button class="header-button"><fmt:message key="administer" bundle="${bundle}"/></button></a>
		</c:if>
	</div>
		
	<div class="header-right">
	  	<div class="dropdown">
	  	<button class="dropbtn"><fmt:message key="language" bundle="${bundle}"/></button>
	  	<div class="dropdown-content">
		    <a href="controller?operation=setLocale&loc=en">English</a>
		    <a href="controller?operation=setLocale&loc=ru">Русский</a>
		</div>
		</div>
		
		<c:if test="${account == null}">
			<a href="login.jsp"><button class="header-button"><fmt:message key="signIn" bundle="${bundle}"/></button></a>
		</c:if>
		
		<c:if test="${account != null}">
			<a href="controller?operation=showTickets"><button class="header-button"><fmt:message key="welcome" bundle="${bundle}"/>, <font size="2" color="SandyBrown" face="mv boli"><b>${account.login}</b></font></button ></a>
			<a href="controller?operation=logout"><button class="header-button"><fmt:message key="leave" bundle="${bundle}"/></button ></a>
		</c:if>
	 
	</div> 
	 
</header>

</html>