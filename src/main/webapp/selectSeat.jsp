<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>

<html>

<head>
  	<meta charset="utf-8">
  	<title>MyCinema Select seat</title>
  
  	<c:if test="${sessionScope.locale == null}">
        <fmt:setLocale value="en"/>
    </c:if>
    <c:if test="${sessionScope.locale != null}">
        <fmt:setLocale value="${sessionScope.locale}"/>
    </c:if>
    <fmt:setBundle basename="localization" var="bundle"/>
    
  	<link rel="stylesheet" href="CSS/headerStyle.css">
  	<link rel="stylesheet" href="CSS/sidebarAndContent(2).css">
  	<link rel="stylesheet" href="CSS/hallMapStyle.css">
  	
  	<jsp:include page="JspParts/header.jsp"></jsp:include>
 
</head>

<body>
  
  	<div id="sidebar">
  		<img src="images/posters/${selectedSession.movie.posterUrl}" width="100%" />
	</div>
	
  	<div id="content">
  		<c:if test="${trouble != null}">
		  	<div class="troubleSpace">
			  	<font color="red" size="5">
					<fmt:message key="problem" bundle="${bundle}"/>:
					<c:choose>
					  	<c:when test="${trouble == 0}"><fmt:message key="ticketBuyFailed" bundle="${bundle}"/></c:when>
					</c:choose>
			  	</font>
		  	</div>
	  	</c:if>
  		
  		<h1> ${selectedSession.movie.name}</h1><br>
    	<h2><fmt:message key="session" bundle="${bundle}"/>: ${selectedSession.date}, ${selectedSession.time}. <br>
    	<fmt:message key="ticketCost" bundle="${bundle}"/>: ${selectedSession.ticketCost} 
    	<fmt:message key="grivnas" bundle="${bundle}"/>.<br></h2>
  		<div class="space">
    		<form action="controller">
    			<h1>
		    		<text class="textForScreenMark">
		    			<fmt:message key="SCREEN" bundle="${bundle}"/>
		    		</text> &nbsp;
		    		
		    		<c:forEach items="${seats}" var="item" varStatus="rowCounter">
		    
				    	<c:if test="${item}">
				    		<input type="checkbox" name="buySeat" value="${rowCounter.count - 1}">
				    	</c:if>
				    	<c:if test="${!item}">
				    		<font color="red">x</font>
				    	</c:if>
				    	<c:if test="${rowCounter.count%10 == 0 }"><font size="4" color="black">
				    	<fmt:formatNumber value="${rowCounter.count/10}" maxFractionDigits="0"/> <fmt:message key="line" bundle="${bundle}"/> </font><br>&nbsp</c:if>
					</c:forEach><br>
					
					<button class="buyTicketsButton">
						<fmt:message key="buyTickets" bundle="${bundle}"/>
					</button>
					<input type="hidden" name="operation" value="confirmTicketsBuy">
				</h1>
			</form>
		</div>
	
  	</div>
  	
</body>

</html>