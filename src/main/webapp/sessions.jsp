<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ taglib prefix="myTag" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>

<html>

<head>

  	<meta charset="utf-8">
  	<title>MyCinema Sessions</title>
  
	<%-- Start of localization block --%>
  	<c:if test="${sessionScope.locale == null}">
        <fmt:setLocale value="en"/>
    </c:if>
    <c:if test="${sessionScope.locale != null}">
        <fmt:setLocale value="${sessionScope.locale}"/>
    </c:if>
	<fmt:setBundle basename="localization" var="bundle"/>
    <%-- End of localization block --%>
    
    <%-- CSS references --%>
	<link rel="stylesheet" href="CSS/headerStyle.css">
	<link rel="stylesheet" href="CSS/sidebarAndContent(1).css">

	<%-- Header include --%>
	<jsp:include page="JspParts/header.jsp"/>
	
</head>

<body>
	
	<div id="sidebar">
	 
		<%-- Start of "order by" button group --%>
	    <div class="btn-group">
	    	<div style="text-align: center;"><font size="5" color="black"><fmt:message key="orderBy" bundle="${bundle}"/></font></div>
		    <a href="controller?operation=showSessions&page=0&orderBy=1">
				<c:choose>
					<c:when test="${orderBy==1}">
				    	<button class="disabledbutton" disabled><fmt:message key="dateTime" bundle="${bundle}"/></button>
				    </c:when>    
				    <c:otherwise>
				    	<button class="button"><fmt:message key="dateTime" bundle="${bundle}"/></button>
				    </c:otherwise>
				</c:choose>
			</a>
		    
		    <a href="controller?operation=showSessions&page=0&orderBy=2">
		    	<c:choose>
			    	<c:when test="${orderBy==2}">
				       <button class="disabledbutton" disabled><fmt:message key="movieName" bundle="${bundle}"/></button>
				    </c:when>    
				    <c:otherwise>
				       <button class="button"><fmt:message key="movieName" bundle="${bundle}"/></button>
					</c:otherwise>
				</c:choose>
			</a>
			    
		    <a href="controller?operation=showSessions&page=0&orderBy=3">
		    	<c:choose>
			    	<c:when test="${orderBy==3}">
				    	<button class="disabledbutton" disabled><fmt:message key="seatsAvailable" bundle="${bundle}"/></button>
				    </c:when>    
				    <c:otherwise>
				    	<button class="button"><fmt:message key="seatsAvailable" bundle="${bundle}"/></button>
				    </c:otherwise>
			    </c:choose>
			</a>
		    
		    <a href="controller?operation=showSessions&page=0&orderBy=4">
		    	<c:choose>
			    	<c:when test="${orderBy==4}">
				        <button class="disabledbutton" disabled><fmt:message key="ticketCost" bundle="${bundle}"/></button>
				    </c:when>    
				    <c:otherwise>
				        <button class="button"><fmt:message key="ticketCost" bundle="${bundle}"/></button>
				    </c:otherwise>
			    </c:choose>
			</a>
			
		</div><hr>   
		<%-- End of "order by" button group --%>
	    
	    <%-- Start of "order" button group --%>
	    <div class="btn-group">
		    <div style="text-align: center;"><font size="5" color="black"><fmt:message key="order" bundle="${bundle}"/></font></div>
		    <a href="controller?operation=showSessions&page=0&orderBy=${orderBy}&order=change">
		    	<c:choose>
					<c:when test="${ordered==false}">
					    <button class="button"><font color="#ADFF2F"><fmt:message key="DIRECT" bundle="${bundle}"/></font> <fmt:message key="order" bundle="${bundle}"/></button>
					</c:when>    
					<c:otherwise>
					    <button class="button"><font color="#DDA0DD"><fmt:message key="REVERSE" bundle="${bundle}"/></font> <fmt:message key="order" bundle="${bundle}"/></button>
					</c:otherwise>
				</c:choose>
			</a>
		</div><hr>
		<%-- End of "order" button group --%>
		
		<%-- Start of "set limit of objects on page" button group --%>
		<div class="btn-group">
			<div style="text-align: center;">
				<font size="5" color="black">
					<fmt:message key="objOnPage" bundle="${bundle}"/>: 
					<c:choose>
						<c:when test="${limit == null}">5</c:when>
						<c:otherwise>
							${limit}
						</c:otherwise>
					</c:choose>
				</font>
			</div>
			<form action="controller">
				<input name="objLimit" type="range" min="1" max="10" step="1" value="${limit}" style="width: 100%;"> <br>
				<input class="button" type="submit" value="<fmt:message key="confirm" bundle="${bundle}"/>">
				<input type="hidden" name="operation" value="showSessions">
				<input type="hidden" name="orderBy" value="${orderBy}">
				<input type="hidden" name="page" value="0">
			</form>
		</div>
		<%-- End of "set limit of objects on page" button group --%>
	</div>

  	<div id="content">
    
    	<%-- Start sessions show --%>
	    <c:forEach items="${sessions}" var="item">
	    	<div style="min-height: 460px;">
		    	<p><div class="leftimg">
		    		<a href="controller?operation=session&sessionid=${item.id}">
				    	<img src="images/posters/${item.movie.posterUrl}"  width="250" height="373"/>
				    	<button class="button2">
				    		<fmt:message key="selectSession" bundle="${bundle}"/><br>
				    		${item.emptySeats}/100 <fmt:message key="available" bundle="${bundle}"/>
				    	</button>
			    	</a>
		    	</div>
		    	
		    	<h1>${item.movie.name}<br> ${item.time}, ${item.date}</h1>
		    	<h2>
			    	<font color="green">
			    		<fmt:message key="ticketCost" bundle="${bundle}"/>: ${item.ticketCost} <fmt:message key="grivnas" bundle="${bundle}"/>.
			    	</font><br>
			    	<fmt:message key="genre" bundle="${bundle}"/>: <fmt:message key="${item.movie.genre}" bundle="${bundle}"/>
			    	<fmt:message key="voice" bundle="${bundle}"/>: <fmt:message key="${item.movie.voiceActing}" bundle="${bundle}"/><p>
			    	<fmt:message key="filmedIn" bundle="${bundle}"/>:<fmt:message key="${item.movie.filmedIn}" bundle="${bundle}"/><p>
			    	<fmt:message key="duration" bundle="${bundle}"/>: ${item.movie.duration} <fmt:message key="minutes" bundle="${bundle}"/>.<p>
			    	<fmt:message key="description" bundle="${bundle}"/>: <br> ${item.movie.shortDesc}
		    	</h2>
	    	</div><hr>
		</c:forEach>
		<%-- End sessions show --%>
		
  	</div>
  	
  	<div style="text-align:center;">
  
		<%-- Navigation bar (pagination) --%>
  		<myTag:navbar lastNum="${lastNum}" operation="showSessions" firstNum="${firstNum}" orderParamName="orderBy"
  				 currentPage="${currentPage}" totalPages="${totalPages}" orderBy="${orderBy}" />
  
	</div>
  
</body>

</html>