<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix = "myTldTag" uri = "../WEB-INF/custom.tld"%>

<!DOCTYPE html>

<html>
<head>
  	<meta charset="utf-8">
  	<title>MyCinema Administrate</title>
  	
  	<c:if test="${sessionScope.locale == null}">
        <fmt:setLocale value="en"/>
    </c:if>
    <c:if test="${sessionScope.locale != null}">
        <fmt:setLocale value="${sessionScope.locale}"/>
    </c:if>
    <fmt:setBundle basename="localization" var="bundle"/>
    
    <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
    
  	<link rel="stylesheet" href="../CSS/headerStyle.css">
  	<link rel="stylesheet" href="../CSS/adminStyle.css">
  
  	<script src="../JS/subMenuButtons.js"></script>
  	<script src="../JS/tableSort.js"></script>

 	</head>
<body>

<header>
	<div class="header-left"><font size="6" color="SandyBrown" face="mv boli"><b>&nbsp;&nbsp;&nbsp;MY CINEMA&nbsp;&nbsp;&nbsp;</b></font>
		<a href="../controller?operation=showMovies"><button class="header-button"><fmt:message key="movies" bundle="${bundle}"/></button></a>
	  	<a href="../controller?operation=showSessions&page=0&orderBy=1"><button class="header-button"><fmt:message key="sessions" bundle="${bundle}"/></button></a>
	  	<c:if test="${account.accRole==\"director\" || account.accRole==\"admin\"}">
			<a href="administration.jsp"><button class="header-button"><fmt:message key="administer" bundle="${bundle}"/></button></a>
		</c:if>
	</div>
		
	<div class="header-right">
	  	<div class="dropdown">
	  		<button class="dropbtn">
	  			<fmt:message key="language" bundle="${bundle}"/>
	  		</button>
		  	<div class="dropdown-content">
			    <a href="../controller?operation=setLocale&loc=en">English</a>
			    <a href="../controller?operation=setLocale&loc=ru">Русский</a>
			</div>
		</div>
		<c:if test="${account != null}">
			<a href="../controller?operation=showTickets">
				<button class="header-button">
					<fmt:message key="welcome" bundle="${bundle}"/>, 
					<font size="2" color="SandyBrown" face="mv boli"><b>${account.login}</b></font>
				</button >
			</a>
			<a href="../controller?operation=logout">
				<button class="header-button">
					<fmt:message key="leave" bundle="${bundle}"/>
				</button >
			</a>
		</c:if>
	</div> 
	 
  	</header>
  
  	<div id="sidebar">
  
  		<%-- 
  		<h2>Date and time is:
  		<myTldTag:GetCurrentDateTime/></h2>
  		--%>
		<div id="menu_body">
			<ul>
				<li><a href="" onclick="openMenu('sub_menu_1');return(false)"><fmt:message key="movieOptions" bundle="${bundle}"/></a>
					<ul id="sub_menu_1">
						<li><a href="controller?operation=showAdminForm&form=createMovieForm"><fmt:message key="createMovie" bundle="${bundle}"/></a></li>
						<li><a href="controller?operation=showRemoveMovie"><fmt:message key="removeMovie" bundle="${bundle}"/></a></li>
					</ul>
				</li>
				<li><a href="#" onclick="openMenu('sub_menu_2');return(false)"><fmt:message key="sessionOptions" bundle="${bundle}"/></a>
					<ul id="sub_menu_2">
						<li><a href="controller?operation=showCreateSession"><fmt:message key="createSession" bundle="${bundle}"/></a></li>
						<li><a href="controller?operation=sessionsToRemove"><fmt:message key="removeSession" bundle="${bundle}"/></a></li>
					</ul>
				</li>
				<li><a href="#" onclick="openMenu('sub_menu_3');return(false)"><fmt:message key="statistic" bundle="${bundle}"/></a>
					<ul id="sub_menu_3">
						<li><a href="controller?operation=showAdminForm&form=dailyReport"><fmt:message key="dailyReport" bundle="${bundle}"/></a></li>
						<li><a href="controller?operation=showAdminForm&form=monthlyReport"><fmt:message key="monthlyReport" bundle="${bundle}"/></a></li>
					</ul>
				</li>
			</ul>
		</div>
		
		<div id="menu_body">
				<c:if test="${account.accRole == \"director\"}">
					<ul>
						<li><a href="#" onclick="openMenu('sub_menu_4');return(false)"><fmt:message key="employeeInfo" bundle="${bundle}"/></a>
							<ul id="sub_menu_4">
								<li><a href="controller?operation=showEmployee"><fmt:message key="employeeManagement" bundle="${bundle}"/></a></li>
								<li><a href="controller?operation=showAdminForm&form=giveAdminRights"><fmt:message key="giveAdminRights" bundle="${bundle}"/></a></li>
							</ul>
						</li>
					</ul>
				</c:if>
		</div>

  	</div>
  	
  	<div id="content">
  
		<c:if test="${notify == null && trouble == null}">
			<div class="carefulSpace">
				<font color="#800000" size="5">
					<fmt:message key="hello" bundle="${bundle}"/>, <em>${account.login}</em>! 
					<fmt:message key="adminNotification" bundle="${bundle}"/>
				</font>
			</div><hr>
		</c:if>
		
		<c:if test="${notify != null}">
	  		<div class="notifySpace">
			  	<font color="green" size="5">
					<fmt:message key="notification" bundle="${bundle}"/>:
					<c:choose>
					  	<c:when test="${notify == 1}"><fmt:message key="sessionCreated" bundle="${bundle}"/></c:when>
					  	<c:when test="${notify == 2}"><fmt:message key="movieCreated" bundle="${bundle}"/></c:when>
					  	<c:when test="${notify == 3}"><fmt:message key="movieDeleted" bundle="${bundle}"/></c:when>
					  	<c:when test="${notify == 4}"><fmt:message key="sessionDeleted" bundle="${bundle}"/></c:when>
					  	<c:when test="${notify == 5}"><fmt:message key="adminRightsGiven" bundle="${bundle}"/></c:when>
					  	<c:when test="${notify == 6}"><fmt:message key="emloyeeDissmised" bundle="${bundle}"/></c:when>
					</c:choose>
			  	</font>
		  	</div>
	  	</c:if>
	  	
	  	<c:if test="${trouble != null}">
		  	<div class="troubleSpace">
			  	<font color="red" size="5">
					<fmt:message key="problem" bundle="${bundle}"/>:
					<c:choose>
					  	<c:when test="${trouble == 1}"><fmt:message key="sessionNotCreated" bundle="${bundle}"/></c:when>
					  	<c:when test="${trouble == 2}"><fmt:message key="problemWithFile" bundle="${bundle}"/></c:when>
					  	<c:when test="${trouble == 3}"><fmt:message key="movieNotCreated" bundle="${bundle}"/></c:when>
					  	<c:when test="${trouble == 4}"><fmt:message key="movieDeletingFailed" bundle="${bundle}"/></c:when>
					  	<c:when test="${trouble == 5}"><fmt:message key="sessionDeletingFailed" bundle="${bundle}"/></c:when>
					  	<c:when test="${trouble == 6}"><fmt:message key="statNotGenerated" bundle="${bundle}"/></c:when>
					  	<c:when test="${trouble == 7}"><fmt:message key="wrongDate" bundle="${bundle}"/></c:when>
					  	<c:when test="${trouble == 8}"><fmt:message key="thisAccNotExists" bundle="${bundle}"/></c:when>
					</c:choose>
			  	</font>
		  	</div>
	  	</c:if>
  
    	<c:if test="${sessionForm}">
    		<h2><fmt:message key="createSession" bundle="${bundle}"/></h2>
	    	<form action="controller" method="post">
	    		<fmt:message key="selectMovie" bundle="${bundle}"/><br>
	    		<select name="movie">
	    			<c:forEach items="${movies}" var="item">
	    				<option value="${item.id}"> ${item.name} </option>
	    			</c:forEach>
	    		</select><br>
	    		<fmt:message key="date" bundle="${bundle}"/>: <br>
	    		<input type="date" name="sessionDate" required><br>
	    		<fmt:message key="time" bundle="${bundle}"/>: <br>
	    		<input type="time" name="sessionTime" required><br>
	    		<fmt:message key="cost" bundle="${bundle}"/>: <br>
	    		<input type="number" name="cost" required><br>
	    		<input class="confirm_button" type="submit" value="<fmt:message key="createSession" bundle="${bundle}"/>">
	    		<input type="hidden" name="operation" value="createSession">
	    	</form>
    	</c:if>
    	
    	<c:if test="${movieForm}">
    		<h2><fmt:message key="createMovie" bundle="${bundle}"/></h2>
	    	<form action="controller" method="post" enctype="multipart/form-data">
	    		<input type="hidden" name="operation" value="createMovie" required>
	    		<fmt:message key="movieName" bundle="${bundle}"/> <br>
	    		<input type="text" name="movieName" placeholder="<fmt:message key="enterMovieName" bundle="${bundle}"/>" required><br>
	    		<fmt:message key="genre" bundle="${bundle}"/> <br>
	    		<select name="movieGenre">
	    			<option value="adventure"><fmt:message key="adventure" bundle="${bundle}"/></option>
	    			<option value="biography"><fmt:message key="biography" bundle="${bundle}"/></option>
	    			<option value="fantasy"><fmt:message key="fantasy" bundle="${bundle}"/></option>
	    			<option value="detective"><fmt:message key="detective" bundle="${bundle}"/></option>
	    			<option value="drama"><fmt:message key="drama" bundle="${bundle}"/></option>
	    			<option value="sport"><fmt:message key="sport" bundle="${bundle}"/></option>
	    			<option value="western"><fmt:message key="western" bundle="${bundle}"/></option>
	    			<option value="mystery"><fmt:message key="mystery" bundle="${bundle}"/></option>
	    			<option value="thriller"><fmt:message key="thriller" bundle="${bundle}"/></option>
	    			<option value="horror"><fmt:message key="horror" bundle="${bundle}"/></option>
	    			<option value="war"><fmt:message key="war" bundle="${bundle}"/></option>
	    			<option value="action"><fmt:message key="action" bundle="${bundle}"/></option>
	    			<option value="comedy"><fmt:message key="comedy" bundle="${bundle}"/></option>
	    			<option value="history"><fmt:message key="history" bundle="${bundle}"/></option>
	    			<option value="romance"><fmt:message key="romance" bundle="${bundle}"/></option>
	    		</select><br>
	    		<fmt:message key="voice" bundle="${bundle}"/> <br>
	    		<select name="movieVoiceActing">
	    			<option value="english"><fmt:message key="english" bundle="${bundle}"/></option>
	    			<option value="russian"><fmt:message key="russian" bundle="${bundle}"/></option>
	    			<option value="ukrainian"><fmt:message key="ukrainian" bundle="${bundle}"/></option>
	    		</select><br>
	    		<fmt:message key="filmedIn" bundle="${bundle}"/><br>
	    		<select name="movieFilmedIn">
	    			<option value="USA"><fmt:message key="USA" bundle="${bundle}"/></option>
	    			<option value="Britain"><fmt:message key="Britain" bundle="${bundle}"/></option>
	    			<option value="Ukraine"><fmt:message key="Ukraine" bundle="${bundle}"/></option>
	    			<option value="Germany"><fmt:message key="Germany" bundle="${bundle}"/></option>
	    			<option value="France"><fmt:message key="France" bundle="${bundle}"/></option>
	    			<option value="Italy"><fmt:message key="Italy" bundle="${bundle}"/></option>
	    			<option value="Australia"><fmt:message key="Australia" bundle="${bundle}"/></option>
	    			<option value="Canada"><fmt:message key="Canada" bundle="${bundle}"/></option>
	    		</select><br>
	    		<fmt:message key="duration" bundle="${bundle}"/> <br>
	    		<input type="number" name="movieDuration" placeholder="<fmt:message key="enterDuration" bundle="${bundle}"/>" required><br>
	    		<fmt:message key="shortDesc" bundle="${bundle}"/><br>
	    		<textarea name="movieShortDesc" rows=5 maxlength=1024 placeholder="<fmt:message key="enterShortDesc" bundle="${bundle}"/>" required></textarea><br>
	    		<fmt:message key="fullDesc" bundle="${bundle}"/><br>
	    		<textarea name="movieDesc" rows=5 maxlength=2048 placeholder="<fmt:message key="enterFullDesc" bundle="${bundle}"/>" required></textarea><br>
	    		<fmt:message key="poster" bundle="${bundle}"/> : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="file" name="poster" required><br><br>
	    		<button class="confirm_button"><fmt:message key="addMovie" bundle="${bundle}"/></button>
	    	</form>
		</c:if>
		
    	<c:if test="${removeSessionForm}">
	    	<h1> ${session.movie.name}: <fmt:message key="session" bundle="${bundle}"/> 
	    	${session.date}, ${session.time}, ${session.ticketCost}<fmt:message key="grivnas" bundle="${bundle}"/>. </h1>
		    <h2><fmt:message key="genre" bundle="${bundle}"/>: ${session.movie.genre}</h2><br>
		    <h2><fmt:message key="voice" bundle="${bundle}"/>: ${session.movie.voiceActing}</h2><br>
		    <h2><fmt:message key="filmedIn" bundle="${bundle}"/>:${session.movie.filmedIn}</h2><br>
		    <h2><fmt:message key="duration" bundle="${bundle}"/>: ${session.movie.duration} 
		    <fmt:message key="minutes" bundle="${bundle}"/>.</h2><br>
		    <h2><fmt:message key="description" bundle="${bundle}"/>: <br> ${session.movie.fullDesc}</h2><br>
		    <h2><fmt:message key="seatsAvailable" bundle="${bundle}"/>: <br> ${session.emptySeats}/100</h2><br>
		    <form action="controller" method="post">
			  	<input class="confirm_button" style="background: red; width: 300px;" type="submit" value="<fmt:message key="removeSession" bundle="${bundle}"/>">
				<input type="hidden" name="operation" value="removeSession">
				<input type="hidden" name="sessionid" value="${session.id}">
			</form>
    	</c:if>
    	
    	<c:if test="${showSessionsToRemove}">
    		<h2><fmt:message key="removeSession" bundle="${bundle}"/></h2>
    		<table class="table_sort">
					<thead>
						<tr>
							<th>ID</th>
							<th><fmt:message key="movie" bundle="${bundle}"/></th>
							<th><fmt:message key="date" bundle="${bundle}"/></th>
							<th><fmt:message key="time" bundle="${bundle}"/></th>
							<th><fmt:message key="ticketsBought" bundle="${bundle}"/></th>
							<th><fmt:message key="ticketCost" bundle="${bundle}"/>, <fmt:message key="grivnas" bundle="${bundle}"/></th>
							<th><fmt:message key="removeSession" bundle="${bundle}"/></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${allSessions}" var="item">
					    	<tr>
								<td>${item.id}</td>
								<td>${item.movie.name}</td>
								<td>${item.date}</td>
								<td>${item.time}</td>
								<td>${100 - item.emptySeats}</td>
								<td>${item.ticketCost}</td>
								<td>
									<a href="controller?operation=removeSessionForm&sessionid=${item.id}">
										<button style="background: #DAA520; width: 100%; height: 100%;">
											<fmt:message key="removeSession" bundle="${bundle}"/>
										</button>
									</a>
								</td>
							</tr>
					    </c:forEach>
				    </tbody>
			    </table>
    	</c:if>
    	
    	<c:if test="${removeMovieForm}">
    		<h2><fmt:message key="removeMovie" bundle="${bundle}"/></h2>
	    	<form action="controller" method="post">
		    	<select name="movie">
			    	<c:forEach items="${movies}" var="item">
			    		<option value="${item.id}"> ${item.name} </option>
			    	</c:forEach>
		    	</select><br>
		    	<button class="confirm_button"><fmt:message key="confirmDelete" bundle="${bundle}"/></button>
		    	<input type="hidden" name="operation" value="removeMovie">
	    	</form>
    	</c:if>
    	
    	<c:if test="${dailyReportForm}"> 
    		<c:if test="${date == null}"> 
				<form action="controller">
					<fmt:message key="chooseReportDate" bundle="${bundle}"/>:<br>
					<input type="date" name="reportDate" required><br>
					<input class="confirm_button" type="submit" value="<fmt:message key="getReport" bundle="${bundle}"/>">
					<input type="hidden" name="operation" value="dailyReport">
				</form><hr>
			</c:if>
			<c:if test="${date != null}"> 
				<h1><fmt:message key="generatedReportDate" bundle="${bundle}"/>: ${date}.</h1><br>
				<h2>
				&nbsp;<fmt:message key="totalSessions" bundle="${bundle}"/>: ${sessionsPerDay} ;<br> 
				&nbsp;<fmt:message key="totalTickets" bundle="${bundle}"/>: ${totalTicketsBought} ;<br> 
				&nbsp;<fmt:message key="totalProceeds" bundle="${bundle}"/>: ${totalProceeds} <fmt:message key="grivnas" bundle="${bundle}"/>.<br><br>
				&nbsp;<fmt:message key="sessionsInfo" bundle="${bundle}"/>:<br></h2>
				
				<table class="table_sort">
					<thead>
						<tr>
							<th>ID</th>
							<th><fmt:message key="time" bundle="${bundle}"/></th>
							<th><fmt:message key="movie" bundle="${bundle}"/></th>
							<th><fmt:message key="ticketsBought" bundle="${bundle}"/></th>
							<th><fmt:message key="proceeds" bundle="${bundle}"/>, <fmt:message key="grivnas" bundle="${bundle}"/></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${sessionsByDate}" var="item">
							<tr>
								<td>${item.id}</td>
								<td>${item.time}</td>
								<td>${item.movie.name}</td>
								<td>${100-item.emptySeats}</td>
								<td>${(100-item.emptySeats)*item.ticketCost}</td>
							</tr>
					    </c:forEach>
				    </tbody>
			    </table>
			    
			</c:if>
    	</c:if>
    	
    	<c:if test="${monthlyReportForm}"> 
    		<c:if test="${date == null}">
				<form action="controller">
					<fmt:message key="chooseReportDate" bundle="${bundle}"/>:<br>
					<input type="month" name="reportMonth" required><br>
					<input class="confirm_button" type="submit" value="Get new report">
					<input type="hidden" name="operation" value="monthlyReport">
				</form>
			</c:if>
			<c:if test="${date != null}"> 
				<h1><fmt:message key="generatedReportDate" bundle="${bundle}"/>: ${date}.</h1>
				<h2><br>
				&nbsp;<fmt:message key="totalSessions" bundle="${bundle}"/>: ${sessionsPerMonth} ;<br> 
				&nbsp;<fmt:message key="totalTickets" bundle="${bundle}"/>: ${totalTicketsBought} ;<br> 
				&nbsp;<fmt:message key="totalProceeds" bundle="${bundle}"/>: ${totalProceeds} ₴.<br><br>
				&nbsp;<fmt:message key="sessionsInfo" bundle="${bundle}"/>:<br></h2>
				<table class="table_sort">
					<thead>
						<tr>
							<th>ID</th>
							<th><fmt:message key="date" bundle="${bundle}"/></th>
							<th><fmt:message key="time" bundle="${bundle}"/></th>
							<th><fmt:message key="movie" bundle="${bundle}"/></th>
							<th><fmt:message key="ticketsBought" bundle="${bundle}"/></th>
							<th><fmt:message key="proceeds" bundle="${bundle}"/>, <fmt:message key="grivnas" bundle="${bundle}"/></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${sessionsByMonth}" var="item">
					    	<tr>
								<td>${item.id}</td>
								<td>${item.date}</td>
								<td>${item.time}</td>
								<td>${item.movie.name}</td>
								<td>${100-item.emptySeats}</td>
								<td>${(100-item.emptySeats)*item.ticketCost}</td>
							</tr>
					    </c:forEach>
				    </tbody>
			    </table>
			</c:if>
    	</c:if>
    	
    	<c:if test="${showEmployeeForm && account.accRole == \"director\"}">
	    	<h2><fmt:message key="employeeInfo" bundle="${bundle}"/>:<br></h2>
	    	<table class="table_sort">
					<thead>
						<tr>
							<th>ID</th>
							<th><fmt:message key="role" bundle="${bundle}"/></th>
							<th><fmt:message key="login" bundle="${bundle}"/></th>
							<th><fmt:message key="name" bundle="${bundle}"/></th>
							<th><fmt:message key="surname" bundle="${bundle}"/></th>
							<th><fmt:message key="email" bundle="${bundle}"/></th>
							<th><fmt:message key="phone" bundle="${bundle}"/></th>
							<th><fmt:message key="dismiss" bundle="${bundle}"/></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${employeeList}" var="item">
					    	<tr>
								<td>${item.id}</td>
								<td>${item.accRole}</td>
								<td>${item.login}</td>
								<td>${item.name}</td>
								<td>${item.surname}</td>
								<td>${item.email}</td>
								<td>${item.phoneNumber}</td>
								<td>
									<c:if test="${item.accRole == \"admin\"}">
									<form action="controller" method="post">
										<input type="hidden" name="operation" value="dismiss">
										<input type="hidden" name="loginToDismiss" value="${item.login}">
										<button style="background: #DAA520; width: 100%; height: 100%;"><fmt:message key="dismiss" bundle="${bundle}"/></button>
									</form>
									</c:if>
									<c:if test="${item.login == account.login}">
										<fmt:message key="itsYou" bundle="${bundle}"/>
									</c:if>
								</td>
							</tr>
					    </c:forEach>
				    </tbody>
			    </table>
    	</c:if>
    	
    	<c:if test="${giveAdminRightsForm && account.accRole == \"director\"}">
	    	<form action="controller" method="post">
					<h2><fmt:message key="enterLoginToGiveRights" bundle="${bundle}"/>:<br></h2>
					<input type="text" name="loginToAdmin" required><br>
					<input class="confirm_button" type="submit" value="<fmt:message key="giveAdminRights" bundle="${bundle}"/>">
					<input type="hidden" name="operation" value="giveAdminRights">
			</form>
    	</c:if>
</div>

</body>

</html>