<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>

<html>

<head>
  	<meta charset="utf-8">
  	<title> MyCinema ${movie.name} </title>
  	
  	<c:if test="${sessionScope.locale == null}">
        <fmt:setLocale value="en"/>
	</c:if>
    <c:if test="${sessionScope.locale != null}">
        <fmt:setLocale value="${sessionScope.locale}"/>
    </c:if>
    <fmt:setBundle basename="localization" var="bundle"/>
  
  	<link rel="stylesheet" href="CSS/headerStyle.css">
  	<link rel="stylesheet" href="CSS/sidebarAndContent(2).css">
  
  	<jsp:include page="JspParts/header.jsp"/>
  	
</head>

<body>
  
  	<div id="sidebar">
    	<img src="images/posters/${movie.posterUrl}" width="100%" height="100%" class="leftimg"/>
  	</div>
  	
  	<div id="content">
	    <h1> ${movie.name} </h1><br>
	    <h2><fmt:message key="genre" bundle="${bundle}"/>: <fmt:message key="${movie.genre}" bundle="${bundle}"/></h2>
	    <h2><fmt:message key="voice" bundle="${bundle}"/>: <fmt:message key="${movie.voiceActing}" bundle="${bundle}"/></h2>
	    <h2><fmt:message key="filmedIn" bundle="${bundle}"/>: <fmt:message key="${movie.filmedIn}" bundle="${bundle}"/></h2>
	    <h2><fmt:message key="duration" bundle="${bundle}"/>: ${movie.duration} <fmt:message key="minutes" bundle="${bundle}"/>.</h2>
	    <h2><fmt:message key="description" bundle="${bundle}"/>: </h2><h3> ${movie.fullDesc}</h3>
	    
	    <c:forEach items="${movieSessions}" var="item">
	    	<a href="controller?operation=session&sessionid=${item.id}">
	    		<button class="button2">
	    			${item.date}, ${item.time}, ${item.ticketCost} <fmt:message key="grivnas" bundle="${bundle}"/><br>
	    			<font color="#ADFF2F">${item.emptySeats}/100</font> <fmt:message key="seatsAvailable" bundle="${bundle}"/>!
	    		</button>
	    	</a>	
	    </c:forEach>
    
	</div>
	
</body>

</html>