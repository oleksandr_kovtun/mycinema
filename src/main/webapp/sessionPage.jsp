<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>

<html>

<head>
  	<meta charset="utf-8">
  	<title>MyCinema Session</title>
  	
  	<c:if test="${sessionScope.locale == null}">
        <fmt:setLocale value="en"/>
    </c:if>
    <c:if test="${sessionScope.locale != null}">
        <fmt:setLocale value="${sessionScope.locale}"/>
    </c:if>

    <fmt:setBundle basename="localization" var="bundle"/>
  	<link rel="stylesheet" href="CSS/headerStyle.css">
  	<link rel="stylesheet" href="CSS/sidebarAndContent(2).css">
  	
  	<jsp:include page="JspParts/header.jsp"/>
  
</head>
 
<body>
  
  	<div id="sidebar">
    	<img src="images/posters/${selectedSession.movie.posterUrl}" width="100%" height="100%" class="leftimg"/>	
  	</div>
  	
  	<div id="content">
	    <h1> ${selectedSession.movie.name}: ${selectedSession.date}, ${selectedSession.time}, ${selectedSession.ticketCost}₴.</h1>
	    <h2><fmt:message key="genre" bundle="${bundle}"/>: 
	    <fmt:message key="${selectedSession.movie.genre}" bundle="${bundle}"/></h2><br>
	    
	    <h2><fmt:message key="voice" bundle="${bundle}"/>: 
	    <fmt:message key="${selectedSession.movie.voiceActing}" bundle="${bundle}"/></h2><br>
	    
	    <h2><fmt:message key="filmedIn" bundle="${bundle}"/>:
	    <fmt:message key="${selectedSession.movie.filmedIn}" bundle="${bundle}"/></h2><br>
	    
	    <h2><fmt:message key="duration" bundle="${bundle}"/>: 
	    ${selectedSession.movie.duration} <fmt:message key="minutes" bundle="${bundle}"/>.</h2><br>
	    
	    <h2><fmt:message key="description" bundle="${bundle}"/>: <br> ${selectedSession.movie.fullDesc}</h2><br>
	    
	    <h2><fmt:message key="seatsAvailable" bundle="${bundle}"/>: <br> ${selectedSession.emptySeats}/100</h2><br>
	    
	    <a href="controller?operation=selectSeat&sessionid=${selectedSession.id}">
	    	<button class="button2" style="width: 95%;">
	    		<fmt:message key="selectSeat" bundle="${bundle}"/>
	    	</button>
	    </a>
	    
  	</div>
  	
</body>

</html>