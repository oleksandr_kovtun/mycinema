<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="myTag" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>

<html>

<head>

	<meta charset="utf-8">
  	<title>MyCinema Movies</title>
  	
  	
	<%-- Start of localization block --%>
  	<c:if test="${sessionScope.locale == null}">
        <fmt:setLocale value="en"/>
    </c:if>
    <c:if test="${sessionScope.locale != null}">
        <fmt:setLocale value="${sessionScope.locale}"/>
    </c:if>
	<fmt:setBundle basename="localization" var="bundle"/>
    <%-- End of localization block --%>
    
    <%-- CSS references --%>
	<link rel="stylesheet" href="CSS/headerStyle.css">
	<link rel="stylesheet" href="CSS/sidebarAndContent(1).css">

	<%-- Header include --%>
	<jsp:include page="JspParts/header.jsp"/>
	
	
 </head>
 
 <body>
  	<div id="sidebar">
    
    	<%-- Start of "order by" button group --%>
	    <div class="btn-group">
	    	<div style="text-align: center;">
	    		<font size="5" color="black"><fmt:message key="orderBy" bundle="${bundle}"/></font>
	    	</div>
		    <a href="controller?operation=showMovies&page=0&sortCode=1">
			    <c:choose>
				    <c:when test="${sort==1}">
				       <button class="disabledbutton" disabled><fmt:message key="movieName" bundle="${bundle}"/></button>
				    </c:when>    
				    <c:otherwise>
				       <button class="button"><fmt:message key="movieName" bundle="${bundle}"/></button>
				    </c:otherwise>
				</c:choose>
			</a>
				
			<a href="controller?operation=showMovies&page=0&sortCode=4">
		    	<c:choose>
			    	<c:when test="${sort==4}">
				       <button class="disabledbutton" disabled><fmt:message key="sessionsCount" bundle="${bundle}"/></button>
				    </c:when>    
				    <c:otherwise>
				       <button class="button"><fmt:message key="sessionsCount" bundle="${bundle}"/></button>
				    </c:otherwise>
			    </c:choose>
			</a>
		    
		    <a href="controller?operation=showMovies&page=0&sortCode=2">
		    	<c:choose>
			    	<c:when test="${sort==2}">
				       <button class="disabledbutton" disabled><fmt:message key="voice" bundle="${bundle}"/></button>
				    </c:when>    
				    <c:otherwise>
				       <button class="button"><fmt:message key="voice" bundle="${bundle}"/></button>
				    </c:otherwise>
			    </c:choose>
			</a>
			    
		    <a href="controller?operation=showMovies&page=0&sortCode=3">
		    	<c:choose>
			    	<c:when test="${sort==3}">
				       <button class="disabledbutton" disabled><fmt:message key="genre" bundle="${bundle}"/></button>
				    </c:when>    
				    <c:otherwise>
				       <button class="button"><fmt:message key="genre" bundle="${bundle}"/></button>
				    </c:otherwise>
			    </c:choose>
			</a>
	    </div><hr>
	    <%-- End of "order by" button group --%>
	    
	    <%-- Start of "object on page" button group --%>
	    <div class="btn-group">
	    	<div style="text-align: center;">
				<font size="5" color="black"><fmt:message key="objOnPage" bundle="${bundle}"/>: 
					<c:choose>
					<c:when test="${limit == null}">5</c:when>
					<c:otherwise>
						${limit}
					</c:otherwise>
					</c:choose>
				</font>
			</div>
			<form action="controller">
				<input name="objLimit" type="range" min="1" max="10" step="1" value="${limit}" style="width: 100%;"> <br>
				<input class="button" type="submit" value="<fmt:message key="confirm" bundle="${bundle}"/>">
				<input type="hidden" name="operation" value="showMovies">
				<input type="hidden" name="sortCode" value="${sort}">
				<input type="hidden" name="page" value="0">
			</form>
		</div>
		<%-- End of "object on page" button group --%>
	</div>
	
  	<div id="content">
  		
  		<%-- Start movies show --%>
	    <c:forEach items="${movies}" var="item">
	    	<c:if test="${item.sessionsAvailable != 0 }">
		    	<div style="min-height: 460px;">
			    	<div class="leftimg">
				    	<a href="controller?operation=movie&movieid=${item.id}">
				    		<img src="images/posters/${item.posterUrl}" width="250" />
				    		<button class="button2">${item.sessionsAvailable}<br><fmt:message key="sessionsAvailable" bundle="${bundle}"/></button>
				    	</a>
			    	</div>
			    	<h1>${item.name}</h1><br>
			    	<h2><fmt:message key="genre" bundle="${bundle}"/>: <fmt:message key="${item.genre}" bundle="${bundle}"/></h2><br>
			    	<h2><fmt:message key="voice" bundle="${bundle}"/>: <fmt:message key="${item.voiceActing}" bundle="${bundle}"/></h2><br>
			    	<h2><fmt:message key="filmedIn" bundle="${bundle}"/>: <fmt:message key="${item.filmedIn}" bundle="${bundle}"/></h2><br>
			    	<h2><fmt:message key="duration" bundle="${bundle}"/>: ${item.duration} <fmt:message key="minutes" bundle="${bundle}"/>.</h2><br>
			    	<h2><fmt:message key="description" bundle="${bundle}"/>: <br> ${item.shortDesc}</h2><br>
		    	</div><hr>
	    	</c:if>
		</c:forEach>
		<%-- End movies show --%>
	</div>
	
  	<div style="text-align:center;">
  
  		<%-- Navigation bar (pagination) --%>
  		<myTag:navbar lastNum="${lastNum}" operation="showMovies" firstNum="${firstNum}" orderParamName="sortCode"
  				 currentPage="${currentPage}" totalPages="${totalPages}" orderBy="${sort}" />
  
	</div>
</body>

</html>