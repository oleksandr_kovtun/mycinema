<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>

<head>
  	<meta charset="utf-8">
 	<title>MyCinema Log In</title>
  
 	<c:if test="${sessionScope.locale == null}">
        <fmt:setLocale value="en"/>
    </c:if>
    <c:if test="${sessionScope.locale != null}">
        <fmt:setLocale value="${sessionScope.locale}"/>
    </c:if>
    <fmt:setBundle basename="localization" var="bundle"/>
    
    <link rel="stylesheet" href="CSS/loginForm.css">
</head>



<body>

 	<c:if test="${account != null }">
		<c:redirect url="controller?operation=showMovies"/>
	</c:if>
	
	<div id="centerLayer">
		<form action="controller" method="post">
  		
  			<br>
  			<div style="text-align: center;">
  				<font size="5">
  					<b><fmt:message key="enterDetails" bundle="${bundle}"/></b>
  				</font>
  			</div>
  		
			
		  
		  	<div class="container">
		  		<font color="red" size="4">
				  	<c:if test="${trouble != null}">
				  		<fmt:message key="problem" bundle="${bundle}"/>:
					  	<c:choose>
					  		<c:when test="${trouble == 1}"><fmt:message key="problemWithServer" bundle="${bundle}"/></c:when>
					  		<c:when test="${trouble == 2}"><fmt:message key="thisAccNotExists" bundle="${bundle}"/></c:when>
					  		<c:when test="${trouble == 3}"><fmt:message key="wrongPass" bundle="${bundle}"/></c:when>
					  	</c:choose><br><br>
					</c:if>
				</font>
		  	
			    <label for="uname"><b><fmt:message key="login" bundle="${bundle}"/></b></label>
			    <input type="text" placeholder="<fmt:message key="enterLogin" bundle="${bundle}"/>" name="login" required>
			
			    <label for="psw"><b><fmt:message key="password" bundle="${bundle}"/></b></label>
			    <input type="password" placeholder="<fmt:message key="enterPassword" bundle="${bundle}"/>" name="password" required>
			        
			    <button type="submit"><fmt:message key="LogIn" bundle="${bundle}"/></button>
			    <input type="hidden" name="operation" value="login">	
		 	</div>
		
		  	<div class="container">
		    	<a href="controller?operation=showMovies"><button type="button" class="cancelbtn"><fmt:message key="cancel" bundle="${bundle}"/></button></a>
		    	<span class="psw"><fmt:message key="dontHaveAcc" bundle="${bundle}"/>?&nbsp;<a href="register.jsp"><fmt:message key="register" bundle="${bundle}"/></a></span>
		  	</div>
		</form>
	</div>
</body>

</html>