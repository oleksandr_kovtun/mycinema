<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>MyCinema 404</title>
</head>
<body>
	<div style="text-align: center; margin-top: 15vh;">
		<font size=7 color="#191970" face="COMIC SANS MS"><big><big><b>ERROR 404</b></big></big></font><br>
		<img src="${pageContext.request.contextPath}/images/error.jpg" height=400px /><br>
		<font size=6 color="#191970" face="COMIC SANS MS">Sorry, but there is no such page, or it is not available at the moment!</font><br>
	</div>
</body>
</html>