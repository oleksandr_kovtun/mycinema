<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>MyCinema 500</title>
</head>
<body>
	<div style="text-align: center; margin-top: 15vh;">
		<font size=7 color="#191970" face="COMIC SANS MS"><big><big><b>ERROR 500</b></big></big></font><br>
		<img src="${pageContext.request.contextPath}/images/error.jpg" height=400px /><br>
		<font size=6 color="#191970" face="COMIC SANS MS">Sorry, there are some problems on our side. We are doing our best to fix the problem. 
		<br>Please try a little later.</font><br>
	</div>
</body>
</html>