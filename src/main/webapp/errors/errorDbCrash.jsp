<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>MyCinema DB error</title>
</head>
<body>
	<div style="text-align: center; margin-top: 15vh;">
		<font size=7 color="#191970" face="COMIC SANS MS"><big><big><b>DATA BASE ERROR</b></big></big></font><br>
		<img src="${pageContext.request.contextPath}/images/error.jpg" height=400px /><br>
		<font size=6 color="#191970" face="COMIC SANS MS">Sorry, but we can't get the correct data right now. We are working to fix the problem!</font><br>
	</div>
</body>
</html>