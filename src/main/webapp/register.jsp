<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>

<head>
  	<meta charset="utf-8">
  	<title>MyCinema Register</title>
  	
  	<c:if test="${sessionScope.locale == null}">
        <fmt:setLocale value="en"/>
    </c:if>
    <c:if test="${sessionScope.locale != null}">
        <fmt:setLocale value="${sessionScope.locale}"/>
    </c:if>
    <fmt:setBundle basename="localization" var="bundle"/>
    
	<link rel="stylesheet" href="CSS/loginForm.css">
</head>

<body>
 	
  	<div id="centerLayer">
		<c:if test="${account != null }">
			<c:redirect url="controller?operation=showMovies"/>
		</c:if>
  
		<form action="controller" method="post"><br>
	  		<div style="text-align: center;">
	  			<font size="5">
	  				<b><fmt:message key="enterDetails" bundle="${bundle}"/></b>
	  			</font>
	  		</div>
		
			<div class="container">
				<font color="red" size="4">
			  		<c:if test="${trouble != null}">
			 			<fmt:message key="problem" bundle="${bundle}"/>:
					  	<c:choose>
					  		<c:when test="${trouble == 4}"><fmt:message key="accountAlreadyExists" bundle="${bundle}"/></c:when>
					  		<c:when test="${trouble == 5}"><fmt:message key="passwordsNotEquals" bundle="${bundle}"/></c:when>
					  		<c:when test="${trouble == 6}"><fmt:message key="invalidDetails" bundle="${bundle}"/></c:when>
					  		<c:when test="${trouble == 7}"><fmt:message key="problemWithServer" bundle="${bundle}"/></c:when>
					  	</c:choose><br><br>
			  		</c:if>
		  		</font>
		  		
			    <label for="uname"><b>
			    	<fmt:message key="login" bundle="${bundle}"/> 
			    	<fmt:message key="condition.4ch" bundle="${bundle}"/></b>
			    </label>
			    <input type="text" placeholder="<fmt:message key="enterLogin" bundle="${bundle}"/>" name="login" required>
			
			    <label for="psw"><b>
			    	<fmt:message key="password" bundle="${bundle}"/> 
			    	<fmt:message key="condition.4ch" bundle="${bundle}"/></b>
			    </label>
			    <input type="password" placeholder="<fmt:message key="enterPassword" bundle="${bundle}"/>" name="password" required>
			    
			    <label for="psw"><b>
			    	<fmt:message key="repeatPass" bundle="${bundle}"/></b>
			    </label>
			    <input type="password" placeholder="<fmt:message key="repeatPass" bundle="${bundle}"/>" name="repPassword" required>
			    
			    <label for="uname"><b>
			    	<fmt:message key="email" bundle="${bundle}"/></b>
			    </label>
			    <input type="email" placeholder="<fmt:message key="enterEmail" bundle="${bundle}"/>l" name="email" required>
			    
			    <label for="uname"><b>
			    	<fmt:message key="name" bundle="${bundle}"/> <fmt:message key="notRequired" bundle="${bundle}"/></b>
			    </label>
			    <input type="text" placeholder="<fmt:message key="enterName" bundle="${bundle}"/>" name="accName">
			    
			    <label for="uname"><b>
			    	<fmt:message key="surname" bundle="${bundle}"/> <fmt:message key="notRequired" bundle="${bundle}"/></b>
			    </label>
			    <input type="text" placeholder="<fmt:message key="enterSurname" bundle="${bundle}"/>" name="surname">
			    
			    <label for="uname"><b>
					<fmt:message key="phone" bundle="${bundle}"/> <fmt:message key="notRequired" bundle="${bundle}"/></b>
				</label>
			    <input type="tel" placeholder="<fmt:message key="enterPhone" bundle="${bundle}"/>" name="number">
			     
			    <button type="submit">
			    	<fmt:message key="register" bundle="${bundle}"/>
			    </button>
			    <input type="hidden" name="operation" value="register">
		 	</div>
		
		  	<div class="container">
		    	<a href="controller?operation=showMovies">
		    		<button type="button" class="cancelbtn">
		    			<fmt:message key="cancel" bundle="${bundle}"/>
		    		</button>
		    	</a>
		  	</div>
		</form>
	
 	</div>
</body>

</html>