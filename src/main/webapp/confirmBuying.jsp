<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>

<html>

<head>
  <meta charset="utf-8">
  <title>MyCinema Confirm purchase</title>
  
  <c:if test="${sessionScope.locale == null}">
     <fmt:setLocale value="en"/>
  </c:if>
  <c:if test="${sessionScope.locale != null}">
     <fmt:setLocale value="${sessionScope.locale}"/>
  </c:if>
  <fmt:setBundle basename="localization" var="bundle"/>
    
  <link rel="stylesheet" href="CSS/receiptForm.css">
  
  <c:if test="${account == null }">
	 <c:redirect url="login.jsp"/>
  </c:if>

</head>
	 
<body>

 	<h2>
  	<div id="centerLayer">
		<div align="center">
			<fmt:message key="confirmBuying" bundle="${bundle}"/><p>
			<div style="letter-spacing: 3px;">
				<font size=6 color="#800000" face="mv boli">${selectedSession.movie.name}</font>
			</div>
		</div><br>
		<fmt:message key="date" bundle="${bundle}"/>: ${selectedSession.date},<br>
		<fmt:message key="time" bundle="${bundle}"/>: ${selectedSession.time}
		<p>Total tickets: ${fn:length(buySeat)}<br>
		<div class="ticketsWindow">
			<c:forEach items="${buySeat}" var="item" varStatus="rowCounter">
	    		&nbsp;${rowCounter.count}. 
	    		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="line" bundle="${bundle}"/> 
	    		<fmt:formatNumber value="${item / 10 + 1}" maxFractionDigits="0"/>, 
	    		<fmt:message key="seat" bundle="${bundle}"/> ${item % 10 + 1};<br>
			</c:forEach>
		</div>
		
		<p>Total cost: ${fn:length(buySeat) * selectedSession.ticketCost} <fmt:message key="grivnas" bundle="${bundle}"/>
		
		<form action="controller" method="post">
			<input class="button" type="submit" value="Confirm">
			<input type="hidden" name="operation" value="confirmedBuying">
		</form>
		
		<a href="controller?operation=selectSeat&sessionid=${selectedSession.id}">
			<button class="cancelbtn"><b>
				<fmt:message key="cancel" bundle="${bundle}"/></b>
			</button>
		</a>
		
  	</div>
  	</h2>
  	
</body>

</html>