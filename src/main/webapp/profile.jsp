<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix = "myTldTag" uri = "WEB-INF/custom.tld"%>

<!DOCTYPE html>

<html>
<head>
  	<meta charset="utf-8">
  	<title>MyCinema Profile</title>
  
  	<c:if test="${sessionScope.locale == null}">
        <fmt:setLocale value="en"/>
    </c:if>
    <c:if test="${sessionScope.locale != null}">
        <fmt:setLocale value="${sessionScope.locale}"/>
    </c:if>
    <fmt:setBundle basename="localization" var="bundle"/>
    
  	<link rel="stylesheet" href="CSS/headerStyle.css">
  	<link rel="stylesheet" href="CSS/profileStyle.css">

	<jsp:include page="JspParts/header.jsp"/>
	
</head>

<body>
  
  	<div id="sidebar">
  		<div class="btn-group">
  			<a href="controller?operation=showTickets">
			    <c:choose>
				    <c:when test="${todayTickets != null}">
				       <button class="disabledbutton" disabled><fmt:message key="showTickets" bundle="${bundle}"/></button>
				    </c:when>    
				    <c:otherwise>
				       <button class="button"><fmt:message key="showTickets" bundle="${bundle}"/></button>
				    </c:otherwise>
				</c:choose>
			</a>
			
			<a href="controller?operation=changePassForm">
			    <c:choose>
				    <c:when test="${changePassForm}">
				       <button class="disabledbutton" disabled><fmt:message key="changePass" bundle="${bundle}"/></button>
				    </c:when>    
				    <c:otherwise>
				       <button class="button"><fmt:message key="changePass" bundle="${bundle}"/></button>
				    </c:otherwise>
				</c:choose>
			</a>
			
			<a href="controller?operation=changeInfoForm">
			    <c:choose>
				    <c:when test="${fullAcc != null}">
				       <button class="disabledbutton" disabled><fmt:message key="changeAccInfo" bundle="${bundle}"/></button>
				    </c:when>    
				    <c:otherwise>
				       <button class="button"><fmt:message key="changeAccInfo" bundle="${bundle}"/></button>
				    </c:otherwise>
				</c:choose>
			</a>
  		</div>
  
  	</div>
  	
  	<div id="content">
  	
    	<h1>
    		<fmt:message key="myProfile" bundle="${bundle}"/>: 
    		<font color="SandyBrown" face="mv boli"><b>${account.login}</b></font>
    	</h1><hr>
    
    
    	<font color="green" size="5">
	  		<c:if test="${notify != null}">
			  	<div class="notifySpace">
				  	<fmt:message key="notification" bundle="${bundle}"/>:
				  	<c:choose>
				  		<c:when test="${notify == 1}"><fmt:message key="passwordChanged" bundle="${bundle}"/></c:when>
				  		<c:when test="${notify == 2}"><fmt:message key="infoChanged" bundle="${bundle}"/></c:when>
				  		<c:when test="${notify == 3}"><fmt:message key="movieDeleted" bundle="${bundle}"/></c:when>
				  		<c:when test="${notify == 4}"><fmt:message key="sessionDeleted" bundle="${bundle}"/></c:when>
				  	</c:choose>
			  	</div>
	 		</c:if>
  		</font>
  	
  		<font color="red" size="5">
	  		<c:if test="${trouble != null}">
	  			<div class="troubleSpace">
				  	<fmt:message key="problem" bundle="${bundle}"/>:
				  	<c:choose>
				  		<c:when test="${trouble == 0}"><fmt:message key="passwordInvalid" bundle="${bundle}"/></c:when>
				  		<c:when test="${trouble == 1}"><fmt:message key="problemWithFile" bundle="${bundle}"/></c:when>
				  		<c:when test="${trouble == 2}"><fmt:message key="passwordsNotEquals" bundle="${bundle}"/></c:when>
				  		<c:when test="${trouble == 3}"><fmt:message key="problemWithServer" bundle="${bundle}"/></c:when>
				  		<c:when test="${trouble == 4}"><fmt:message key="invalidDetails" bundle="${bundle}"/></c:when>
				  	</c:choose>
			  	</div>
	 		 </c:if>
  		</font>
  
    
    	<c:if test="${todayTickets != null}">
	    	<h2><fmt:message key="myTickets" bundle="${bundle}"/>:</h2>
	    	<details open>
	    		<summary>
	    			<font size=5><fmt:message key="todayTickets" bundle="${bundle}"/>:</font>
	    		</summary><br>
		    		<c:forEach items="${todayTickets}" var="item">
			    		<div class="ticketForm todayTicket">${item.movieName} <br>
			    			${item.time}, ${item.date} | <fmt:message key="line" bundle="${bundle}"/> 
			    			<fmt:formatNumber value="${item.seat / 10 + 1}" maxFractionDigits="0"/>, 
			    			<fmt:message key="seat" bundle="${bundle}"/> ${item.seat % 10 + 1}
			    		</div>
		    		</c:forEach>
	    			<c:if test="${fn:length(todayTickets) == 0}">
	  					<h2><fmt:message key="noTodayTickets" bundle="${bundle}"/></h2>
					</c:if>
	    	</details><br>
	    	
	    	<details open>
	    		<summary>
	    			<font size=5><fmt:message key="futureTickets" bundle="${bundle}"/>:</font>
	    		</summary><br>
		    		<c:forEach items="${futureTickets}" var="item">
		    			<div class="ticketForm futureTicket">${item.movieName} <br>
		    				${item.time}, ${item.date} | <fmt:message key="line" bundle="${bundle}"/> 
		    				<fmt:formatNumber value="${item.seat / 10 + 1}" maxFractionDigits="0"/>, 
		    				<fmt:message key="seat" bundle="${bundle}"/> ${item.seat % 10 + 1}
		    			</div>
		    		</c:forEach>
		    		<c:if test="${fn:length(futureTickets) == 0}">
		  				<h2><fmt:message key="noFutureTickets" bundle="${bundle}"/></h2>
					</c:if>
	    	</details><br>
			
			<details>
	    		<summary>
	    			<font size=5><fmt:message key="oldTickets" bundle="${bundle}"/>:</font>
	    		</summary><br>
		    		<c:forEach items="${notActualTickets}" var="item">
		    			<div class="ticketForm oldTicket">${item.movieName} <br>
		    				${item.time}, ${item.date} | <fmt:message key="line" bundle="${bundle}"/> 
		    				<fmt:formatNumber value="${item.seat / 10 + 1}" maxFractionDigits="0"/>, 
		    				<fmt:message key="seat" bundle="${bundle}"/> ${item.seat % 10 + 1}
		    			</div>
		    		</c:forEach>
		    		<c:if test="${fn:length(notActualTickets) == 0}">
		  				<h2><fmt:message key="noOldTickets" bundle="${bundle}"/></h2>
					</c:if>
	    	</details><br>
    	</c:if>
    	
    	<c:if test="${changePassForm}">
	    	<form action="controller" method="post">
	    		<h2><fmt:message key="fillChangPassForm" bundle="${bundle}"/></h2>
	    		
	    		<fmt:message key="oldPass" bundle="${bundle}"/>:<br>
	    		<input type="password" placeholder="<fmt:message key="enterOldPass" bundle="${bundle}"/>" name="oldPass" required><br><br>
	    		
	    		<fmt:message key="newPass" bundle="${bundle}"/>:<br>
	    		<input type="password" placeholder="<fmt:message key="enterNewPass" bundle="${bundle}"/>" name="newPass" required><br><br>
	    		
	    		<fmt:message key="repeatNewPass" bundle="${bundle}"/>:<br>
	    		<input type="password" placeholder="<fmt:message key="enterRepeatNewPass" bundle="${bundle}"/>" name="repeatPass" required><br><br>
	    		
	    		<button class="confirm_button">
	    			<fmt:message key="changePass" bundle="${bundle}"/>
	    		</button>
				<input type="hidden" name="operation" value="changePass">
	    	</form>
    	</c:if>
    	
    	<c:if test="${fullAcc != null}">
			 
			<script src="JS/profileInfoScript.js"></script>
    		<form action="controller">
	    		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    		<fmt:message key="login" bundle="${bundle}"/>: <br>
	    		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    		<input type="text" value="${fullAcc.login}" disabled><br>
	    		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    		<fmt:message key="name" bundle="${bundle}"/>: <br>
	    		<input type="checkbox" name="changeName" value="true" onclick="agreeForm(this.form)">
	    		<input type="text" name="chName" placeholder="${fullAcc.name}" disabled><br>
	    		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    		<fmt:message key="surname" bundle="${bundle}"/>: <br>
	    		<input type="checkbox" name="changeSurname" value="true" onclick="agreeForm(this.form)">
	    		<input type="text" name="chSurname" placeholder="${fullAcc.surname}" disabled><br>
	    		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="email" bundle="${bundle}"/>: <br>
	    		<input type="checkbox" name="changeEmail" value="true" onclick="agreeForm(this.form)">
	    		<input type="email" name="chEmail" placeholder="${fullAcc.email}" disabled><br>
	    		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="phone" bundle="${bundle}"/>: <br>
	    		<input type="checkbox" name="changeNumber" value="true" onclick="agreeForm(this.form)">
	    		<input type="tel" name="chNumber" placeholder="${fullAcc.phoneNumber}" disabled><br>
	    		&nbsp;&nbsp;&nbsp;&nbsp;
	    		<button class="confirm_button" name="changeInfo" disabled>
	    			<fmt:message key="changeInfo" bundle="${bundle}"/>
	    		</button>
	    		<input type="hidden" name="operation" value="changeAccInfo">
    		</form>
    	</c:if>
  	</div>
</body>

</html>