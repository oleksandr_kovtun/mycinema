
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ attribute name="operation" required="true" %>
<%@ attribute name="orderParamName" required="true" %>

<%@ attribute name="currentPage" type="java.lang.Integer" required="true" %>
<%@ attribute name="orderBy" type="java.lang.Integer" required="true" %>
<%@ attribute name="firstNum" type="java.lang.Integer" required="true" %>
<%@ attribute name="lastNum" type="java.lang.Integer" required="true" %>
<%@ attribute name="totalPages" type="java.lang.Integer" required="true" %>

<c:if test="${sessionScope.locale == null}">
        <fmt:setLocale value="en"/>
    </c:if>
    <c:if test="${sessionScope.locale != null}">
        <fmt:setLocale value="${sessionScope.locale}"/>
    </c:if>

    <fmt:setBundle basename="localization" var="bundle"/>

<a href="controller?operation=${operation}&page=0&${orderParamName}=${orderBy}">
		<c:choose>
			<c:when test="${currentPage==0}">
				<button class="pagination_button button1disabled" disabled><fmt:message key="firstPage" bundle="${bundle}"/></button>
			</c:when>    
			<c:otherwise>
				<button class="pagination_button button1"><fmt:message key="firstPage" bundle="${bundle}"/></button>
			</c:otherwise>
		</c:choose></a>
		
		<a href="controller?operation=${operation}&page=${currentPage-1}&${orderParamName}=${orderBy}">
		<c:choose>
			<c:when test="${currentPage==0}">
				<button class="pagination_button button1disabled" disabled><fmt:message key="prevPage" bundle="${bundle}"/></button>
			</c:when>    
			<c:otherwise>
				<button class="pagination_button button1"><fmt:message key="prevPage" bundle="${bundle}"/></button>
			</c:otherwise>
		</c:choose></a>
		
	<c:forEach begin="${firstNum}" end="${lastNum}" items="${pages}" var="item">
		<a href="controller?operation=${operation}&page=${item}&${orderParamName}=${orderBy}">
		<c:choose>
			<c:when test="${item==currentPage}">
				<button class="pagination_button button1disabled" disabled>${item + 1}</button>
			</c:when>    
			<c:otherwise>
				<button class="pagination_button button1">${item + 1}</button>
			</c:otherwise>
		</c:choose></a>
	</c:forEach>
	
		<a href="controller?operation=${operation}&page=${currentPage+1}&${orderParamName}=${orderBy}">
		<c:choose>
			<c:when test="${currentPage+1==totalPages}">
				<button class="pagination_button button1disabled" disabled><fmt:message key="nextPage" bundle="${bundle}"/></button>
			</c:when>    
			<c:otherwise>
				<button class="pagination_button button1"><fmt:message key="nextPage" bundle="${bundle}"/></button>
			</c:otherwise>
		</c:choose></a>
		
		<a href="controller?operation=${operation}&page=${totalPages - 1}&${orderParamName}=${orderBy}">
		<c:choose>
			<c:when test="${currentPage==totalPages-1}">
				<button class="pagination_button button1disabled" disabled><fmt:message key="lastPage" bundle="${bundle}"/></button>
			</c:when>    
			<c:otherwise>
				<button class="pagination_button button1"><fmt:message key="lastPage" bundle="${bundle}"/></button>
			</c:otherwise>
		</c:choose></a>