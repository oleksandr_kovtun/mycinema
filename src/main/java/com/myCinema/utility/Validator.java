package com.myCinema.utility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class for checking validity
 *
 * @author Oleksandr Kovtun
 */
public class Validator {

    /**
     * Method for checking validity of common fields (login, password and other).
     *
     * @param input - string, which must be checked
     * @return true, if length of input string is more or equal - 4 characters.
     */
    public static boolean commonFieldIsValid(String input) {
        return input.length() > 3;
    }

    /**
     * Method will checking validity of email address.
     * Email must to contain text (can be lower, upper case and numbers) before '@',
     * text between '@' and dot(.) only in lower case
     * and text after dot(.) only in lower case.
     *
     * @param email - input string with email address
     * @return true, if email is valid
     */
    public static boolean emailIsValid(String email) {
        Pattern p = Pattern.compile("[a-zA-Z0-9]+?@[a-z]+?\\.[a-z]+");
        Matcher m = p.matcher(email);
        return m.find();
    }

    /**
     * Method will checking validity of phone number.
     * Phone number must to contain numbers (0-9), length diapason: from 7 to 12 numbers.
     * Input can to contain next symbols, which will not be consider: 'spaces' + ( )
     *
     * @param phoneNumber - input string with phone number
     * @return true, if phone number is valid
     */
    public static boolean phoneIsValid(String phoneNumber) {
        Pattern p = Pattern.compile("\\b[0-9]{7,12}\\b");
        String phoneWithoutSpaces = phoneNumber.replaceAll("[\\s()+]", "");
        Matcher m = p.matcher(phoneWithoutSpaces);
        return m.find();
    }

    /**
     * Method will return phone number without symbols [+ ( )]
     * Phone number must to contain numbers (0-9), length diapason: from 7 to 12 numbers.
     * Input can to contain next symbols, which will not be consider: 'spaces' + ( )
     *
     * @param phone - input string with phone number
     * @return phone number, if it is valid. Else return "Phone number is invalid!"
     */
    public static String validatePhone(String phone) {
        Pattern p = Pattern.compile("\\b[0-9]{7,12}\\b");
        String phoneWithoutSpaces = phone.replaceAll("[\\s()+]", "");
        Matcher m = p.matcher(phoneWithoutSpaces);
        if (m.find()) {
            return m.group();
        }
        return "Phone number is invalid!";
    }
}
