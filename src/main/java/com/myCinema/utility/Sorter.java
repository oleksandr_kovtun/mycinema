package com.myCinema.utility;

import com.myCinema.model.Movie;

import java.util.Comparator;
import java.util.List;

/**
 * Class for sorting movie collections.
 *
 * @author Oleksandr Kovtun
 */
public class Sorter {

    /**
     * Method will sort movies by name in alphabet order.
     *
     * @param movies - list, which contain Movie entities inside
     */
    public static void sortMovieByName(List<Movie> movies) {
    	movies.sort(SORT_MOVIES_BY_NAME);
    }

    /**
     * Method will sort movies by value of voiceActing field in alphabet order.
     *
     * @param movies - list, which contain Movie entities inside
     */
    public static void sortMovieByVoiceActing(List<Movie> movies) {
    	movies.sort(SORT_MOVIES_BY_VOICE_ACTING);
    }

    /**
     * Method will sort movies by value of genre field in alphabet order.
     *
     * @param movies - list, which contain Movie entities inside
     */
    public static void sortMovieByGenre(List<Movie> movies) {
        movies.sort(SORT_MOVIES_BY_GENRE);
    }

    /**
     * Method will sort movies by actual count of sessions, which have this movie.
     * Order: movies with most sessions first.
     *
     *
     * @param movies - list, which contain Movie entities inside
     */
    public static void sortMovieBySessionCount(List<Movie> movies) {
        movies.sort(SORT_MOVIES_BY_SESSION_COUNT);
    }

    /**
     * Comparator implementation for movie sorting by name.
     */
    private static final Comparator<Movie> SORT_MOVIES_BY_NAME = Comparator.comparing(Movie::getName);

    /**
     * Comparator implementation for movie sorting by voiceActing field.
     */
    private static final Comparator<Movie> SORT_MOVIES_BY_VOICE_ACTING = Comparator.comparing(Movie::getVoiceActing);

    /**
     * Comparator implementation for movie sorting by genre.
     */
    private static final Comparator<Movie> SORT_MOVIES_BY_GENRE = Comparator.comparing(Movie::getGenre);

    /**
     * Comparator implementation for movie sorting by count of sessions.
     */
    private static final Comparator<Movie> SORT_MOVIES_BY_SESSION_COUNT = Comparator.comparing(Movie::getSessionsAvailable).reversed();
    
}
