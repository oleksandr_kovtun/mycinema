package com.myCinema.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Class designed to date or time parsing, and getting current date in SQL format.
 *
 * @author Oleksandr Kovtun
 */
public class DateManager {

    /**
     * Method will generate current date string, which suit to insert in SQL Data Base.
     *
     * @return string with current date. Format - "yyyy-MM-dd"
     */
    public static String currentDateForSql() {
        Date date = new Date();
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("yyyy-MM-dd");

        return formatForDateNow.format(date);
    }

    /**
     * Method will parse date to classic format from SQL.
     *
     * @param date - string with date in SQL format (yyyy-MM-dd)
     * @return string with date in classic format (dd.MM.yyyy)
     */
    public static String parseDate (String date) {

        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        Date parsingDate = null;
        try {
            parsingDate = ft.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ft.applyPattern("dd.MM.yyyy");

        return ft.format(parsingDate);
    }

    /**
     * Method will parse date to format without seconds from SQL.
     *
     * @param time - string with time in format (HH:mm:ss)
     * @return string with time in format (HH:mm)
     */
    public static String parseTime (String time) {
        SimpleDateFormat ft = new SimpleDateFormat ("HH:mm:ss");
        Date parsingTime = null;
        try {
            parsingTime = ft.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ft.applyPattern("HH:mm");

        return ft.format(parsingTime);
    }
}
