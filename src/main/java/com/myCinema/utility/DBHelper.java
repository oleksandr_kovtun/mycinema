package com.myCinema.utility;

import com.myCinema.model.Account;
import com.myCinema.model.Movie;
import com.myCinema.model.Session;
import com.myCinema.model.Ticket;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * This class contains multiple repetitive operations in DAO implementation classes.
 *
 * @author Oleksandr Kovtun
 */
public class DBHelper {

    /**
     * Method interprets data from ResultSet to entity Account, without fields - name, surname and phone number.
     *
     * @param resultSet - ResultSet, which was called on statement, which get data from DB table 'account'
     * @return Account with actual data, without fields - name, surname and phone number
     * @throws SQLException, if resultSet don't contain required fields
     */
    public static Account fillAccount (ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String log = resultSet.getString("login");
        String pass = resultSet.getString("password");
        String email = resultSet.getString("email");
        String role = resultSet.getString("role");

        return new Account(id, log, pass, email, role);
    }

    /**
     * Method interprets data from ResultSet to entity Account, with fields - name, surname and phone number.
     *
     * @param resultSet - ResultSet, which was called on statement,
     *                  which get data from DB table 'account' and 'account_info'
     * @return Account with actual data
     * @throws SQLException, if resultSet don't contain required fields
     */
    public static Account fillAccountFullInfo (ResultSet resultSet) throws SQLException {
        Account acc = fillAccount(resultSet);

        String name = resultSet.getString("name");
        String surname = resultSet.getString("surname");
        String phoneNumber = resultSet.getString("phone_number");
        acc.setName(name);
        acc.setSurname(surname);
        acc.setPhoneNumber(phoneNumber);

        return acc;
    }

    /**
     * Method interprets data from ResultSet to entity Movie.
     *
     * @param resultSet - ResultSet, which was called on statement, which get data from DB table 'movie'
     * @return Movie with actual data
     * @throws SQLException, if resultSet don't contain required fields
     */
    public static Movie fillMovie (ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("movie.id");
        String name = resultSet.getString("name");
        String poster = resultSet.getString("poster");
        String genre = resultSet.getString("genre");
        String voiceActing = resultSet.getString("voice_acting");
        String filmedIn = resultSet.getString("filmed_in");
        int duration = resultSet.getInt("duration");
        String shDesc = resultSet.getString("short_desc");
        String desc = resultSet.getString("description");

        return new Movie(id, name, poster, genre, voiceActing, filmedIn, duration, shDesc, desc);
    }

    /**
     * Method interprets data from ResultSet to entity Session, with Movie data inside.
     *
     * @param resultSet - ResultSet, which was called on statement, which get data from DB table 'session' and 'movie'
     * @return Session with actual data
     * @throws SQLException, if resultSet don't contain required fields
     */
    public static Session fillSession (ResultSet resultSet) throws SQLException {
        Movie m = fillMovie(resultSet);
        int sesId = resultSet.getInt("session.id");
        String date = DateManager.parseDate(resultSet.getString("date"));
        String time = DateManager.parseTime(resultSet.getString("time"));
        int seatsOccupied = resultSet.getInt("seat_occupied");
        int ticketCost = resultSet.getInt("cost");

        return new Session(m, sesId, date, time, 100 - seatsOccupied, ticketCost);
    }

    /**
     * Method interprets data from ResultSet to entity Session, without Movie data inside.
     *
     * @param resultSet - ResultSet, which was called on statement, which get data from DB table 'session'
     * @return Session with actual data, without Movie
     * @throws SQLException, if resultSet don't contain required fields
     */
    public static Session fillSessionWithoutMovie (ResultSet resultSet) throws SQLException {
        int sesId = resultSet.getInt("session.id");
        String date = DateManager.parseDate(resultSet.getString("date"));
        String time = DateManager.parseTime(resultSet.getString("time"));
        int seatsOccupied = resultSet.getInt("seat_occupied");
        int ticketCost = resultSet.getInt("cost");

        return new Session(sesId, date, time, 100 - seatsOccupied, ticketCost);
    }

    /**
     * Method interprets data from ResultSet to entity Ticket, without movie name, date and time.
     *
     * @param resultSet - ResultSet, which was called on statement, which get data from DB table 'account_has_session'
     * @return Ticket with actual data, without movie name, date and time.
     * @throws SQLException, if resultSet don't contain required fields
     */
    public static Ticket fillMainInfoTicket (ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("account_id");
        int sesId = resultSet.getInt("session_id");
        int seat = resultSet.getInt("seat");

        return new Ticket(id, sesId, seat);
    }

    /**
     * Method interprets data from ResultSet to entity Ticket.
     *
     * @param resultSet - ResultSet, which was called on statement, which get data from DB table 'account_has_session'
     * @return Ticket with all actual data
     * @throws SQLException, if resultSet don't contain required fields
     */
    public static Ticket fillTicket (ResultSet resultSet) throws SQLException {
        Ticket t;

        int id = resultSet.getInt("account_id");
        int sesId = resultSet.getInt("session_id");
        int seat = resultSet.getInt("seat");
        String movieName = resultSet.getString("name");
        String date = DateManager.parseDate(resultSet.getString("date"));
        String time = DateManager.parseTime(resultSet.getString("time"));

        t = new Ticket(id, sesId, seat, movieName, date, time);

        t.setActual(resultSet.getString("date").compareTo(DateManager.currentDateForSql()) >= 0);

        return t;
    }

    /**
     * Method for closing connection.
     *
     * @param connection - object type Connection
     */
    public static void closeConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
    }

    /**
     * Method for closing statement.
     *
     * @param statement - object type Statement
     */
    public static void closeStatement (Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                //
            }
        }
    }

    /**
     * Method for closing result set.
     *
     * @param resultSet - object type ResultSet
     */
    public static void closeResultSet (ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                //
            }
        }
    }
}
