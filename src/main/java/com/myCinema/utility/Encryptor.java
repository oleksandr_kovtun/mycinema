package com.myCinema.utility;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Class designed to encrypt strings (passwords).
 *
 * @author Oleksandr Kovtun
 */
public class Encryptor {

    /**
     * This method will encrypt any string. Using MD5 hashing algorithm.
     *
     * @param input - string, which must be ecnrypted
     * @return string, which has 32 hex-numbers
     */
    public static String hash(String input) {
        MessageDigest digest;
        byte[] hash = {};
        try {
            digest = MessageDigest.getInstance("MD5");
            digest.update(input.getBytes());
            hash = digest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }


        StringBuilder result = new StringBuilder();
        for (byte b : hash) {
            String s1 = Integer.toHexString(b);
            String s2;
            if (s1.length() > 2) {
                s2 = s1.substring(s1.length() - 2);
            } else if (s1.length() == 1) {
                s2 = "0" + s1;
            } else {
                s2 = s1;
            }
            result.append(s2);
        }

        return result.toString().toUpperCase();
    }

    public static void main(String[] args) {
        System.out.print(hash("bigfan"));
    }
}
