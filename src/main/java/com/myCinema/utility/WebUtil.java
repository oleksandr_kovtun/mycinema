package com.myCinema.utility;

import javax.servlet.http.HttpServletRequest;

/**
 * Class for realization notifications on JSP.
 *
 * @author Oleksandr Kovtun
 */
public class WebUtil {

    /**
     * Method for clearing notification and trouble messages from session.
     *
     * @param request - object type HttpServletRequest
     */
    public static void clearSessionMessages(HttpServletRequest request) {
        request.getSession().removeAttribute("trouble");
        request.getSession().removeAttribute("notify");
    }

    /**
     * Method for adding trouble message code into session.
     *
     * @param request - object type HttpServletRequest
     */
    public static void setTroubleMessage(HttpServletRequest request, int messageCode) {
        request.getSession().setAttribute("trouble", messageCode);
    }

    /**
     * Method for adding notification code into session.
     *
     * @param request - object type HttpServletRequest
     */
    public static void setNotificationMessage(HttpServletRequest request, int messageCode) {
        request.getSession().setAttribute("notify", messageCode);
    }
}
