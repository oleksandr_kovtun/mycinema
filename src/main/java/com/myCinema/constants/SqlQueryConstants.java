package com.myCinema.constants;

/**
 * Enum contains all queries for statement in DAO.
 */
public enum SqlQueryConstants {

    // Account DAO
    FIND_ACC_BY_ID("SELECT * FROM account, account_role WHERE account.id=? AND role_id = account_role.id"),
    FIND_FULL_ACC_BY_ID("SELECT * FROM account, account_info, account_role WHERE account.id=? AND account_id=account.id AND role_id = account_role.id"),
    FIND_ACC_BY_LOGIN("SELECT * FROM account, account_role WHERE login=? AND role_id = account_role.id"),
    INSERT_ACC("INSERT INTO account (id, login, password, email) VALUES (DEFAULT, ?, ?, ?)"),
    INSERT_ACC_INFO("INSERT INTO account_info (account_id, name, surname, phone_number) VALUES (?, ?, ?, ?)"),
    UPDATE_PASS("UPDATE account SET password=? WHERE id=?"),
    UPDATE_EMAIL("UPDATE account SET email=? WHERE id=?"),
    UPDATE_NAME("UPDATE account_info SET name=? WHERE account_id=?"),
    UPDATE_SURNAME("UPDATE account_info SET surname=? WHERE account_id=?"),
    UPDATE_NUMBER("UPDATE account_info SET phone_number=? WHERE account_id=?"),
    UPDATE_ROLE("UPDATE account SET role_id = ? WHERE login = ?"),
    FIND_COUNT_BY_LOGIN("SELECT count(*) FROM account WHERE login = ?"),
    FIND_ADMIN_ACCOUNTS("SELECT * FROM account, account_info, account_role WHERE (role='director' OR role='admin') AND account_id=account.id AND role_id = account_role.id"),

    // Movie DAO
    FIND_ALL_MOVIES("SELECT * FROM movie"),
    FIND_MOVIE_BY_ID("SELECT * FROM movie WHERE id=?"),
    INSERT_MOVIE("INSERT INTO movie (name, poster, genre, voice_acting, filmed_in, duration, short_desc, description) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"),
    DELETE_MOVIE_BY_ID("DELETE FROM movie WHERE id=?"),

    // Session DAO
    FIND_SESSION_BY_ID("SELECT * FROM session, movie WHERE session.id=? AND movie.id=session.movie_id"),
    FIND_ALL_ACTUAL_SESSIONS("SELECT * FROM session, movie WHERE movie.id=session.movie_id AND date >= ?"),
    FIND_ALL_SESSIONS_PAGINATION("SELECT * FROM session, movie WHERE movie.id=session.movie_id ORDER BY ^ LIMIT ? OFFSET ?"),
    FIND_ACTUAL_SESSIONS("SELECT * FROM session, movie WHERE movie.id=session.movie_id AND date >= ? ORDER BY ^ LIMIT ? OFFSET ?"),
    FIND_SESSIONS_BY_MOVIE_ID("SELECT * FROM session WHERE movie_id=? AND date >= ? ORDER BY date, time"),
    FIND_SESSIONS_BY_DATE("SELECT * FROM session, movie WHERE date=? AND movie.id=session.movie_id ORDER BY time"),
    FIND_SESSIONS_BY_MONTH("SELECT * FROM session, movie WHERE year(date) = ? AND month(date) = ? AND movie.id=session.movie_id ORDER BY date, time"),
    FIND_COUNT_OF_SESSIONS("SELECT COUNT(*) FROM session WHERE date >= ?"),
    FIND_COUNT_OF_SESSIONS_BY_MOVIE_ID("SELECT COUNT(*) FROM session WHERE date >= ? AND movie_id = ?"),
    INSERT_SESSION("INSERT INTO session(movie_id, date, time, cost) VALUES (?, ?, ?, ?)"),
    DELETE_SESSION_BY_ID("DELETE FROM session WHERE id = ?"),

    // Tickets DAO
    FIND_TICKETS_BY_ACCOUNT_ID("SELECT account_id, session_id, seat, name, date, time FROM account_has_session, session, movie WHERE account_id=? AND session.id = session_id AND movie.id = session.movie_id ORDER BY bought DESC"),
    FIND_TICKETS_BY_SESSION_ID("SELECT * FROM account_has_session WHERE session_id=?"),
    INSERT_TICKET("INSERT INTO account_has_session(account_id, session_id, seat) VALUES (?, ?, ?)");

    private final String value;

    /**
     * Constructor.
     * @param value - SQL query for prepared statement
     */
    SqlQueryConstants (String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
