package com.myCinema.constants;

/**
 * This enum contains paths to all web pages.
 */
public enum JspPages {

    // Forms
    LOG_IN("login.jsp"),
    REGISTER("register.jsp"),
    CONFIRM_BUYING("confirmBuying.jsp"),

    // Pages
    ADMINISTRATE("administration.jsp"),
    PROFILE("profile.jsp"),
    MOVIES("index.jsp"),
    MOVIE("moviePage.jsp"),
    SESSIONS("sessions.jsp"),
    SESSION("sessionPage.jsp"),
    SELECT_SEAT("selectSeat.jsp"),

    // Errors
    DB_ERROR("errors/errorDbCrash.jsp"),
    DB_ERROR_ADMIN("../errors/errorDbCrash.jsp"),

    // Redirect to controller
    SHOW_TICKETS("controller?operation=showTickets"),
    SHOW_MOVIES("controller?operation=showMovies"),
    CHANGE_ACC_INFO("controller?operation=changeInfoForm");

    private final String value;

    /**
     * Constructor.
     * @param value - relative path
     */
    JspPages (String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
