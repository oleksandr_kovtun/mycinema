package com.myCinema.service.movieService;

import com.myCinema.exception.DBException;
import com.myCinema.model.Movie;

import java.util.List;

/**
 * Basic service interface.
 * Provided operations with MovieDao.
 *
 * @author Oleksandr Kovtun
 */
public interface MovieService {

    /**
     * Getting list, which contains all movies.
     *
     * @return list of Movie entities
     * @throws DBException -
     */
    List<Movie> findAllMovies() throws DBException;

    /**
     * Getting movie by id.
     *
     * @param id - id of movie, which must be received
     * @return Movie entity
     * @throws DBException -
     */
    Movie findMovie(int id) throws DBException;

    /**
     * Creation new movie.
     *
     * @param movie - Movie entity, which must be created
     * @throws DBException -
     */
    void createNewMovie(Movie movie) throws DBException;

    /**
     * Removing movie by id.
     *
     * @param id - id of movie, which must be deleted
     * @throws DBException -
     */
    void removeMovie(int id) throws DBException;
}
