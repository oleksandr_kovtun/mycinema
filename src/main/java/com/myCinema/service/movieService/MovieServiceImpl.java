package com.myCinema.service.movieService;

import com.myCinema.connection.ConnectionPool;
import com.myCinema.dao.daoFactory.AbstractDaoFactory;
import com.myCinema.dao.daoFactory.DaoFactory;
import com.myCinema.dao.movieDao.MovieDao;
import com.myCinema.exception.DBException;
import com.myCinema.model.Movie;
import com.myCinema.utility.DBHelper;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Implementation of MovieService.
 * Provided operations with MovieDao.
 *
 * @author Oleksandr Kovtun
 */
public class MovieServiceImpl implements MovieService{

    MovieDao movieDao;
    ConnectionPool connectionPool;

    private static final Logger LOGGER = Logger.getLogger(MovieServiceImpl.class);

    /**
     * Constructor for getting connection pool and movie DAO objects.
     */
    public MovieServiceImpl() {
        DaoFactory daoFactory = AbstractDaoFactory.getDaoFactory();
        movieDao = daoFactory.getMovieDao();
        connectionPool = ConnectionPool.getInstance();
        LOGGER.debug("Movie service constructor worked!");
    }

    /**
     * Getting list, which contains all movies.
     * See also {@link MovieService#findAllMovies()}
     */
    @Override
    public List<Movie> findAllMovies() throws DBException {
        Connection con = connectionPool.getConnection();
        List<Movie> movies;
        try {
            LOGGER.info("Try to find all movies...");
            movies = movieDao.findAllMovies(con);
            LOGGER.info("All movies successfully founded!");
        } catch (SQLException e) {
            LOGGER.error("Searching all movies failed!", e);
            throw new DBException("Searching all movies failed!", e);
        } finally {
            DBHelper.closeConnection(con);
        }
        return movies;
    }

    /**
     * Getting movie by id.
     * See also {@link MovieService#findMovie(int)}
     */
    @Override
    public Movie findMovie(int id) throws DBException {
        Connection con = connectionPool.getConnection();
        Movie movie;
        try {
            LOGGER.info("Try to find movie (id = " + id + ")...");
            movie = movieDao.findMovieById(con, id);
            LOGGER.info("Movie (id = " + id + ") successfully founded!");
        } catch (SQLException e) {
            LOGGER.error("Searching movie by id failed!", e);
            throw new DBException("Searching movie by id failed!", e);
        } finally {
            DBHelper.closeConnection(con);
        }
        return movie;
    }

    /**
     * Creation new movie.
     * See also {@link MovieService#createNewMovie(Movie)}
     */
    @Override
    public void createNewMovie(Movie movie) throws DBException {
        Connection con = connectionPool.getConnection();
        try {
            LOGGER.info("Try to create new movie...");
            movieDao.insertMovie(con, movie);
            LOGGER.info("New movie successfully created!");
        } catch (SQLException e) {
            LOGGER.error("Creation new movie failed!", e);
            throw new DBException("Creation new movie failed!", e);
        } finally {
            DBHelper.closeConnection(con);
        }
    }

    /**
     * Removing movie by id.
     * See also {@link MovieService#removeMovie(int)}
     */
    @Override
    public void removeMovie(int id) throws DBException {
        Connection con = connectionPool.getConnection();
        try {
            LOGGER.info("Try to delete movie (id = " + id + ")...");
            movieDao.deleteMovieById(con, id);
            LOGGER.info("Movie (id = " + id + ") successfully deleted!");
        } catch (SQLException e) {
            LOGGER.error("Deleting movie (id = " + id + ") failed!", e);
            throw new DBException("Deleting movie (id = " + id + ") failed!", e);
        } finally {
            DBHelper.closeConnection(con);
        }
    }
}
