package com.myCinema.service.ticketService;

import com.myCinema.exception.DBException;
import com.myCinema.model.Ticket;

import java.util.List;

/**
 * Basic service interface.
 * Provided operations with TicketDao.
 *
 * @author Oleksandr Kovtun
 */
public interface TicketService {

    /**
     * Creation new tickets.
     *
     * @param tickets - list of tickets, which must be created
     * @throws DBException -
     */
    void addTickets (List<Ticket> tickets) throws DBException;

    /**
     * Getting tickets by account id.
     *
     * @param id - id of account
     * @return list of Ticket entities
     * @throws DBException -
     */
    List<Ticket> findTicketsByAccountId(int id) throws DBException;

    /**
     * Getting list of booleans,
     * which contains count of elements equal to count of seats in cinema hall (100).
     * If element is true, then seat number equal to index of this element - is empty.
     * Else seat is busy.
     *
     * @param id - id of session
     * @return list of booleans
     * @throws DBException -
     */
    List<Boolean> findSeatsInfoBySessionId(int id) throws DBException;
}
