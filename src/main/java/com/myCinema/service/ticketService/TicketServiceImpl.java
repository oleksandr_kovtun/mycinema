package com.myCinema.service.ticketService;

import com.myCinema.connection.ConnectionPool;
import com.myCinema.dao.daoFactory.AbstractDaoFactory;
import com.myCinema.dao.daoFactory.DaoFactory;
import com.myCinema.dao.ticketDao.TicketDao;
import com.myCinema.exception.DBException;
import com.myCinema.model.Ticket;
import com.myCinema.utility.DBHelper;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of TicketService.
 * Provided operations with TicketDao.
 *
 * @author Oleksandr Kovtun
 */
public class TicketServiceImpl implements TicketService{

    TicketDao ticketDao;
    ConnectionPool connectionPool;

    private static final Logger LOGGER = Logger.getLogger(TicketServiceImpl.class);

    /**
     * Constructor for getting connection pool and ticket DAO objects.
     */
    public TicketServiceImpl() {
        DaoFactory daoFactory = AbstractDaoFactory.getDaoFactory();
        ticketDao = daoFactory.getTicketDao();
        connectionPool = ConnectionPool.getInstance();
        LOGGER.debug("Ticket service constructor worked!");
    }

    /**
     * Creation new tickets.
     * See also {@link TicketService#addTickets(List)}
     */
    @Override
    public void addTickets(List<Ticket> tickets) throws DBException {
        if (tickets == null || tickets.size() == 0) {
            return;
        }
        Connection con = connectionPool.getConnection();
        try {
            LOGGER.info("Try to insert tickets (session id =" + tickets.get(0).getSessionId() + ", count of tickets = " + tickets.size() +")...");
            con.setAutoCommit(false);
            for (Ticket t : tickets) {
                ticketDao.insertTicket(con, t);
            }
            con.commit();
            LOGGER.info("Tickets successfully inserted (session id =" + tickets.get(0).getSessionId() + ", count of tickets = " + tickets.size() +").");
        } catch (SQLException e) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                //
            }
            LOGGER.error("Tickets insertion failed!", e);
            throw new DBException("Tickets insertion failed!", e);
        } finally {
            try {
                con.setAutoCommit(true);
            } catch (SQLException e) {
                //
            }
            DBHelper.closeConnection(con);
        }
    }

    /**
     * Getting tickets by account id.
     * See also {@link TicketService#findTicketsByAccountId(int)}
     */
    @Override
    public List<Ticket> findTicketsByAccountId(int id) throws DBException {
        Connection con = connectionPool.getConnection();
        List<Ticket> tickets;
        try {
            LOGGER.info("Try to find tickets by account (id = " + id + ")...");
            tickets = ticketDao.findTicketsByAccountId(con, id);
            LOGGER.info("Tickets successfully founded by account (id = " + id + ").");
        } catch (SQLException e) {
            LOGGER.error("Searching tickets by account id failed!", e);
            throw new DBException("Searching tickets by account id failed!", e);
        } finally {
            DBHelper.closeConnection(con);
        }
        return tickets;
    }

    /**
     * Getting list of booleans,
     * which contains count of elements equal to count of seats in cinema hall (100).
     * See also {@link TicketService#findSeatsInfoBySessionId(int)}
     */
    @Override
    public List<Boolean> findSeatsInfoBySessionId(int id) throws DBException {
        Connection con = connectionPool.getConnection();
        List<Ticket> tickets;
        try {
            LOGGER.info("Try to find tickets by session (id = " + id + ")...");
            tickets = ticketDao.findTicketsBySessionId(con, id);
            LOGGER.info("Tickets successfully founded by session (id = " + id + ").");
        } catch (SQLException e) {
            LOGGER.error("Searching tickets by session id failed!", e);
            throw new DBException("Searching tickets by session id failed!", e);
        } finally {
            DBHelper.closeConnection(con);
        }
        List<Boolean> seats = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            seats.add(true);
        }
        for (Ticket ticket : tickets) {
            seats.set(ticket.getSeat(), false);
        }
        return seats;
    }
}
