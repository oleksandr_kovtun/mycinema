package com.myCinema.service.accountService;

import com.myCinema.exception.DBException;
import com.myCinema.model.Account;

import java.util.List;

/**
 * Basic service interface.
 * Provided operations with AccountDao.
 *
 * @author Oleksandr Kovtun
 */
public interface AccountService {

    /**
     * Getting account by id. Can choose - all or main fields must be filled.
     *
     * @param id - id of account, which must be received
     * @param fullInformation - true, if want to get account with all fields filled;
     *                        false, if want to get account with main fields filled.
     * @return Account entity
     * @throws DBException -
     */
    Account findAccountById(int id, boolean fullInformation) throws DBException;

    /**
     * Getting account by login.
     * Id, login, password(encrypt), email and role fields will be filled.
     * @param login - login of account
     * @return Account entity
     * @throws DBException -
     */
    Account findAccountByLogin(String login) throws DBException;

    /**
     * Creating new account.
     *
     * @param account - Account entity, which must be created
     * @return same account, which actual fields
     * @throws DBException -
     */
    Account createNewAccount(Account account) throws DBException;

    /**
     * Updating password of account.
     *
     * @param account - Account entity, which contain new password
     * @return same account, which changed password
     * @throws DBException -
     */
    Account updatePassword(Account account) throws DBException;

    /**
     * Updating information of account.
     *
     * @param account Account entity, which contain new information
     * @return same account, which changed information
     * @throws DBException -
     */
    Account updateInformation(Account account) throws DBException;

    /**
     * Updating role fo account.
     *
     * @param login - login of account, which must be updated
     * @param admin - true, if set role "admin" or false, if set role "customer"
     */
    void updateRoleByLogin(String login, boolean admin) throws DBException;

    List<Account> findAllAdmins() throws DBException;

    /**
     * Check, that account with current login exists.
     *
     * @param login - login, which must be checked
     * @return - true, if account not exists;
     *          false, if account exists.
     * @throws DBException -
     */
    boolean loginIsFree(String login) throws DBException;
}
