package com.myCinema.service.accountService;

import com.myCinema.connection.ConnectionPool;
import com.myCinema.dao.accountDao.AccountDao;
import com.myCinema.dao.daoFactory.AbstractDaoFactory;
import com.myCinema.dao.daoFactory.DaoFactory;
import com.myCinema.exception.DBException;
import com.myCinema.model.Account;
import com.myCinema.utility.DBHelper;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Implementation of AccountService.
 * Provided operations with AccountDao.
 *
 * @author Oleksandr Kovtun
 */
public class AccountServiceImpl implements AccountService{

    AccountDao accountDao;
    ConnectionPool connectionPool;

    private static final Logger LOGGER = Logger.getLogger(AccountServiceImpl.class);

    /**
     * Constructor for getting connection pool and account DAO objects.
     */
    public AccountServiceImpl() {
        DaoFactory daoFactory = AbstractDaoFactory.getDaoFactory();
        accountDao = daoFactory.getAccountDao();
        connectionPool = ConnectionPool.getInstance();
        LOGGER.debug("Account service constructor worked!");
    }

    /**
     * Getting account by id. Can choose - all or main fields must be filled.
     * See also {@link AccountService#findAccountById(int, boolean)}
     */
    @Override
    public Account findAccountById(int id, boolean fullInformation) throws DBException {
        Connection con = connectionPool.getConnection();
        Account acc;
        try {
            if (fullInformation) {
                LOGGER.info("Try to find full account by id " + id +"...");
                acc = accountDao.findFullAccountById(con, id);
                LOGGER.info("Account (full) by id = " + id +" successfully founded!");
            } else {
                LOGGER.info("Try to find account by id " + id +"...");
                acc = accountDao.findAccountById(con, id);
                LOGGER.info("Account by id = " + id +" successfully founded!");
            }
        } catch (SQLException e) {
            LOGGER.error("Searching account by id failed!", e);
            throw new DBException("Searching account by id failed!", e);
        } finally {
            DBHelper.closeConnection(con);
        }
        return acc;
    }

    /**
     * Getting account by login.
     * See also {@link AccountService#findAccountByLogin(String)}
     */
    @Override
    public Account findAccountByLogin(String login) throws DBException {
        Connection con = connectionPool.getConnection();
        Account account;
        try {
            LOGGER.info("Try to find account by login - " + login + "...");
            account = accountDao.findAccountByLogin(con, login);
            LOGGER.info("Account by login '" + login + "' successfully founded.");
        } catch (SQLException e) {
            LOGGER.error("Searching account by login '" + login + "' failed!", e);
            throw new DBException("Searching account by login failed!", e);
        } finally {
            DBHelper.closeConnection(con);
        }
        return account;
    }

    /**
     * Creating new account.
     * See also {@link AccountService#createNewAccount(Account)}
     */
    @Override
    public Account createNewAccount(Account account) throws DBException {
        Connection con = connectionPool.getConnection();
        if (account == null) {
            return null;
        }
        try {
            LOGGER.info("Try to create new account...");
            con.setAutoCommit(false);
            account = accountDao.insertAccount(con, account);
            accountDao.insertAccountInfo(con, account);
            con.commit();
            LOGGER.info("Account successfully created. New account id = " + account.getId() + ".");
        } catch (SQLException e) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                //
            }
            LOGGER.error("Account creation failed", e);
            throw new DBException("Account creation failed", e);

        } finally {
            try {
                con.setAutoCommit(true);
            } catch (SQLException e) {
                //
            }
            DBHelper.closeConnection(con);
        }
        return account;
    }

    /**
     * Updating password of account.
     * See also {@link AccountService#updatePassword(Account)}
     */
    @Override
    public Account updatePassword(Account account) throws DBException {
        Connection con = connectionPool.getConnection();
        if (account == null) {
            return null;
        }
        try {
            LOGGER.info("Try to update password for account (id = " + account.getId() + ")...");
            con.setAutoCommit(false);
            boolean success = accountDao.updatePassword(con, account);
            account = accountDao.findAccountById(con, account.getId());
            con.commit();
            if (success) {
                LOGGER.info("Password for account (id = " + account.getId() + ") successfully updated.");
            }
        } catch (SQLException e) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                //
            }
            LOGGER.error("Password updating failed!", e);
            throw new DBException("Password updating failed!", e);

        } finally {
            try {
                con.setAutoCommit(true);
            } catch (SQLException e) {
                //
            }
            DBHelper.closeConnection(con);
        }
        return account;
    }

    /**
     * Updating information of account.
     * See also {@link AccountService#updateInformation(Account)}
     */
    @Override
    public Account updateInformation(Account account) throws DBException {
        Connection con = connectionPool.getConnection();
        if (account == null) {
            return null;
        }
        try {
            LOGGER.info("Try to update details for account (id = " + account.getId() + ")...");
            con.setAutoCommit(false);
            if (account.getName() != null) {
                accountDao.updateName(con, account);
            }
            if (account.getSurname() != null) {
                accountDao.updateSurname(con, account);
            }
            if (account.getPhoneNumber() != null) {
                accountDao.updateNumber(con, account);
            }
            if (account.getEmail() != null) {
                accountDao.updateEmail(con, account);
            }
            account = accountDao.findAccountById(con, account.getId());
            con.commit();
            LOGGER.info("Account details (id = " + account.getId() + ") successfully updated.");
        } catch (SQLException e) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                //
            }
            LOGGER.error("Update account information failed!", e);
            throw new DBException("Update account information failed!", e);

        } finally {
            try {
                con.setAutoCommit(true);
            } catch (SQLException e) {
                //
            }
            DBHelper.closeConnection(con);
        }
        return account;
    }

    @Override
    public void updateRoleByLogin(String login, boolean admin) throws DBException {
        Connection con = connectionPool.getConnection();
        try {
            LOGGER.info("Try to update role for account (login = " + login + ", admin = " + admin + ")...");
            boolean success;
            if (admin) {
                success = accountDao.updateRole(con, login, 2);
            } else {
                success = accountDao.updateRole(con, login, 3);
            }
            if (success) {
                LOGGER.info("Account role successfully updated!");
            }
        } catch (SQLException e) {
            LOGGER.error("Role updating failed!", e);
            throw new DBException("Role updating failed!", e);

        } finally {
            DBHelper.closeConnection(con);
        }
    }

    @Override
    public List<Account> findAllAdmins() throws DBException {
        Connection con = connectionPool.getConnection();
        List<Account> result;
        try {
            LOGGER.info("Try to get all admins accounts...");
            result = accountDao.findAdminAccounts(con);
            LOGGER.info("Admin accounts successfully got!");
        } catch (SQLException e) {
            LOGGER.error("Getting admin accounts failed", e);
            throw new DBException("Getting admin accounts failed", e);
        } finally {
            DBHelper.closeConnection(con);
        }
        return result;
    }

    /**
     * Check, that account with current login exists.
     * See also {@link AccountService#loginIsFree(String)}
     */
    @Override
    public boolean loginIsFree(String login) throws DBException {
        Connection con = connectionPool.getConnection();
        boolean result;
        try {
            LOGGER.info("Try to check, the login '" + login + "' is free?..");
            result = accountDao.findCountByLogin(con, login) != 1;
            LOGGER.info("Checking successfully completed. Login '" + login + "' is free? (" + result + ").");
        } catch (SQLException e) {
            LOGGER.error("Checking  by login '" + login + "' failed", e);
            throw new DBException("Checking  by login '" + login + "' failed", e);
        } finally {
            DBHelper.closeConnection(con);
        }
        return result;
    }
}
