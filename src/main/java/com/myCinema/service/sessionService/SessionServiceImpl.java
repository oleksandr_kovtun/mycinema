package com.myCinema.service.sessionService;

import com.myCinema.connection.ConnectionPool;
import com.myCinema.dao.daoFactory.AbstractDaoFactory;
import com.myCinema.dao.daoFactory.DaoFactory;
import com.myCinema.dao.sessionDao.SessionDao;
import com.myCinema.exception.DBException;
import com.myCinema.model.Session;
import com.myCinema.utility.DBHelper;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Implementation of SessionService.
 * Provided operations with SessionDao.
 *
 * @author Oleksandr Kovtun
 */
public class SessionServiceImpl implements SessionService{

    SessionDao sessionDao;
    ConnectionPool connectionPool;

    private static final Logger LOGGER = Logger.getLogger(SessionServiceImpl.class);

    /**
     * Constructor for getting connection pool and session DAO objects.
     */
    public SessionServiceImpl() {
        DaoFactory daoFactory = AbstractDaoFactory.getDaoFactory();
        sessionDao = daoFactory.getSessionDao();
        connectionPool = ConnectionPool.getInstance();
        LOGGER.debug("Session service constructor worked!");
    }

    /**
     * Getting session by id.
     * See also {@link SessionService#findSessionById(int)}
     */
    @Override
    public Session findSessionById(int sessionId) throws DBException {
        Connection con = connectionPool.getConnection();
        Session session;
        try {
            LOGGER.info("Try to find session (id = " + sessionId + ")...");
            session = sessionDao.findSessionById(con, sessionId);
            LOGGER.info("Session successfully founded (id = " + sessionId + ").");
        } catch (SQLException e) {
            LOGGER.error("Searching session by id failed!", e);
            throw new DBException("Searching session by id failed!", e);
        } finally {
            DBHelper.closeConnection(con);
        }
        return session;
    }

    /**
     * Getting all session.
     * See also {@link SessionService#findAllSessions()}
     */
    @Override
    public List<Session> findAllSessions() throws DBException {
        Connection con = connectionPool.getConnection();
        List<Session> sessions;
        try {
            LOGGER.info("Try to find all actual sessions...");
            sessions = sessionDao.findAllSessions(con);
            LOGGER.info("All actual sessions successfully founded!");
        } catch (SQLException e) {
            LOGGER.error("Searching all sessions failed!", e);
            throw new DBException("Searching all sessions failed!", e);
        } finally {
            DBHelper.closeConnection(con);
        }
        return sessions;
    }

    /**
     * Getting all session by parts (pagination).
     * See also {@link SessionService#findAllSessions(int, int, String, boolean)}
     */
    @Override
    public List<Session> findAllSessions(int limit, int offset, String orderBy, boolean actual) throws DBException {
        Connection con = connectionPool.getConnection();
        List<Session> sessions;
        try {
            if (actual) {
                LOGGER.info("Try to find all actual sessions...");
                sessions = sessionDao.findAllActualSessions(con, limit, offset, orderBy);
                LOGGER.info("All actual sessions successfully founded!");
            } else {
                LOGGER.info("Try to find all sessions...");
                sessions = sessionDao.findAllSessions(con, limit, offset, orderBy);
                LOGGER.info("All sessions successfully founded!");
            }
        } catch (SQLException e) {
            LOGGER.error("Searching all sessions failed!", e);
            throw new DBException("Searching all sessions failed!", e);
        } finally {
            DBHelper.closeConnection(con);
        }
        return sessions;
    }

    /**
     * Getting all sessions, which relate to current movie.
     * See also {@link SessionService#findSessionsByMovieId(int)}
     */
    @Override
    public List<Session> findSessionsByMovieId(int movieId) throws DBException {
        Connection con = connectionPool.getConnection();
        List<Session> sessions;
        try {
            LOGGER.info("Try to find sessions by movie (id = " + movieId + ")...");
            sessions = sessionDao.findSessionsByMovieId(con, movieId);
            LOGGER.info("Sessions by movie (id = " + movieId + ") successfully founded.");
        } catch (SQLException e) {
            LOGGER.error("Searching session by movie id failed!", e);
            throw new DBException("Searching session by movie id failed!", e);
        } finally {
            DBHelper.closeConnection(con);
        }
        return sessions;
    }

    /**
     * Getting all sessions, which relate to current month.
     * See also {@link SessionService#findSessionsByDate(String)}
     */
    @Override
    public List<Session> findSessionsByDate(String sessionDate) throws DBException {
        Connection con = connectionPool.getConnection();
        List<Session> sessions;
        try {
            LOGGER.info("Try to find sessions by date (" + sessionDate + ")...");
            sessions = sessionDao.findSessionsByDate(con, sessionDate);
            LOGGER.info("Sessions by date (" + sessionDate + ") successfully founded.");
        } catch (SQLException e) {
            LOGGER.error("Searching session by date failed!", e);
            throw new DBException("Searching session by date failed!", e);
        } finally {
            DBHelper.closeConnection(con);
        }
        return sessions;
    }

    /**
     * Getting all sessions, which relate to current month.
     * See also {@link SessionService#findSessionsByMonth(int, int)}
     */
    @Override
    public List<Session> findSessionsByMonth(int year, int month) throws DBException {
        Connection con = connectionPool.getConnection();
        List<Session> sessions;
        try {
            LOGGER.info("Try to find sessions by month (" + month + ")...");
            sessions = sessionDao.findSessionsByMonth(con, year, month);
            LOGGER.info("Sessions by month (" + month + ") successfully founded.");
        } catch (SQLException e) {
            LOGGER.error("Searching session by month failed!", e);
            throw new DBException("Searching session by month failed!", e);
        } finally {
            DBHelper.closeConnection(con);
        }
        return sessions;
    }

    /**
     * Getting count of actual sessions (date = today date or later).
     * See also {@link SessionService#findCountOfSessions()}
     */
    @Override
    public int findCountOfSessions() throws DBException {
        Connection con = connectionPool.getConnection();
        int count;
        try {
            LOGGER.info("Try to find count of sessions...");
            count = sessionDao.findCountOfSessions(con);
            LOGGER.info("Sessions count successfully founded.");
        } catch (SQLException e) {
            LOGGER.error("Finding count of sessions failed!", e);
            throw new DBException("Finding count of sessions failed!", e);
        } finally {
            DBHelper.closeConnection(con);
        }
        return count;
    }

    /**
     * Finding count of actual sessions (date = today date or later),
     * by movie, which sessions must contain.
     * See also {@link SessionService#findCountOfSessionsByMovieId(int)}
     */
    @Override
    public int findCountOfSessionsByMovieId(int movieId) throws DBException {
        Connection con = connectionPool.getConnection();
        int count;
        try {
            LOGGER.debug("Try to find count of sessions by movie (id = " + movieId + ")...");
            count = sessionDao.findCountOfSessionsByMovieId(con, movieId);
            LOGGER.debug("Sessions count by movie (id = " + movieId + ") successfully founded.");
        } catch (SQLException e) {
            LOGGER.error("Searching count of sessions by movie id failed!", e);
            throw new DBException("Searching count of sessions by movie id failed!", e);
        } finally {
            DBHelper.closeConnection(con);
        }
        return count;
    }

    /**
     * Creation new session.
     * See also {@link SessionService#createSession(int, Session)}
     */
    @Override
    public void createSession(int movieId, Session session) throws DBException {
        Connection con = connectionPool.getConnection();
        try {
            LOGGER.info("Try to create new sessions by movie (id = " + movieId + ")...");
            sessionDao.insertSession(con, movieId, session);
            LOGGER.info("Session by movie (id = " + movieId + ") successfully created.");
        } catch (SQLException e) {
            LOGGER.error("Creation new session failed!", e);
            throw new DBException("Creation new session failed!", e);
        } finally {
            DBHelper.closeConnection(con);
        }
    }

    /**
     * Removing session by id.
     * See also {@link SessionService#removeSessionById(int)}
     */
    @Override
    public void removeSessionById(int id) throws DBException {
        Connection con = connectionPool.getConnection();
        try {
            LOGGER.info("Try to delete session (id = " + id + ")...");
            sessionDao.deleteSessionById(con, id);
            LOGGER.info("Session (id + " + id + ") successfully deleted.");
        } catch (SQLException e) {
            LOGGER.error("Deleting session failed!", e);
            throw new DBException("Deleting session failed!", e);
        } finally {
            DBHelper.closeConnection(con);
        }
    }
}
