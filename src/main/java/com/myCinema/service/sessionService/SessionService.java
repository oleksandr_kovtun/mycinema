package com.myCinema.service.sessionService;

import com.myCinema.exception.DBException;
import com.myCinema.model.Session;

import java.util.List;

/**
 * Basic service interface.
 * Provided operations with SessionDao.
 *
 * @author Oleksandr Kovtun
 */
public interface SessionService {

    /**
     * Getting session by id.
     *
     * @param sessionId - id of session, which must be received
     * @return Session entity
     * @throws DBException -
     */
    Session findSessionById(int sessionId) throws DBException;

    /**
     * Getting all sessions.
     *
     * @return list of Session entities
     * @throws DBException -
     */
    List<Session> findAllSessions() throws DBException;

    /**
     * Getting all sessions by parts (pagination).
     *
     * @param limit - size of result list
     * @param offset - pagination parameter offset
     * @param orderBy - part for SQL query, which indicates sorting
     * @param actual - true, if want to get sessions with actual date
     * @return list of Session entities
     * @throws DBException -
     */
    List<Session> findAllSessions(int limit, int offset, String orderBy, boolean actual) throws DBException;

    /**
     * Getting all sessions, which relate to current movie.
     *
     * @param movieId - id of movie, which sessions must to contain
     * @return list of Session entities
     * @throws DBException -
     */
    List<Session> findSessionsByMovieId(int movieId) throws DBException;

    /**
     * Getting all sessions, which relate to current month.
     *
     * @param sessionDate - string, which contains date in format (yyyy-MM-dd)
     * @return list of Session entities
     * @throws DBException -
     */
    List<Session> findSessionsByDate(String sessionDate) throws DBException;

    /**
     * Getting all sessions, which relate to current month.
     *
     * @param year - year in full format (example 2021)
     * @param month - month number (1 - 12)
     * @return list of Session entities
     * @throws DBException -
     */
    List<Session> findSessionsByMonth(int year, int month) throws DBException;

    /**
     * Getting count of actual sessions (date = today date or later).
     *
     * @return count of actual sessions
     * @throws DBException -
     */
    int findCountOfSessions() throws DBException;

    /**
     * Finding count of actual sessions (date = today date or later),
     * by movie, which sessions must contain.
     *
     * @param movieId -identifier number of movie
     * @return count of actual sessions
     * @throws DBException -
     */
    int findCountOfSessionsByMovieId(int movieId) throws DBException;

    /**
     * Creation new session.
     *
     * @param movieId - id of movie, which session must to contain
     * @param session - Session entity, which must be inserted
     * @throws DBException -
     */
    void createSession(int movieId, Session session) throws DBException;

    /**
     * Removing session by id.
     *
     * @param id - id of session, which must be deleted
     * @throws DBException -
     */
    void removeSessionById(int id) throws DBException;
}
