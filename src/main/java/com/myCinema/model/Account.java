package com.myCinema.model;

import java.io.Serializable;
import java.util.Objects;

public class Account implements Serializable{
    
	private static final long serialVersionUID = 1L;
	
	private int id;
    private String login;
    private String password;
    private String email;
    private String accRole;
    private String name;
    private String surname;
    private String phoneNumber;

    /**
     * Constructor for insertion account into DB.
     */
    public Account(String login, String password, String email) {
        this.login = login;
        this.password = password;
        this.email = email;
    }

    /**
     * Constructor for getting account from DB.
     */
    public Account(int id, String login, String password, String email, String role) {
        this(login, password, email);
        this.id = id;
        this.accRole = role;
    }

    // Getters

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getAccRole() {
        return accRole;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    // Setters

    public void setId(int id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAccRole(String accRole) {
        this.accRole = accRole;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    //////

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id && login.equals(account.login) && password.equals(account.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, password);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (id != 0) {
            sb.append("id = ").append(id).append(", ");
        }
        sb.append("login = ").append(login).append(", ");
        sb.append("email = ").append(email).append(", ");
        if (accRole != null) {
            sb.append("role = ").append(accRole).append(", ");
        }
        if (name != null) {
            sb.append("name = ").append(name).append(", ");
        }
        if (surname != null) {
            sb.append("surname = ").append(surname).append(", ");
        }
        if (phoneNumber != null) {
            sb.append("phone number = ").append(phoneNumber).append(", ");
        }
        sb.delete(sb.length() - 2, sb.length());
        sb.append(".");
        return sb.toString();
    }
}
