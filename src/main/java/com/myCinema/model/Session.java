package com.myCinema.model;

import java.io.Serializable;
import java.util.Objects;

public class Session implements Serializable{
    
	private static final long serialVersionUID = 1L;
	
	private Movie movie;
    private int id;
    private String date;
    private String time;
    private int emptySeats;
    private int ticketCost;

    /**
     * Constructor for insertion session into DB.
     */
    public Session(String date, String time, int ticketCost) {
        this.date = date;
        this.time = time;
        this.ticketCost = ticketCost;
    }

    /**
     * Constructor for getting session from DB without movie.
     */
    public Session(int id, String date, String time, int emptySeats, int ticketCost) {
        this(date, time, ticketCost);
        this.id = id;
        this.emptySeats = emptySeats;
    }

    /**
     * Constructor for getting session from DB with movie.
     */
    public Session(Movie movie, int id, String date, String time, int emptySeats, int ticketCost) {
        this(id, date, time, emptySeats, ticketCost);
        this.movie = movie;
    }

    // Getters

    public int getId() {
        return id;
    }

    public Movie getMovie() {
        return movie;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public int getTicketCost() {
        return ticketCost;
    }

    public int getEmptySeats() {
        return emptySeats;
    }

    // Setters

    public void setId(int id) {
        this.id = id;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setEmptySeats(int emptySeats) {
        this.emptySeats = emptySeats;
    }

    public void setTicketCost(int ticketCost) {
        this.ticketCost = ticketCost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Session session = (Session) o;
        return id == session.id && date.equals(session.date) && time.equals(session.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, time);
    }

    @Override
    public String toString() {
        return "Session{" +
                "movie=" + movie +
                ", id=" + id +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", emptySeats=" + emptySeats +
                ", ticketCost=" + ticketCost +
                '}';
    }
}
