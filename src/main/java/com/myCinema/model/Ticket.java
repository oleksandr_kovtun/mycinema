package com.myCinema.model;

import java.io.Serializable;
import java.util.Objects;

public class Ticket implements Serializable{
  
	private static final long serialVersionUID = 1L;
	
	private int accId;
    private int sessionId;
    private int seat;
    private String movieName;
    private String date;
    private String time;

    private boolean actual;

    /**
     * Constructor for insertion ticket into DB.
     */
    public Ticket(int accId, int sessionId, int seat) {
        this.accId = accId;
        this.sessionId = sessionId;
        this.seat = seat;
    }

    /**
     * Constructor for getting ticket from DB.
     */
    public Ticket(int accId, int sessionId, int seat, String movieName, String date, String time) {
        this(accId, sessionId, seat);
        this.movieName = movieName;
        this.date = date;
        this.time = time;
    }

    // Getters

    public int getAccId() {
        return accId;
    }

    public int getSessionId() {
        return sessionId;
    }

    public int getSeat() {
        return seat;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getMovieName() {
        return movieName;
    }

    public boolean getActual() {
        return actual;
    }

    // Setters

    public void setAccId(int accId) {
        this.accId = accId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public void setActual(boolean actual) {
        this.actual = actual;
    }

    //////

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return accId == ticket.accId && sessionId == ticket.sessionId && seat == ticket.seat;
    }

    @Override
    public int hashCode() {
        return Objects.hash(accId, sessionId, seat);
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "accId=" + accId +
                ", sessionId=" + sessionId +
                ", seat=" + seat +
                '}';
    }
}
