package com.myCinema.model;

import java.io.Serializable;
import java.util.Objects;

public class Movie implements Serializable{
    
	private static final long serialVersionUID = 1L;
	
	private int id;
    private String name;
    private String posterUrl;
    private String genre;
    private String voiceActing;
    private String filmedIn;
    private int duration;
    private String shortDesc;
    private String fullDesc;

    int sessionsAvailable;

    /**
     * Constructor for insertion movie into DB.
     */
    public Movie(String name, String posterUrl, String genre, String voiceActing,
                 String filmedIn, int duration, String shortDesc, String fullDesc) {
        this.name = name;
        this.posterUrl = posterUrl;
        this.genre = genre;
        this.voiceActing = voiceActing;
        this.filmedIn = filmedIn;
        this.duration = duration;
        this.shortDesc = shortDesc;
        this.fullDesc = fullDesc;
    }

    /**
     * Constructor for getting movie from DB.
     */
    public Movie(int id, String name, String posterUrl, String genre, String voiceActing,
                 String filmedIn, int duration, String shortDesc, String fullDesc) {
        this(name, posterUrl, genre, voiceActing, filmedIn, duration, shortDesc, fullDesc);
        this.id = id;
    }

    // Getters

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public String getGenre() {
        return genre;
    }

    public String getFilmedIn() {
        return filmedIn;
    }

    public String getVoiceActing() {
        return voiceActing;
    }

    public int getDuration() {
        return duration;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public String getFullDesc() {
        return fullDesc;
    }

    public int getSessionsAvailable() {
        return sessionsAvailable;
    }

    // Setters

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setFilmedIn(String filmedIn) {
        this.filmedIn = filmedIn;
    }

    public void setFullDesc(String fullDesc) {
        this.fullDesc = fullDesc;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public void setVoiceActing(String voice_acting) {
            this.voiceActing = voice_acting;
    }

    public void setSessionsAvailable(int sessionsAvailable) {
        this.sessionsAvailable = sessionsAvailable;
    }

    //////

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return id == movie.id && name.equals(movie.name) && voiceActing.equals(movie.voiceActing);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, voiceActing);
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", posterUrl='" + posterUrl + '\'' +
                ", genre='" + genre + '\'' +
                ", voice_acting='" + voiceActing + '\'' +
                ", filmedIn='" + filmedIn + '\'' +
                ", duration=" + duration +
                ", shortDesc='" + shortDesc + '\'' +
                ", fullDesc='" + fullDesc + '\'' +
                '}';
    }
}
