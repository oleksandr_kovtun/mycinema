package com.myCinema.command.admin;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Movie;
import com.myCinema.service.movieService.MovieService;
import com.myCinema.service.movieService.MovieServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;

import static com.myCinema.utility.WebUtil.*;

/**
 * Class creates new movie and using MovieService add it to DB.
 *
 * @author Oleksandr Kovtun
 */
public class CreateMovieCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(CreateMovieCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        Movie movie;
        String fileName = null;

        LOGGER.info("Try to create new movie...");
        String uploadPath = request.getServletContext().getRealPath("") + File.separator + "images\\posters\\";
        try {
            LOGGER.info("Try to download poster image...");
            for (Part part : request.getParts()) {
                fileName = getFileName(part);
                part.write(uploadPath + File.separator + fileName);
            }
            LOGGER.info("Poster image downloaded...");
        } catch (IOException | ServletException e) {
            LOGGER.info("Poster image downloading failed!..");
            setTroubleMessage(request, 2);
        }

        movie = fillMovieFromRequest(request, fileName);
        MovieService movieService = new MovieServiceImpl();
        try {
            movieService.createNewMovie(movie);
            setNotificationMessage(request, 2);
            LOGGER.info("Movie successfully created!");
        } catch (DBException e) {
            setTroubleMessage(request, 3);
            LOGGER.error("Movie creation failed!", e);
        }

        return JspPages.ADMINISTRATE.getValue();
    }

    private String getFileName(Part part) {
        String result;
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                result = content.substring(content.indexOf("filename=") + 10, content.length() - 1);
                return result;
            }
        }
        return "No name";
    }

    private Movie fillMovieFromRequest (HttpServletRequest request, String posterUrl) {
        String name = request.getParameter("movieName");
        String genre = request.getParameter("movieGenre");
        String voice = request.getParameter("movieVoiceActing");
        String filmedIn = request.getParameter("movieFilmedIn");
        int duration = Integer.parseInt(request.getParameter("movieDuration"));
        String shortDesc = request.getParameter("movieShortDesc");
        String desc = request.getParameter("movieDesc");
        return new Movie(name, posterUrl, genre, voice, filmedIn, duration, shortDesc, desc);
    }

}
