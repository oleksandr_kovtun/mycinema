package com.myCinema.command.admin;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Session;
import com.myCinema.service.sessionService.SessionService;
import com.myCinema.service.sessionService.SessionServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.myCinema.utility.WebUtil.clearSessionMessages;

/**
 * Class shows "remove session form".
 *
 * @author Oleksandr Kovtun
 */
public class RemoveSessionFormCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(RemoveSessionFormCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        LOGGER.info("Try to create form for session deleting...");
        int id = Integer.parseInt(request.getParameter("sessionid"));

        SessionService sessionService = new SessionServiceImpl();
        Session s = null;
        try {
            s = sessionService.findSessionById(id);
            LOGGER.info("Form for session deleting successfully created!");
        } catch (DBException e) {
            LOGGER.error("Form for session deleting isn't created! Error on dao level!", e);
        }
        request.setAttribute("session", s);
        request.setAttribute("removeSessionForm", true);

        return JspPages.ADMINISTRATE.getValue();
    }
}
