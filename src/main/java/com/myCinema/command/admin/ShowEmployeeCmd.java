package com.myCinema.command.admin;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Account;
import com.myCinema.service.accountService.AccountService;
import com.myCinema.service.accountService.AccountServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.myCinema.utility.WebUtil.clearSessionMessages;

public class ShowEmployeeCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(ShowEmployeeCmd.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        LOGGER.info("Try to get employee data...");
        List<Account> employee;

        AccountService accountService = new AccountServiceImpl();
        try {
            employee = accountService.findAllAdmins();
            LOGGER.info("Got employee list!");
        } catch (DBException e) {
            LOGGER.error("Getting employee list failed!");
            return JspPages.DB_ERROR_ADMIN.getValue();
        }

        request.setAttribute("employeeList", employee);
        request.setAttribute("showEmployeeForm", true);
        return JspPages.ADMINISTRATE.getValue();
    }
}
