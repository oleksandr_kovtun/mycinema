package com.myCinema.command.admin;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.service.accountService.AccountService;
import com.myCinema.service.accountService.AccountServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.myCinema.utility.WebUtil.*;

public class GiveAdminRightsCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(GiveAdminRightsCmd.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        LOGGER.info("Try to give administrator rights to account...");

        String loginToUpdate = request.getParameter("loginToAdmin");
        System.out.println(loginToUpdate);
        AccountService accountService = new AccountServiceImpl();

        try {
            if (accountService.loginIsFree(loginToUpdate)) {
                setTroubleMessage(request, 8);
                return JspPages.ADMINISTRATE.getValue();
            }
            accountService.updateRoleByLogin(loginToUpdate, true);
        } catch (DBException e) {
            LOGGER.error("Role update failed. Problem on dao level!");
            return JspPages.DB_ERROR_ADMIN.getValue();
        }

        setNotificationMessage(request, 5);
        return JspPages.ADMINISTRATE.getValue();
    }
}
