package com.myCinema.command.admin;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.service.sessionService.SessionService;
import com.myCinema.service.sessionService.SessionServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.myCinema.utility.WebUtil.*;

/**
 * Class removes session using SessionService.
 *
 * @author Oleksandr Kovtun
 */
public class RemoveSessionCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(RemoveSessionCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        LOGGER.info("Try to delete session...");
        int sesId = Integer.parseInt(request.getParameter("sessionid"));
        SessionService sessionService = new SessionServiceImpl();
        try {
            sessionService.removeSessionById(sesId);
            setNotificationMessage(request, 4);
            LOGGER.info("Session successfully deleted!");
        } catch (DBException e) {
            setTroubleMessage(request, 5);
            LOGGER.error("Session deleting failed! Error on DAO level!", e);
        }

        return JspPages.ADMINISTRATE.getValue();
    }
}
