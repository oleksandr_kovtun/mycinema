package com.myCinema.command.admin;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.myCinema.utility.WebUtil.clearSessionMessages;

/**
 * Class shows different administrator forms by input parameter.
 *
 * @author Oleksandr Kovtun
 */
public class ShowAdminFormCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(ShowAdminFormCmd.class);

    /**
     * Can show forms: "daily report", "monthly report", "movie form".
     *
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        LOGGER.info("Try to create admin form...");
        String form = request.getParameter("form");
        switch (form) {
            case "dailyReport":
                request.setAttribute("dailyReportForm", true);
                LOGGER.info("Form \"daily report\" created!");
                break;
            case "monthlyReport":
                request.setAttribute("monthlyReportForm", true);
                LOGGER.info("Form \"monthly report\" created!");
                break;
            case "createMovieForm":
                request.setAttribute("movieForm", true);
                LOGGER.info("Form \"create movie\" created!");
                break;
            case "giveAdminRights":
                request.setAttribute("giveAdminRightsForm", true);
                LOGGER.info("Form \"give admin rights\" created!");
                break;
        }

        return JspPages.ADMINISTRATE.getValue();
    }
}
