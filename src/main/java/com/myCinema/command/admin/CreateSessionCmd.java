package com.myCinema.command.admin;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Session;
import com.myCinema.service.sessionService.SessionService;
import com.myCinema.service.sessionService.SessionServiceImpl;
import com.myCinema.utility.DateManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.myCinema.utility.WebUtil.*;

/**
 * Class creates new session and using SessionService add it to DB.
 *
 * @author Oleksandr Kovtun
 */
public class CreateSessionCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(CreateSessionCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        LOGGER.info("Try to create new session...");
        String date = request.getParameter("sessionDate");
        String time = request.getParameter("sessionTime");
        int cost = Integer.parseInt(request.getParameter("cost"));
        int movieId = Integer.parseInt(request.getParameter("movie"));
        if (DateManager.currentDateForSql().compareTo(date) > 0) {
            setTroubleMessage(request, 7);
            return JspPages.ADMINISTRATE.getValue();
        }

        Session s = new Session(date, time, cost);

        SessionService sessionService = new SessionServiceImpl();
        try {
            sessionService.createSession(movieId, s);
            setNotificationMessage(request, 1);
            LOGGER.info("Session successfully created!");
        } catch (DBException e) {
            setTroubleMessage(request, 1);
            LOGGER.info("Session creation failed!");
        }

        return JspPages.ADMINISTRATE.getValue();
    }
}
