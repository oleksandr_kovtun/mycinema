package com.myCinema.command.admin;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Movie;
import com.myCinema.service.movieService.MovieService;
import com.myCinema.service.movieService.MovieServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.myCinema.utility.WebUtil.clearSessionMessages;

/**
 * Class shows "create session form by movies"
 *
 * @author Oleksandr Kovtun
 */
public class ShowCreateSessionFormCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(ShowCreateSessionFormCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        LOGGER.info("Try to create form for session creating...");
        MovieService movieService = new MovieServiceImpl();
        List<Movie> movies = null;
        try {
            movies = movieService.findAllMovies();
            LOGGER.info("Form for session creating successfully created!");
        } catch (DBException e) {
            LOGGER.error("Cannot create form! Error on DAO level!", e);
            return "errors/errorDbCrash.jsp";
        }
        request.setAttribute("sessionForm", true);
        request.setAttribute("movies", movies);

        return JspPages.ADMINISTRATE.getValue();
    }
}
