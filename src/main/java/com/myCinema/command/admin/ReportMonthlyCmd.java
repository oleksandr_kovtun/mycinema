package com.myCinema.command.admin;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Session;
import com.myCinema.service.sessionService.SessionService;
import com.myCinema.service.sessionService.SessionServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.myCinema.utility.WebUtil.clearSessionMessages;
import static com.myCinema.utility.WebUtil.setTroubleMessage;

/**
 * Class generates report data by month.
 *
 * @author Oleksandr Kovtun
 */
public class ReportMonthlyCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(ReportMonthlyCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        LOGGER.info("Try to Create monthly report...");
        String date = request.getParameter("reportMonth");
        int year = Integer.parseInt(date.substring(0, date.indexOf("-")));
        int month = Integer.parseInt(date.substring(date.indexOf("-")+1));
        SessionService sessionService = new SessionServiceImpl();
        List<Session> sessions = null;
        try {
            sessions = sessionService.findSessionsByMonth(year, month);
            LOGGER.info("Session list by month successfully received...");
        } catch (DBException e) {
            LOGGER.error("Cannot get list of session by month! Error on DAO level!", e);
            return "errors/errorDbCrash.jsp";
        }

        if (sessions == null) {
            setTroubleMessage(request, 6);
            return JspPages.ADMINISTRATE.getValue();
        }

        int countOfSessions = sessions.size();
        int totalTicketsBought = 0;
        int totalProceeds = 0;
        for (Session s : sessions) {
            totalTicketsBought += (100 - s.getEmptySeats());
        }
        for (Session s :sessions) {
            totalProceeds += ((100 - s.getEmptySeats()) * s.getTicketCost());
        }

        request.setAttribute("totalProceeds", totalProceeds);
        request.setAttribute("totalTicketsBought", totalTicketsBought);
        request.setAttribute("sessionsByMonth", sessions);
        request.setAttribute("sessionsPerMonth", countOfSessions);
        request.setAttribute("date", date);
        request.setAttribute("monthlyReportForm", true);

        LOGGER.info("Monthly report successfully generated!");

        return JspPages.ADMINISTRATE.getValue();
    }
}
