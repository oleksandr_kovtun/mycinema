package com.myCinema.command.admin;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Session;
import com.myCinema.service.sessionService.SessionService;
import com.myCinema.service.sessionService.SessionServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.myCinema.utility.WebUtil.*;

/**
 * Class generates report data by date.
 *
 * @author Oleksandr Kovtun
 */
public class ReportDailyCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(ReportDailyCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        LOGGER.info("Try to create daily report...");
        String date = request.getParameter("reportDate");
        SessionService sessionService = new SessionServiceImpl();
        List<Session> sessions = null;
        try {
            sessions = sessionService.findSessionsByDate(date);
            LOGGER.info("Sessions list by day successfully received...");
        } catch (DBException e) {
            LOGGER.error("Cannot get list of session by day! Error on DAO level!", e);
            return "errors/errorDbCrash.jsp";
        }

        if (sessions == null) {
            setTroubleMessage(request, 6);
            return JspPages.ADMINISTRATE.getValue();
        }

        int countOfSessions = sessions.size();
        int totalTicketsBought = 0;
        int totalProceeds = 0;
        for (Session s : sessions) {
            totalTicketsBought += (100 - s.getEmptySeats());
        }
        for (Session s :sessions) {
            totalProceeds += ((100 - s.getEmptySeats()) * s.getTicketCost());
        }

        request.setAttribute("totalProceeds", totalProceeds);
        request.setAttribute("totalTicketsBought", totalTicketsBought);
        request.setAttribute("sessionsByDate", sessions);
        request.setAttribute("sessionsPerDay", countOfSessions);
        request.setAttribute("date", date);
        request.setAttribute("dailyReportForm", true);

        LOGGER.info("Daily report successfully generated!");

        return JspPages.ADMINISTRATE.getValue();
    }
}
