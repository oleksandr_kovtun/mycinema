package com.myCinema.command.admin;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.service.accountService.AccountService;
import com.myCinema.service.accountService.AccountServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.myCinema.utility.WebUtil.*;

public class TakeAwayAdminRightsCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(TakeAwayAdminRightsCmd.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        String loginToDismiss = request.getParameter("loginToDismiss");
        LOGGER.info("Try to dismiss admin " + loginToDismiss + "...");

        AccountService accountService = new AccountServiceImpl();
        try {
            accountService.updateRoleByLogin(loginToDismiss, false);
            setNotificationMessage(request, 6);
        } catch (DBException e) {
            LOGGER.error("Dismiss failed. Error on DAO level!");
            return JspPages.DB_ERROR_ADMIN.getValue();
        }

        return JspPages.ADMINISTRATE.getValue();
    }
}
