package com.myCinema.command.admin;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Movie;
import com.myCinema.service.movieService.MovieService;
import com.myCinema.service.movieService.MovieServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.myCinema.utility.WebUtil.*;

/**
 * Class shows "remove movie form".
 *
 * @author Oleksandr Kovtun
 */
public class RemoveMovieFormCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(RemoveMovieFormCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        LOGGER.info("Try to create form for movie deleting...");
        MovieService movieService = new MovieServiceImpl();
        List<Movie> movies = null;
        try {
            movies = movieService.findAllMovies();
            LOGGER.info("Form for movie deleting created!");
        } catch (DBException e) {
            LOGGER.error("Cannot create form for movie deleting. Error on DAO level!", e);
        }
        request.setAttribute("movies", movies);
        request.setAttribute("removeMovieForm", true);

        return JspPages.ADMINISTRATE.getValue();
    }
}
