package com.myCinema.command.admin;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.service.movieService.MovieService;
import com.myCinema.service.movieService.MovieServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.myCinema.utility.WebUtil.*;

/**
 * Class removes movie using MovieService.
 *
 * @author Oleksandr Kovtun
 */
public class RemoveMovieCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(RemoveMovieCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        LOGGER.info("Try to delete movie...");
        int id = Integer.parseInt(request.getParameter("movie"));
        MovieService movieService = new MovieServiceImpl();
        try {
            movieService.removeMovie(id);
            setNotificationMessage(request, 3);
            LOGGER.info("Movie successfully deleted!");
        } catch (DBException e) {
            setTroubleMessage(request, 4);
            LOGGER.error("Movie deleting failed on DAO level!", e);
        }

        return JspPages.ADMINISTRATE.getValue();
    }
}
