package com.myCinema.command.admin;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Session;
import com.myCinema.service.sessionService.SessionService;
import com.myCinema.service.sessionService.SessionServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class SessionsToRemoveCmd implements Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        List<Session> sessions;
        SessionService sessionService = new SessionServiceImpl();

        try {
            sessions = sessionService.findAllSessions();
        } catch (DBException e) {
            return JspPages.DB_ERROR_ADMIN.getValue();
        }

        request.setAttribute("showSessionsToRemove", true);
        request.setAttribute("allSessions", sessions);

        return JspPages.ADMINISTRATE.getValue();
    }
}
