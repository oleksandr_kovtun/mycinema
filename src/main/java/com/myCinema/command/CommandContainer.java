package com.myCinema.command;

import com.myCinema.command.account.*;
import com.myCinema.command.admin.*;
import com.myCinema.command.common.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of command pattern.
 *
 * Container contains all classes, that implements Command interface.
 */
public class CommandContainer {
    private static Map<String, Command> commands;

    /**
     * Constructor fill map with command classes and keys, that relate to this classes.
     */
    public CommandContainer() {
        commands = new HashMap<>();

        // Common commands
        commands.put("login", new LoginCmd());
        commands.put("logout", new LogoutCmd());
        commands.put("register", new RegisterCmd());
        commands.put("showMovies", new ShowMoviesCmd());
        commands.put("showSessions", new ShowSessionsCmd());
        commands.put("movie", new MovieDetailsCmd());
        commands.put("session", new SessionDetailsCmd());
        commands.put("selectSeat", new SelectSeatCmd());
        commands.put("setLocale", new SetLocaleCmd());

        // Account commands
        commands.put("confirmTicketsBuy", new ConfirmTicketBuyCmd());
        commands.put("confirmedBuying", new BuyingConfirmedCmd());
        commands.put("showTickets", new ShowTicketsCmd());
        commands.put("changePassForm", new ShowChangePassFormCmd());
        commands.put("changePass", new ChangePassCmd());
        commands.put("changeInfoForm", new ShowChangeAccFieldsForm());
        commands.put("changeAccInfo", new ChangeAccInfoCmd());

        // Administrator commands
        commands.put("showCreateSession", new ShowCreateSessionFormCmd());
        commands.put("createSession", new CreateSessionCmd());
        commands.put("createMovie", new CreateMovieCmd());
        commands.put("sessionsToRemove", new SessionsToRemoveCmd());
        commands.put("removeSessionForm", new RemoveSessionFormCmd());
        commands.put("removeSession", new RemoveSessionCmd());
        commands.put("showRemoveMovie", new RemoveMovieFormCmd());
        commands.put("removeMovie", new RemoveMovieCmd());
        commands.put("showAdminForm", new ShowAdminFormCmd());
        commands.put("dailyReport", new ReportDailyCmd());
        commands.put("monthlyReport", new ReportMonthlyCmd());
        commands.put("giveAdminRights", new GiveAdminRightsCmd());
        commands.put("showEmployee", new ShowEmployeeCmd());
        commands.put("dismiss", new TakeAwayAdminRightsCmd());
    }

    /**
     * Method will return command class by key.
     *
     * @param key - key of command
     * @return Object, that implements Command interface
     */
    public Command getCommand(String key) {
        return commands.get(key);
    }
}
