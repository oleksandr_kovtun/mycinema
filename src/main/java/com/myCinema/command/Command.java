package com.myCinema.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Interface, that implements command pattern.
 */
public interface Command {

    /**
     * Method execute business logic and return path to JSP or servlet with another command.
     *
     * @param request - HttpServletRequest, that received from servlet
     * @param response - HttpServletResponse, that received from servlet
     * @return string, that contains path to jsp or servlet with another command
     */
    String execute(HttpServletRequest request, HttpServletResponse response);
}
