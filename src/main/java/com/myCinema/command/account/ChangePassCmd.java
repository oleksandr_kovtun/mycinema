package com.myCinema.command.account;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Account;
import com.myCinema.service.accountService.AccountService;
import com.myCinema.service.accountService.AccountServiceImpl;
import com.myCinema.utility.Encryptor;
import com.myCinema.utility.Validator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static com.myCinema.utility.WebUtil.*;

/**
 * Class change password of current account.
 *
 * @author Oleksandr Kovtun
 */
public class ChangePassCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(ChangePassCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);
        HttpSession session = request.getSession();

        Account acc = (Account) session.getAttribute("account");
        String oldPass = Encryptor.hash(request.getParameter("oldPass"));
        String newPass = Encryptor.hash(request.getParameter("newPass"));
        String repeatPass = Encryptor.hash(request.getParameter("repeatPass"));

        LOGGER.info("Try to change account password...");
        try {
            if (oldPass.equals(acc.getPassword()) && newPass.equals(repeatPass)) {
                if (!Validator.commonFieldIsValid(request.getParameter("newPass"))) {

                    LOGGER.info("New password is invalid! Changing failed.");
                    setTroubleMessage(request, 0);
                    return JspPages.PROFILE.getValue();
                }
                acc.setPassword(newPass);

                AccountService accountService = new AccountServiceImpl();
                acc = accountService.updatePassword(acc);
                session.removeAttribute("account");
                session.setAttribute("account", acc);
                setNotificationMessage(request, 1);
                LOGGER.info("Password successfully changed!");
            } else {
                LOGGER.info("Passwords is not equals! Changing failed.");
                setTroubleMessage(request, 2);
            }
        } catch (DBException e) {
            LOGGER.error("Password changing failed on DAO level!", e);
            setTroubleMessage(request, 1);
            e.printStackTrace();
        }
        return JspPages.PROFILE.getValue();
    }
}
