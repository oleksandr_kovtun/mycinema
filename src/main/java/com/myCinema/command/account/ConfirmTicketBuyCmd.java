package com.myCinema.command.account;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.model.Session;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static com.myCinema.utility.WebUtil.clearSessionMessages;

/**
 * Class generates receipt to confirmation of tickets buying.
 *
 * @author Oleksandr Kovtun
 */
public class ConfirmTicketBuyCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(ConfirmTicketBuyCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);
        HttpSession session = request.getSession();

        LOGGER.info("Try to create form for ticket buying...");
        if (request.getParameter("buySeat") == null) {
            LOGGER.info("No seats selected. Return to 'select seat page'!");
            Session s = (Session) session.getAttribute("selectedSession");
            return "controller?operation=selectSeat&sessionid=" + s.getId();
        }

        String[] seat = request.getParameterValues("buySeat");
        session.removeAttribute("buySeat");
        session.setAttribute("buySeat", seat);
        LOGGER.info("Form for confirm ticket buying created!");

        return JspPages.CONFIRM_BUYING.getValue();
    }
}
