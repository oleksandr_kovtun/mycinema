package com.myCinema.command.account;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.myCinema.utility.WebUtil.clearSessionMessages;

/**
 * Class for showing "change password form".
 *
 * @author Oleksandr Kovtun
 */
public class ShowChangePassFormCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(ShowChangePassFormCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        LOGGER.info("Form for changing password created!");
        request.setAttribute("changePassForm", true);

        return JspPages.PROFILE.getValue();
    }
}
