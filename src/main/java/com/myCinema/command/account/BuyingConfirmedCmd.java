package com.myCinema.command.account;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Account;
import com.myCinema.model.Session;
import com.myCinema.model.Ticket;
import com.myCinema.service.ticketService.TicketService;
import com.myCinema.service.ticketService.TicketServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

import static com.myCinema.utility.WebUtil.*;

/**
 * Class parses input data, and create new tickets to current account.
 * Using TicketService.
 *
 * @author Oleksandr Kovtun
 */
public class BuyingConfirmedCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(BuyingConfirmedCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        HttpSession session = request.getSession();
        List<Ticket> ticketList = new ArrayList<>();

        int accId = ((Account) session.getAttribute("account")).getId();
        int sessionId = (int) session.getAttribute("sesId");
        String[] seat = (String[]) session.getAttribute("buySeat");

        TicketService ticketService = new TicketServiceImpl();
        for (String s : seat) {
            Ticket t = new Ticket(accId, sessionId, Integer.parseInt(s));
            ticketList.add(t);
        }
        try {
            LOGGER.info("Try to add tickets to DB...");
            ticketService.addTickets(ticketList);
            LOGGER.info("Tickets successfully added.");
        } catch (DBException e) {
            LOGGER.info("Tickets adding failed.");
            setTroubleMessage(request, 0);
            return "controller?operation=selectSeat&sessionid=" + ( (Session) request.getSession().getAttribute("selectedSession")).getId();
        }

        return JspPages.SHOW_TICKETS.getValue();
    }
}
