package com.myCinema.command.account;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Account;
import com.myCinema.service.accountService.AccountService;
import com.myCinema.service.accountService.AccountServiceImpl;
import com.myCinema.utility.Validator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.myCinema.utility.WebUtil.*;

/**
 *  Class changes account data.
 *  Using AccountService.
 *
 * @author Oleksandr Kovtun
 */
public class ChangeAccInfoCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(ChangeAccInfoCmd.class);

    /**
     * Can change email, name, surname and phone number of account using AccountService.
     *
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        Account acc = (Account) request.getSession().getAttribute("account");

        LOGGER.info("Try to update account info...");

        if(request.getParameter("changeName") != null && !request.getParameter("chName").equals("")) {
            acc.setName(request.getParameter("chName"));
        } else {
            acc.setName(null);
        }

        if(request.getParameter("changeSurname") != null && !request.getParameter("chSurname").equals("")) {
            acc.setSurname(request.getParameter("chSurname"));
        } else {
            acc.setSurname(null);
        }

        if(request.getParameter("changeNumber") != null
                && Validator.phoneIsValid(request.getParameter("chNumber"))) {

            acc.setPhoneNumber(Validator.validatePhone(request.getParameter("chNumber")));
        } else {
            acc.setPhoneNumber(null);
        }

        if(request.getParameter("changeEmail") != null
                && Validator.emailIsValid(request.getParameter("chEmail"))) {

            acc.setEmail(request.getParameter("chEmail"));
        } else {
            acc.setEmail(null);
        }

        if (acc.getEmail() == null && acc.getPhoneNumber() == null
                && acc.getName() == null && acc.getSurname() == null) {

            LOGGER.info("Details invalid. Updating info failed!");
            setTroubleMessage(request, 4);
            return JspPages.CHANGE_ACC_INFO.getValue();
        }

        AccountService accountService = new AccountServiceImpl();
        try {
            accountService.updateInformation(acc);
            setNotificationMessage(request, 2);
            LOGGER.info("Account info successfully updated.");
        } catch (DBException e) {
            setTroubleMessage(request, 3);
            LOGGER.info("Account info update failed!");
        }

        return JspPages.CHANGE_ACC_INFO.getValue();
    }
}
