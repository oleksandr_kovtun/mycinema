package com.myCinema.command.account;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Account;
import com.myCinema.service.accountService.AccountService;
import com.myCinema.service.accountService.AccountServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.myCinema.utility.WebUtil.*;

/**
 * Class generates new "change profile information form" with actual data.
 *
 * @author Oleksandr Kovtun
 */
public class ShowChangeAccFieldsForm implements Command {

    private static final Logger LOGGER = Logger.getLogger(ShowChangeAccFieldsForm.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.info("Try to create form for change account details...");

        Account acc = (Account) request.getSession().getAttribute("account");
        AccountService accountService = new AccountServiceImpl();
        try {
            acc = accountService.findAccountById(acc.getId() ,true);
            LOGGER.info("Form for changing account details successfully created!");
        } catch (DBException e) {
            setTroubleMessage(request, 3);
            LOGGER.error("Cannot create form. Error on DAO level!", e);
            return JspPages.PROFILE.getValue();
        }
        request.setAttribute("fullAcc", acc);

        return JspPages.PROFILE.getValue();
    }
}
