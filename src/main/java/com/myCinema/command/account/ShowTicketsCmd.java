package com.myCinema.command.account;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Account;
import com.myCinema.model.Ticket;
import com.myCinema.service.ticketService.TicketService;
import com.myCinema.service.ticketService.TicketServiceImpl;
import com.myCinema.utility.DateManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;

import static com.myCinema.utility.WebUtil.clearSessionMessages;

/**
 * Class fills output data with all tickets, that have current account.
 *
 * @author Oleksandr Kovtun
 */
public class ShowTicketsCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(ShowTicketsCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);



        LOGGER.info("Try to show account tickets...");
        HttpSession session = request.getSession();
        Account acc = (Account) session.getAttribute("account");

        String currentDate = DateManager.parseDate(DateManager.currentDateForSql());

        TicketService ticketService = new TicketServiceImpl();
        List<Ticket> tickets;
        List<Ticket> todaySessionTickets;
        List<Ticket> futureSessionTickets;
        List<Ticket> notActualTickets;
        try {
            LOGGER.info("Try to get tickets details from DB...");
            tickets = ticketService.findTicketsByAccountId(acc.getId());
            LOGGER.info("Tickets details successfully received, filter tickets by categories...");

            todaySessionTickets = tickets.stream()
                    .filter(Ticket::getActual)
                    .filter(Ticket -> Ticket.getDate().equals(currentDate))
                    .collect(Collectors.toList());

            futureSessionTickets = tickets.stream()
                    .filter(Ticket::getActual)
                    .filter(Ticket -> !Ticket.getDate().equals(currentDate))
                    .collect(Collectors.toList());

            notActualTickets = tickets.stream()
                    .filter(Ticket -> !Ticket.getActual())
                    .collect(Collectors.toList());
        } catch (DBException e) {
            LOGGER.error("Cannot get tickets details. Error on DAO level!", e);
            return JspPages.DB_ERROR.getValue();
        }

        request.setAttribute("todayTickets", todaySessionTickets);
        request.setAttribute("futureTickets", futureSessionTickets);
        request.setAttribute("notActualTickets", notActualTickets);

        LOGGER.info("Getting account tickets successfully completed!");
        return JspPages.PROFILE.getValue();
    }
}
