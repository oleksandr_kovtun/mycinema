package com.myCinema.command.common;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Account;
import com.myCinema.model.Session;
import com.myCinema.service.accountService.AccountService;
import com.myCinema.service.accountService.AccountServiceImpl;
import com.myCinema.utility.Encryptor;
import org.apache.log4j.Logger;

import static com.myCinema.utility.WebUtil.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The class performs the operation of logging into the account.
 *
 * @author Oleksandr Kovtun
 */
public class LoginCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(LoginCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);
        LOGGER.info("Try to log in...");
        if (request.getParameter("login") == null || request.getParameter("password") == null) {
            LOGGER.info("Password or login is empty. Return to log in page!");
            return JspPages.LOG_IN.getValue();
        }
        String login = request.getParameter("login");
        String pass = Encryptor.hash(request.getParameter("password"));

        AccountService accountService = new AccountServiceImpl();
        Account acc;
        try {
            if (accountService.loginIsFree(login)) {
                LOGGER.info("Login is not exists. Return to log in page!");
                setTroubleMessage(request, 2);
                return JspPages.LOG_IN.getValue();
            }
            acc = accountService.findAccountByLogin(login);
            LOGGER.info("Account with current login founded. Comparing passwords...");
        } catch (DBException e) {
            LOGGER.error("Account not found! Error on DAO level!", e);
            setTroubleMessage(request, 1);
            return JspPages.LOG_IN.getValue();
        }

        if (acc.getPassword().equals(pass)) {
            LOGGER.info("Log in successfully completed!");
            HttpSession session = request.getSession();
            session.setAttribute("account", acc);

            if (session.getAttribute("selectedSession") != null) {
                return "controller?operation=selectSeat&sessionid=" + ( (Session) request.getSession().getAttribute("selectedSession")).getId();
            }
            return JspPages.SHOW_MOVIES.getValue();
        } else {
            LOGGER.info("Entered wrong password. Return to log in page!");
            setTroubleMessage(request, 3);
            return JspPages.LOG_IN.getValue();
        }
    }
}
