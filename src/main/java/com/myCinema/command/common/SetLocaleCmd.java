package com.myCinema.command.common;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class for changing localization.
 *
 * @author Oleksandr Kovtun
 */
public class SetLocaleCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(SetLocaleCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String locale = request.getParameter("loc");
        request.getSession().setAttribute("locale", locale);

        LOGGER.info("Change locale. New locale - " + locale + ".");

        return JspPages.SHOW_MOVIES.getValue();
    }
}
