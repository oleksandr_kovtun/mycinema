package com.myCinema.command.common;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Movie;
import com.myCinema.model.Session;
import com.myCinema.service.movieService.MovieService;
import com.myCinema.service.movieService.MovieServiceImpl;
import com.myCinema.service.sessionService.SessionService;
import com.myCinema.service.sessionService.SessionServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.myCinema.utility.WebUtil.clearSessionMessages;

/**
 * Class fills output data with movie details.
 *
 * @author Oleksandr Kovtun
 */
public class MovieDetailsCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(MovieDetailsCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        LOGGER.info("Try to get movie details...");
        int id = Integer.parseInt(request.getParameter("movieid"));
        MovieService movieService = new MovieServiceImpl();
        SessionService sessionService = new SessionServiceImpl();

        List<Session> sesList = null;
        Movie movie = null;
        try {
            sesList = sessionService.findSessionsByMovieId(id);
            movie = movieService.findMovie(id);
            LOGGER.info("Movie details successfully received");
        } catch (DBException e) {
            LOGGER.error("Getting movie details failed on DAO level!", e);
            return JspPages.DB_ERROR.getValue();
        }
        request.setAttribute("movie", movie);
        request.setAttribute("movieSessions", sesList);

        return JspPages.MOVIE.getValue();
    }
}
