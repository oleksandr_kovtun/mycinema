package com.myCinema.command.common;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Session;
import com.myCinema.service.ticketService.TicketService;
import com.myCinema.service.ticketService.TicketServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Class generates data for creation map of cinema hall with seats.
 *
 * @author Oleksandr Kovtun
 */
public class SelectSeatCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(SelectSeatCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        LOGGER.info("Try to create map of cinema hall with info about seats...");
        int sesId = ((Session) request.getSession().getAttribute("selectedSession")).getId();

        TicketService ticketService = new TicketServiceImpl();
        List<Boolean> seats;
        try {
            seats = ticketService.findSeatsInfoBySessionId(sesId);
            LOGGER.info("Try to create map of cinema hall with info about seats...");
        } catch (DBException e) {
            LOGGER.error("Cannot get info about seats of session! Error on DAO level!", e);
            return JspPages.DB_ERROR.getValue();
        }
        request.setAttribute("seats", seats);

        return JspPages.SELECT_SEAT.getValue();
    }
}
