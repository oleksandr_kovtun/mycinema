package com.myCinema.command.common;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Account;
import com.myCinema.service.accountService.AccountService;
import com.myCinema.service.accountService.AccountServiceImpl;
import com.myCinema.utility.Encryptor;
import org.apache.log4j.Logger;

import static com.myCinema.utility.WebUtil.*;
import static com.myCinema.utility.Validator.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The class performs the operation of creation new account.
 *
 * @author Oleksandr Kovtun
 */
public class RegisterCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(RegisterCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        LOGGER.info("Try to create new account...");
        AccountService accountService = new AccountServiceImpl();

        try {
            if (!accountService.loginIsFree(request.getParameter("login"))) {
                LOGGER.info("Login booked! Return to register page!");
                setTroubleMessage(request, 4);
                return JspPages.REGISTER.getValue();
            }
        } catch (DBException e) {
            LOGGER.error("Cannot check login. Error on DAO level!", e);
            return JspPages.DB_ERROR.getValue();
        }

        if (!request.getParameter("password").equals(request.getParameter("repPassword")) ) {
            LOGGER.info("Passwords not equals. Return to register page!");
            setTroubleMessage(request, 5);
            return JspPages.REGISTER.getValue();
        }

        String login = request.getParameter("login");
        String pass = Encryptor.hash(request.getParameter("password"));
        String email = request.getParameter("email");
        String name = request.getParameter("accName");
        String surname = request.getParameter("surname");
        String phone = request.getParameter("number");

        Account acc = new Account(login, pass, email);
        if (commonFieldIsValid(login) && commonFieldIsValid(pass) && emailIsValid(email)) {

            acc.setName(name);
            acc.setSurname(surname);
            if (phoneIsValid(phone)) {
                acc.setPhoneNumber(validatePhone(phone));
            }
            try {
                acc = accountService.createNewAccount(acc);
                acc.setAccRole("customer");
                LOGGER.info("Account successfully created!");
            } catch (DBException e) {
                LOGGER.error("Account creation failed on DAO level!", e);
                setTroubleMessage(request, 7);
                return JspPages.REGISTER.getValue();
            }
            HttpSession session = request.getSession();
            session.setAttribute("account", acc);
            return JspPages.SHOW_MOVIES.getValue();
        } else {
            LOGGER.info("Details are not valid! Return to register page!");
            setTroubleMessage(request, 6);
            return JspPages.REGISTER.getValue();
        }

    }
}
