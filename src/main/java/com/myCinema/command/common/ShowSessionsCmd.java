package com.myCinema.command.common;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Session;
import com.myCinema.service.sessionService.SessionService;
import com.myCinema.service.sessionService.SessionServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

import static com.myCinema.utility.WebUtil.clearSessionMessages;

/**
 * Class generates output data with sessions with considering pagination and sorting.
 *
 * @author Oleksandr Kovtun
 */
public class ShowSessionsCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(ShowSessionsCmd.class);

    int pages;
    int totalElements;
    int currentPage = 0;
    int beginPageNumber;
    int lastPageNumber;
    List<Integer> pagesList = new ArrayList<>();

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        LOGGER.info("Try to show sessions...");
        int orderByCode = Integer.parseInt(request.getParameter("orderBy"));
        String orderBy;
        HttpSession session = request.getSession();
        session.setAttribute("orderBy", orderByCode);
        if (session.getAttribute("ordered") == null) {
            session.setAttribute("ordered", false);
        }
        boolean ordered = (boolean) session.getAttribute("ordered");
        if (request.getParameter("order") != null) {
            ordered = !ordered;
            session.setAttribute("ordered", ordered);
        }

        orderBy = getOrderByStringByCode(orderByCode, ordered);

        SessionService sessionService = new SessionServiceImpl();
        List<Session> sessions = null;
        int limit = 5;
        if (request.getParameter("objLimit") != null) {
            request.getSession().setAttribute("limit", request.getParameter("objLimit"));
        }
        if (request.getSession().getAttribute("limit") != null) {
            limit = Integer.parseInt((String) request.getSession().getAttribute("limit"));
        }
        LOGGER.info("calculate values for pagination...");
        try {
            LOGGER.info("Try to get sessions details from DB...");
            totalElements = sessionService.findCountOfSessions();
            if (totalElements % limit == 0) {
                pages = totalElements / limit;
            } else {
                pages = (totalElements / limit) + 1;
            }
            currentPage = Integer.parseInt(request.getParameter("page"));
            int offset = currentPage * limit;
            for (int i = 0; i < pages; i++) {
                pagesList.add(i);
            }
            sessions = sessionService.findAllSessions(limit, offset, orderBy, true);
            LOGGER.info("Sessions details successfully received...");
        } catch (DBException e) {
            LOGGER.error("Cannot get sessions details. Error on DAO level!", e);
            return JspPages.DB_ERROR.getValue();
        }

        calculateValuesForPaginationBar(currentPage);

        request.setAttribute("firstNum", beginPageNumber);
        request.setAttribute("lastNum", lastPageNumber);
        request.setAttribute("currentPage", currentPage);
        request.setAttribute("pages", pagesList);
        request.setAttribute("sessions", sessions);
        request.setAttribute("totalPages", pagesList.size());

        LOGGER.info("Sessions details to showing successfully downloaded!");
        return JspPages.SESSIONS.getValue();
    }

    private void calculateValuesForPaginationBar(int currentPage) {
        if (currentPage < 4) {
            beginPageNumber = 0;
            lastPageNumber = 6;
        } else if (currentPage >= pagesList.size() - 4 && pagesList.size() >= 7) {
            beginPageNumber = pagesList.size() - 7;
            lastPageNumber = pagesList.size();
        }else if (currentPage >= pagesList.size() - 4 && pagesList.size() <= 7) {
            beginPageNumber = 0;
            lastPageNumber = pagesList.size();
        } else {
            beginPageNumber = currentPage - 3;
            lastPageNumber = currentPage + 3;
        }
    }

    private String getOrderByStringByCode(int code, boolean order) {
        String result = "date, time";

        if (!order) {
            switch (code) {
                case 1:
                    result = "date, time";
                    LOGGER.info("Do sort by date/time...");
                    break;
                case 2:
                    result = "name, date, time";
                    LOGGER.info("Do sort by movie name...");
                    break;
                case 3:
                    result = "seat_occupied, date, time";
                    LOGGER.info("Do sort by empty seats...");
                    break;
                case 4:
                    result = "cost, date, time";
                    LOGGER.info("Do sort by ticket cost...");
            }
        } else {
            switch (code) {
                case 1:
                    result = "date DESC, time DESC";
                    LOGGER.info("Do sort by date/time (reverse order)...");
                    break;
                case 2:
                    result = "name DESC, date, time";
                    LOGGER.info("Do sort by movie name (reverse order)...");
                    break;
                case 3:
                    result = "seat_occupied DESC, date, time";
                    LOGGER.info("Do sort by empty seats (reverse order)...");
                    break;
                case 4:
                    result = "cost DESC, date, time";
                    LOGGER.info("Do sort by ticket cost (reverse order)...");
            }
        }

        return result;
    }
}
