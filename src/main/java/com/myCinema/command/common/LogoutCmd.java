package com.myCinema.command.common;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The class performs the operation of log out of the account.
 *
 * @author Oleksandr Kovtun
 */
public class LogoutCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(LogoutCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.info("Log out success!");
        HttpSession session = request.getSession();
        session.invalidate();

        request.removeAttribute("operation");
        return JspPages.SHOW_MOVIES.getValue();
    }
}
