package com.myCinema.command.common;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Session;
import com.myCinema.service.sessionService.SessionService;
import com.myCinema.service.sessionService.SessionServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static com.myCinema.utility.WebUtil.clearSessionMessages;

/**
 * Class fills output data with session details.
 *
 * @author Oleksandr Kovtun
 */
public class SessionDetailsCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(SessionDetailsCmd.class);

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        LOGGER.info("Try to get session details...");
        int id = Integer.parseInt(request.getParameter("sessionid"));

        HttpSession session = request.getSession();
        session.removeAttribute("sesId");
        session.setAttribute("sesId", id);

        SessionService sessionService = new SessionServiceImpl();
        Session s = null;
        try {
            s = sessionService.findSessionById(id);
            LOGGER.info("Session details successfully received!");
        } catch (DBException e) {
            LOGGER.info("Cannot get session details. Error on DAO level!");
            return JspPages.DB_ERROR.getValue();
        }
        session.removeAttribute("selectedSession");
        session.setAttribute("selectedSession", s);

        return JspPages.SESSION.getValue();
    }
}
