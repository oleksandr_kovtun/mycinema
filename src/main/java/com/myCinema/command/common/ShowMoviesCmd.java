package com.myCinema.command.common;

import com.myCinema.command.Command;
import com.myCinema.constants.JspPages;
import com.myCinema.exception.DBException;
import com.myCinema.model.Movie;
import com.myCinema.service.movieService.MovieService;
import com.myCinema.service.movieService.MovieServiceImpl;
import com.myCinema.service.sessionService.SessionService;
import com.myCinema.service.sessionService.SessionServiceImpl;
import com.myCinema.utility.Sorter;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static com.myCinema.utility.WebUtil.clearSessionMessages;

/**
 * Class generates output data with movies with considering pagination and sorting.
 *
 * @author Oleksandr Kovtun
 */
public class ShowMoviesCmd implements Command {

    private static final Logger LOGGER = Logger.getLogger(ShowMoviesCmd.class);

    int currentPage;
    int sortCode;
    int limit = 5;
    int pages;
    int firstElementIndex;
    int lastElementIndex;
    int beginPageNumber;
    int lastPageNumber;

    List<Movie> movies;
    List<Movie> sortedMovies = new ArrayList<>();
    List<Integer> pagesList = new ArrayList<>();

    /**
     * See {@link Command#execute(HttpServletRequest, HttpServletResponse)}
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        clearSessionMessages(request);

        LOGGER.info("Try to show movies...");
        MovieService movieService = new MovieServiceImpl();
        SessionService sessionService = new SessionServiceImpl();

        try {
            LOGGER.info("Try to get movie details from DB...");
            movies = movieService.findAllMovies();
            if (movies != null) {
                for (Movie m : movies) {
                    m.setSessionsAvailable(sessionService.findCountOfSessionsByMovieId(m.getId()));
                    if (m.getSessionsAvailable() > 0) {
                        sortedMovies.add(m);
                    }
                }
            }
            LOGGER.info("Movie details successfully received...");
        } catch (DBException e) {
            LOGGER.error("Cannot get movie details! Error on DAO level!", e);
            return JspPages.DB_ERROR.getValue();
        }

        if (request.getParameter("sortCode") != null ) {
            request.getSession().setAttribute("sort", Integer.parseInt(request.getParameter("sortCode")));
        }
        if (request.getSession().getAttribute("sort") == null) {
            request.getSession().setAttribute("sort", 1);
        }
        sortCode = (int) request.getSession().getAttribute("sort");
        sortMoviesByCode();

        LOGGER.info("calculate values for pagination...");

        if (request.getParameter("objLimit") != null) {
            request.getSession().setAttribute("limit", request.getParameter("objLimit"));
        }
        if (request.getSession().getAttribute("limit") != null) {
            limit = Integer.parseInt((String) request.getSession().getAttribute("limit"));
        }

        calculateValuesForPaginationBar(request);
        sortedMovies = sortedMovies.subList(firstElementIndex, lastElementIndex);

        request.setAttribute("firstNum", beginPageNumber);
        request.setAttribute("lastNum", lastPageNumber);
        request.setAttribute("movies", sortedMovies);
        request.setAttribute("currentPage", currentPage);
        request.setAttribute("totalPages", pages);
        request.setAttribute("pages", pagesList);

        LOGGER.info("Movie details to showing successfully downloaded!");
        return JspPages.MOVIES.getValue();
    }

    private void sortMoviesByCode() {
        switch (sortCode) {
            case 1:
                Sorter.sortMovieByName(sortedMovies);
                LOGGER.info("Do sort by movie name...");
                break;
            case 2:
                Sorter.sortMovieByVoiceActing(sortedMovies);
                LOGGER.info("Do sort by voice language...");
                break;
            case 3:
                Sorter.sortMovieByGenre(sortedMovies);
                LOGGER.info("Do sort by movie genre...");
                break;
            case 4:
                Sorter.sortMovieBySessionCount(sortedMovies);
                LOGGER.info("Do sort by session count...");
                break;
        }
    }

    private void calculateValuesForPaginationBar(HttpServletRequest request) {
        if (sortedMovies.size() % limit == 0) {
            pages = sortedMovies.size() / limit;
        } else {
            pages = sortedMovies.size() / limit + 1;
        }

        if (request.getParameter("page") == null) {
            currentPage = 0;
        } else {
            currentPage = Integer.parseInt(request.getParameter("page"));
        }
        firstElementIndex = currentPage * limit;
        lastElementIndex = Math.min((currentPage * limit) + limit, sortedMovies.size());
        for (int i = 0; i < pages; i++) {
            pagesList.add(i);
        }

        if (currentPage < 4) {
            beginPageNumber = 0;
            lastPageNumber = 6;
        } else if (currentPage >= pagesList.size() - 4 && pagesList.size() >= 7) {
            beginPageNumber = pagesList.size() - 7;
            lastPageNumber = pagesList.size();
        }else if (currentPage >= pagesList.size() - 4 && pagesList.size() <= 7) {
            beginPageNumber = 0;
            lastPageNumber = pagesList.size();
        } else {
            beginPageNumber = currentPage - 3;
            lastPageNumber = currentPage + 3;
        }
    }
}
