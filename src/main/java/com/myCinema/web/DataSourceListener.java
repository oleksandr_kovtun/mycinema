package com.myCinema.web;

import com.myCinema.command.common.SessionDetailsCmd;
import com.myCinema.connection.ConnectionPool;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.sql.Connection;
import java.sql.SQLException;

@WebListener
public class DataSourceListener implements ServletContextListener {

    private static final Logger LOGGER = Logger.getLogger(DataSourceListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce)  {

        ConnectionPool cp = ConnectionPool.getInstance();
        Connection c = cp.getConnection();

        LOGGER.info("Database connection established! The app is running...");

        try {
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
