package com.myCinema.web;

import com.myCinema.command.Command;

import com.myCinema.command.CommandContainer;
import org.apache.log4j.Logger;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@MultipartConfig
@WebServlet({"/controller", "/admin/controller"})
public class CinemaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(CinemaServlet.class);

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		LOGGER.debug("Initializing CinemaServlet;");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operation;
		CommandContainer cont = new CommandContainer();
		if (request.getParameter("operation") == null) {
			operation = "showMovies";
		} else {
			operation = request.getParameter("operation");
		}
		LOGGER.debug("Get request, command '" + operation + "';");
		Command cmd = cont.getCommand(operation);
		String page = cmd.execute(request, response);
		request.getRequestDispatcher(page).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String operation = request.getParameter("operation");
		LOGGER.debug("Post request, command '" + operation + "';");
		CommandContainer cont = new CommandContainer();
		Command cmd = cont.getCommand(operation);
		String page = cmd.execute(request, response);
		response.sendRedirect(page);
	}

}
