package com.myCinema.web;

import com.myCinema.model.Account;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Objects;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter("/admin/*")
public class AdminRightsFilter implements Filter {

	private static final Logger LOGGER = Logger.getLogger(AdminRightsFilter.class);

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		Account acc = (Account) req.getSession().getAttribute("account");
		if (acc != null && (Objects.equals(acc.getAccRole(), "admin") || Objects.equals(acc.getAccRole(), "director"))) {
			LOGGER.info("Go to the administration page, account id " + acc.getId() + ";");
			chain.doFilter(request, response);
		} else {
			if (acc == null) {
				LOGGER.info("An attempt to gain unauthorized access to an administrative resource. The user is not authorized;");
			} else {
				LOGGER.info("An attempt to gain unauthorized access to an administrative resource. User id " + acc.getId() + ";");
			}
			HttpServletResponse resp = (HttpServletResponse) response;
			resp.sendRedirect("../CinemaServlet?operation=showMovies");
		}

	}

}
