package com.myCinema.connection;

import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Singleton class for getting connection with data base.
 *
 * @author Oleksandr Kovtun
 */
public class ConnectionPool {


    private static ConnectionPool connectionPool = null;

    private static final Logger LOGGER = Logger.getLogger(ConnectionPool.class);

    /**
     * Private constructor without parameters.
     */
    private ConnectionPool() {
    }

    /**
     * Method will return connection, which was taken from DataSource.
     *
     * @return Connection to data base
     */
    public Connection getConnection() {
        Connection c;
        try {
            Context ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/cinema");
            c = ds.getConnection();
            LOGGER.debug("Getting connection successfully completed!");
        } catch (SQLException | NamingException e) {
            LOGGER.error("Get connection failed!", e);
            throw new IllegalStateException("Get connection failed", e);
        }
        return c;
    }

    /**
     * Method will create new ConnectionPool object, if it is not exists,
     * else return object, which was created early.
     *
     * @return object type ConnectionPool
     */
    public static ConnectionPool getInstance() {
    	synchronized (ConnectionPool.class) {
	    	if (connectionPool == null) {
	            connectionPool = new ConnectionPool();
                LOGGER.info("Connection pool - new instance!");
	        }
    	}
        LOGGER.debug("Connection pool - get object.");
        return connectionPool;
    }

}
