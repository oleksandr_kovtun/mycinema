package com.myCinema.dao.ticketDao;

import com.myCinema.model.Ticket;
import com.myCinema.utility.DBHelper;
import org.apache.log4j.Logger;

import static com.myCinema.constants.SqlQueryConstants.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Basic data access object implementation for MySQL.
 * Provided operations with Ticket object.
 *
 * @author Oleksandr Kovtun
 */
public class MySqlTicketDao implements TicketDao{

    private static final Logger LOGGER = Logger.getLogger(MySqlTicketDao.class);

    /**
     * Method will find all tickets, which was bought by Account.
     * See also {@link TicketDao#findTicketsByAccountId(Connection, int)}
     */
    @Override
    public List<Ticket> findTicketsByAccountId(Connection con, int accountId) throws SQLException {
        List<Ticket> tickets = new ArrayList<>();

        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            stmt = con.prepareStatement(FIND_TICKETS_BY_ACCOUNT_ID.getValue());
            stmt.setInt(1, accountId);
            rs = stmt.executeQuery();
            while (rs.next()) {
                tickets.add(DBHelper.fillTicket(rs));
            }
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }
        return tickets;
    }

    /**
     * Method will find all tickets, which related to session.
     * See also {@link TicketDao#findTicketsBySessionId(Connection, int)}
     */
    @Override
    public List<Ticket> findTicketsBySessionId(Connection con, int sessionId) throws SQLException {
        List<Ticket> tickets = new ArrayList<>();

        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            stmt = con.prepareStatement(FIND_TICKETS_BY_SESSION_ID.getValue());
            stmt.setInt(1, sessionId);
            rs = stmt.executeQuery();
            while (rs.next()) {
                tickets.add(DBHelper.fillMainInfoTicket(rs));
            }
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }
        return tickets;
    }

    /**
     * Method will insert ticket into DB.
     * See also {@link TicketDao#insertTicket(Connection, Ticket)}
     */
    @Override
    public void insertTicket(Connection con, Ticket ticket) throws SQLException {
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(INSERT_TICKET.getValue());
            stmt.setInt(1, ticket.getAccId());
            stmt.setInt(2, ticket.getSessionId());
            stmt.setInt(3, ticket.getSeat());
            stmt.executeUpdate();
        } finally {
            DBHelper.closeStatement(stmt);
        }
    }
}
