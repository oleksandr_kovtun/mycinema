package com.myCinema.dao.ticketDao;

import com.myCinema.model.Ticket;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Basic data access object interface.
 * Provided operations with Ticket object.
 *
 * @author Oleksandr Kovtun
 */
public interface TicketDao {

    /**
     * Method will find all tickets, which was bought by Account.
     *
     * @param con - Connection to DB
     * @param accountId - account id, which will find tickets
     * @return ArrayList of Ticket entities
     * @throws SQLException -
     */
    List<Ticket> findTicketsByAccountId(Connection con, int accountId) throws SQLException;

    /**
     * Method will find all tickets, which related to session.
     *
     * @param con - Connection to DB
     * @param sessionId - session id, which will find tickets
     * @return ArrayList of Ticket entities
     * @throws SQLException -
     */
    List<Ticket> findTicketsBySessionId(Connection con, int sessionId) throws SQLException;

    /**
     * Method will insert ticket into DB.
     *
     * @param con - Connection to DB
     * @param ticket - Ticket entity, which must be added to DB
     * @throws SQLException -
     */
    void insertTicket(Connection con, Ticket ticket) throws SQLException;
}
