package com.myCinema.dao.accountDao;

import com.myCinema.connection.ConnectionPool;
import static com.myCinema.constants.SqlQueryConstants.*;
import com.myCinema.model.Account;
import com.myCinema.utility.DBHelper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Basic data access object implementation for MySQL.
 * Provided operations with Account object.
 *
 * @author Oleksandr Kovtun
 */
public class MySqlAccountDao implements AccountDao{

    /**
     * Finding account entity by id.
     * See also {@link AccountDao#findAccountById(Connection, int)}
     */
    @Override
    public Account findAccountById(Connection con, int id) throws SQLException {

        PreparedStatement stmt = null;
        ResultSet rs = null;
        Account acc = null;
        try {
            stmt = con.prepareStatement(FIND_ACC_BY_ID.getValue());
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                acc = DBHelper.fillAccount(rs);
            }
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }
        return acc;
    }

    /**
     * Finding account full information by id.
     * See also {@link AccountDao#findFullAccountById(Connection, int)}
     */
    @Override
    public Account findFullAccountById(Connection con, int id) throws SQLException {

        Account acc = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            stmt = con.prepareStatement(FIND_FULL_ACC_BY_ID.getValue());
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                acc = DBHelper.fillAccountFullInfo(rs);
            }
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }
        return acc;
    }

    /**
     * Finding account by account login.
     * See also {@link AccountDao#findAccountByLogin(Connection, String)}
     */
    @Override
    public Account findAccountByLogin(Connection con, String login) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Account acc = null;
        try {
            stmt = con.prepareStatement(FIND_ACC_BY_LOGIN.getValue());
            stmt.setString(1, login);
            rs = stmt.executeQuery();
            if (rs.next()) {
                acc = DBHelper.fillAccount(rs);
            }
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }
        return acc;
    }

    /**
     * Insertion account to DB.
     * See also {@link AccountDao#insertAccount(Connection, Account)}
     */
    @Override
    public Account insertAccount(Connection con, Account acc) throws SQLException {

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(INSERT_ACC.getValue(), Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, acc.getLogin());
            stmt.setString(2, acc.getPassword());
            stmt.setString(3, acc.getEmail());
            stmt.executeUpdate();
            rs = stmt.getGeneratedKeys();
            rs.next();
            acc.setId(rs.getInt(1));
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }
        return acc;
    }

    /**
     * Insertion account information to DB.
     * See also {@link AccountDao#insertAccountInfo(Connection, Account)}
     */
    @Override
    public boolean insertAccountInfo(Connection con, Account acc) throws SQLException {

        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(INSERT_ACC_INFO.getValue());

            stmt.setInt(1, acc.getId());
            stmt.setString(2, acc.getName());
            stmt.setString(3, acc.getSurname());
            stmt.setString(4, acc.getPhoneNumber());

            stmt.executeUpdate();
        } finally {
            DBHelper.closeStatement(stmt);
        }
        return true;
    }

    /**
     * Updated password of account.
     * See also {@link AccountDao#updatePassword(Connection, Account)}
     */
    @Override
    public boolean updatePassword(Connection con, Account account) throws SQLException {

        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(UPDATE_PASS.getValue());
            stmt.setString(1, account.getPassword());
            stmt.setInt(2, account.getId());
            stmt.executeUpdate();
        } finally {
            DBHelper.closeStatement(stmt);
        }
        return true;
    }

    /**
     * Updated email of account.
     * See also {@link AccountDao#updateEmail(Connection, Account)}
     */
    @Override
    public boolean updateEmail(Connection con, Account account) throws SQLException {

        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(UPDATE_EMAIL.getValue());
            stmt.setString(1, account.getEmail());
            stmt.setInt(2, account.getId());
            stmt.executeUpdate();
        } finally {
            DBHelper.closeStatement(stmt);
        }
        return true;
    }

    /**
     * Updated name of account.
     * See also {@link AccountDao#updateName(Connection, Account)}
     */
    @Override
    public boolean updateName(Connection con, Account account) throws SQLException {

        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(UPDATE_NAME.getValue());
            stmt.setString(1, account.getName());
            stmt.setInt(2, account.getId());
            stmt.executeUpdate();
        } finally {
            DBHelper.closeStatement(stmt);
        }
        return true;
    }

    /**
     * Updated surname of account.
     * See also {@link AccountDao#updateSurname(Connection, Account)}
     */
    @Override
    public boolean updateSurname(Connection con, Account account) throws SQLException {

        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(UPDATE_SURNAME.getValue());
            stmt.setString(1, account.getSurname());
            stmt.setInt(2, account.getId());
            stmt.executeUpdate();
        } finally {
            DBHelper.closeStatement(stmt);
        }
        return true;
    }

    /**
     * Updated phone number of account.
     * See also {@link AccountDao#updateNumber(Connection, Account)}
     */
    @Override
    public boolean updateNumber(Connection con, Account account) throws SQLException {

        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(UPDATE_NUMBER.getValue());
            stmt.setString(1, account.getPhoneNumber());
            stmt.setInt(2, account.getId());
            stmt.executeUpdate();
        } finally {
            DBHelper.closeStatement(stmt);
        }
        return true;
    }

    /**
     * Updated role of account.
     * See also {@link AccountDao#updateRole(Connection, String, int)}
     */
    @Override
    public boolean updateRole(Connection con, String login, int role) throws SQLException {

        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(UPDATE_ROLE.getValue());
            stmt.setInt(1, role);
            stmt.setString(2, login);
            stmt.executeUpdate();
        } finally {
            DBHelper.closeStatement(stmt);
        }

        return true;
    }

    /**
     * Finding count of accounts with current login.
     * See also {@link AccountDao#findCountByLogin(Connection, String)}
     */
    @Override
    public int findCountByLogin(Connection con, String login) throws SQLException {

        int result;

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(FIND_COUNT_BY_LOGIN.getValue());
            stmt.setString(1, login);
            stmt.executeQuery();
            rs = stmt.getResultSet();
            rs.next();
            result = rs.getInt(1);
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }
        return result;
    }

    @Override
    public List<Account> findAdminAccounts(Connection con) throws SQLException {

        List<Account> result = new ArrayList<>();

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(FIND_ADMIN_ACCOUNTS.getValue());
            rs = stmt.executeQuery();
            while (rs.next()) {
                result.add(DBHelper.fillAccountFullInfo(rs));
            }
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }
        return result;
    }

}
