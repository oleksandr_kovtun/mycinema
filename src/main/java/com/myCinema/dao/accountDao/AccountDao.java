package com.myCinema.dao.accountDao;

import com.myCinema.model.Account;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Basic data access object interface.
 * Provided operations with Account object.
 *
 * @author Oleksandr Kovtun
 */
public interface AccountDao {

    /**
     * Finding account by id.
     * Get id, login, password(encrypt), email and role fields.
     *
     * @param con - Connection with DB
     * @param id - identifier number of account.
     * @return object type Account
     * @throws SQLException -
     */
    Account findAccountById(Connection con, int id) throws SQLException;

    /**
     * Finding account full information by id.
     * Get all fields.
     *
     * @param con - Connection with DB
     * @param id - identifier number of account.
     * @return object type Account
     * @throws SQLException -
     */
    Account findFullAccountById(Connection con, int id) throws SQLException;

    /**
     * Finding account by account login.
     * Get id, login, password(encrypt), email and role fields.
     *
     * @param con - Connection with DB
     * @param login - login of account.
     * @return object type Account
     * @throws SQLException -
     */
    Account findAccountByLogin(Connection con, String login) throws SQLException;

    /**
     * Insertion account to DB.
     *
     * @param con - Connection with DB
     * @param acc - Account entity, that must be inserted
     * @return - same Account with actual id.
     * @throws SQLException -
     */
    Account insertAccount(Connection con, Account acc) throws SQLException;

    /**
     * Insertion account info (name, surname, phone number) to DB.
     *
     * @param con - Connection with DB
     * @param acc - Account entity, that must be inserted
     * @return - true, if fields successfully inserted
     * @throws SQLException -
     */
    boolean insertAccountInfo(Connection con, Account acc) throws SQLException;

    /**
     * Updating password of account.
     *
     * @param con - Connection with DB
     * @param account - Account entity with password, which must be updated.
     * @return - true, if password successfully updated
     * @throws SQLException -
     */
    boolean updatePassword(Connection con, Account account) throws  SQLException;

    /**
     * Updating email of account.
     *
     * @param con - Connection with DB
     * @param account - Account entity with email, which must be updated.
     * @return - true, if email successfully updated
     * @throws SQLException -
     */
    boolean updateEmail(Connection con, Account account) throws SQLException;

    /**
     * Updating name of account.
     *
     * @param con - Connection with DB
     * @param account - Account entity with name, which must be updated.
     * @return - true, if name successfully updated
     * @throws SQLException -
     */
    boolean updateName(Connection con, Account account) throws SQLException;

    /**
     * Updating surname of account.
     *
     * @param con - Connection with DB
     * @param account - Account entity with surname, which must be updated.
     * @return - true, if surname successfully updated
     * @throws SQLException -
     */
    boolean updateSurname(Connection con, Account account) throws SQLException;

    /**
     * Updating phone number of account.
     *
     * @param con - Connection with DB
     * @param account - Account entity with phone number, which must be updated.
     * @return - true, if number successfully updated
     * @throws SQLException -
     */
    boolean updateNumber(Connection con, Account account) throws SQLException;

    /**
     * Updating role of account.
     *
     * @param con - Connection with DB
     * @param login - login of account, which must be updated
     * @param role - role id, which must be added to account
     * @return - true, if role successfully updated
     * @throws SQLException -
     */
    boolean updateRole(Connection con, String login, int role) throws SQLException;

    /**
     * Finding count of accounts with login.
     * May returns 0 or 1 values.
     * Will be used for checking, account with login exists or not.
     *
     * @param con - Connection with DB
     * @param login - login of account, which count must be found.
     * @return - count of accounts with current login (0 or 1)
     * @throws SQLException -
     */
    int findCountByLogin(Connection con, String login) throws SQLException;

    List<Account> findAdminAccounts(Connection con) throws SQLException;
}
