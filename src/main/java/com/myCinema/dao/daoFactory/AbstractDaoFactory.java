package com.myCinema.dao.daoFactory;

/**
 * Abstract DAO Factory. Can be modified in the future.
 */
public abstract class AbstractDaoFactory implements DaoFactory{

    /**
     * This method always return new object MySqlDaoFactory. But it can be modified,
     * if in the future this app will work with another DB.
     *
     * @return new object, which implements DaoFactory interface.
     */
    public static DaoFactory getDaoFactory() {
        return new MySqlDaoFactory();
    }
}
