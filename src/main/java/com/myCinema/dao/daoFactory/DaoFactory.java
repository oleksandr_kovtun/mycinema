package com.myCinema.dao.daoFactory;

import com.myCinema.dao.accountDao.AccountDao;
import com.myCinema.dao.movieDao.MovieDao;
import com.myCinema.dao.sessionDao.SessionDao;
import com.myCinema.dao.ticketDao.TicketDao;

/**
 * DaoFactory has methods, which returns entity DAO.
 */
public interface DaoFactory {

    /**
     * Method for getting account DAO.
     *
     * @return object, which implements AccountDao interface
     */
    AccountDao getAccountDao();

    /**
     * Method for getting movie DAO.
     *
     * @return object, which implements MovieDao interface
     */
    MovieDao getMovieDao();

    /**
     * Method for getting session DAO.
     *
     * @return object, which implements SessionDao interface
     */
    SessionDao getSessionDao();

    /**
     * Method for getting ticket DAO.
     *
     * @return object, which implements TicketDao interface
     */
    TicketDao getTicketDao();

}

