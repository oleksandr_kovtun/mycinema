package com.myCinema.dao.daoFactory;

import com.myCinema.dao.accountDao.AccountDao;
import com.myCinema.dao.accountDao.MySqlAccountDao;
import com.myCinema.dao.movieDao.MySqlMovieDao;
import com.myCinema.dao.movieDao.MovieDao;
import com.myCinema.dao.sessionDao.MySqlSessionDao;
import com.myCinema.dao.sessionDao.SessionDao;
import com.myCinema.dao.ticketDao.MySqlTicketDao;
import com.myCinema.dao.ticketDao.TicketDao;

/**
 * Implementation AccountDao for MySQL DB.
 * Class has methods, which returns entity DAO for MySQL DB.
 */
public class MySqlDaoFactory extends AbstractDaoFactory {

    /**
     * @return MySqlAccountDao, which implements AccountDao interface.
     */
    @Override
    public AccountDao getAccountDao() {
        return new MySqlAccountDao();
    }

    /**
     * @return MySqlMovieDao, which implements MovieDao interface.
     */
    @Override
    public MovieDao getMovieDao() {
        return new MySqlMovieDao();
    }

    /**
     * @return MySqlSessionDao, which implements SessionDao interface.
     */
    @Override
    public SessionDao getSessionDao() {
        return new MySqlSessionDao();
    }

    /**
     * @return MySqlTicketDao, which implements TicketDao interface.
     */
    @Override
    public TicketDao getTicketDao() {
        return new MySqlTicketDao();
    }
}
