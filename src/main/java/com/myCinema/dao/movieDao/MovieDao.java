package com.myCinema.dao.movieDao;

import com.myCinema.model.Movie;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Basic data access object interface.
 * Provided operations with Movie object.
 *
 * @author Oleksandr Kovtun
 */
public interface MovieDao {

    /**
     * Finding all movies.
     *
     * @param connection - Connection with DB
     * @return list of Movie entities
     * @throws SQLException -
     */
    List<Movie> findAllMovies(Connection connection) throws SQLException;

    /**
     * Finding movie by id.
     *
     * @param connection - Connection with DB
     * @param id - identifier number of movie, which must be founded.
     * @return Movie entity with current id
     * @throws SQLException -
     */
    Movie findMovieById(Connection connection, int id) throws SQLException;

    /**
     * Insertion movie in DB.
     *
     * @param connection - Connection with DB
     * @param movie Movie entity, which must be inserted
     * @throws SQLException -
     */
    void insertMovie(Connection connection, Movie movie) throws SQLException;

    /**
     * Deleting movie from DB.
     *
     * @param connection - Connection with DB
     * @param id - identifier number of movie, which must be deleted
     * @throws SQLException -
     */
    void deleteMovieById(Connection connection, int id) throws SQLException;
}
