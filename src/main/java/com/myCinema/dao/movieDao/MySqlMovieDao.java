package com.myCinema.dao.movieDao;

import com.myCinema.model.Movie;
import com.myCinema.utility.DBHelper;
import org.apache.log4j.Logger;

import static com.myCinema.constants.SqlQueryConstants.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Basic data access object implementation for MySQL.
 * Provided operations with Movie object.
 *
 * @author Oleksandr Kovtun
 */
public class MySqlMovieDao implements MovieDao{

    private static final Logger LOGGER = Logger.getLogger(MySqlMovieDao.class);

    /**
     * Finding all movies.
     * See also {@link MovieDao#findAllMovies(Connection)}
     */
    @Override
    public List<Movie> findAllMovies(Connection con) throws SQLException {

        List<Movie> result = new ArrayList<>();

        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            stmt = con.prepareStatement(FIND_ALL_MOVIES.getValue());
            rs = stmt.executeQuery();
            while (rs.next()) {
                result.add(DBHelper.fillMovie(rs));
            }
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }
        return result;
    }

    /**
     * Finding movie by id.
     * See also {@link MovieDao#findMovieById(Connection, int)}
     */
    @Override
    public Movie findMovieById(Connection con, int id) throws SQLException {

        Movie movie = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            stmt = con.prepareStatement(FIND_MOVIE_BY_ID.getValue());
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                movie = DBHelper.fillMovie(rs);
            }
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }
        return movie;
    }

    /**
     * Insertion movie in DB.
     * See also {@link MovieDao#insertMovie(Connection, Movie)}
     */
    @Override
    public void insertMovie(Connection con, Movie movie) throws SQLException {

        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(INSERT_MOVIE.getValue());
            stmt.setString(1, movie.getName());
            stmt.setString(2, movie.getPosterUrl());
            stmt.setString(3, movie.getGenre());
            stmt.setString(4, movie.getVoiceActing());
            stmt.setString(5, movie.getFilmedIn());
            stmt.setInt(6, movie.getDuration());
            stmt.setString(7, movie.getShortDesc());
            stmt.setString(8, movie.getFullDesc());
            stmt.executeUpdate();
        } finally {
            DBHelper.closeStatement(stmt);
        }
    }

    /**
     * Deleting movie from DB.
     * See also {@link MovieDao#deleteMovieById(Connection, int)}
     */
    @Override
    public void deleteMovieById(Connection con, int id) throws SQLException {
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(DELETE_MOVIE_BY_ID.getValue());
            stmt.setInt(1, id);
            stmt.executeUpdate();
        } finally {
            DBHelper.closeStatement(stmt);
        }
    }
}
