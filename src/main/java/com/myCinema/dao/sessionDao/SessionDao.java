package com.myCinema.dao.sessionDao;

import com.myCinema.model.Session;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Basic data access object interface.
 * Provided operations with Session object.
 *
 * @author Oleksandr Kovtun
 */
public interface SessionDao {

    /**
     * Finding session by id.
     *
     * @param connection - Connection with DB
     * @param sessionId - identifier number of session, which must be founded
     * @return Session entity with actual data
     * @throws SQLException -
     */
    Session findSessionById(Connection connection, int sessionId) throws SQLException;

    /**
     * Finding all session.
     *
     * @param connection - Connection with DB
     * @return list of Session entities
     * @throws SQLException -
     */
    List<Session> findAllSessions(Connection connection) throws SQLException;

    /**
     * Finding all session by parts (pagination).
     *
     * @param connection - Connection with DB
     * @param limit - size of result list
     * @param offset - pagination parameter offset
     * @param orderBy - part for SQL query, which indicates sorting
     * @return list of Session entities
     * @throws SQLException -
     */
    List<Session> findAllSessions(Connection connection, int limit, int offset, String orderBy) throws SQLException;

    /**
     * Finding all actual session by parts (pagination).
     * Actual - date equal today date or later.
     *
     * @param connection - Connection with DB
     * @param limit - size of result list
     * @param offset - pagination parameter offset
     * @param orderBy - part for SQL query, which indicates sorting
     * @return list of Session entities
     * @throws SQLException -
     */
    List<Session> findAllActualSessions(Connection connection, int limit, int offset, String orderBy) throws SQLException;

    /**
     * Finding all sessions, which relate to current movie.
     *
     * @param connection - Connection with DB
     * @param movieId - identifier number of movie, which sessions must to contain
     * @return list of Session entities
     * @throws SQLException -
     */
    List<Session> findSessionsByMovieId(Connection connection, int movieId) throws SQLException;

    /**
     * Finding all sessions, which relate to current date.
     *
     * @param connection - Connection with DB
     * @param sessionDate - string, which contains date in format (yyyy-MM-dd)
     * @return list of Session entities
     * @throws SQLException -
     */
    List<Session> findSessionsByDate(Connection connection, String sessionDate) throws SQLException;

    /**
     * Finding all sessions, which relate to current month.
     *
     * @param connection - Connection with DB
     * @param year - year in full format (example 2021)
     * @param month - month number (1 - 12)
     * @return list of Session entities
     * @throws SQLException -
     */
    List<Session> findSessionsByMonth(Connection connection, int year, int month) throws SQLException;

    /**
     * Finding count of actual sessions (date = today date or later).
     *
     * @param connection - Connection with DB
     * @return count of actual sessions
     * @throws SQLException -
     */
    int findCountOfSessions(Connection connection) throws SQLException;

    /**
     * Finding count of actual sessions (date = today date or later),
     * by movie, which sessions must contain.
     *
     * @param connection - Connection with DB
     * @param movieId -identifier number of movie
     * @return count of actual sessions
     * @throws SQLException -
     */
    int findCountOfSessionsByMovieId(Connection connection, int movieId) throws SQLException;

    /**
     * Insertion new session into DB.
     *
     * @param connection - Connection with DB
     * @param movieId - id of movie, which session must to contain
     * @param session - Session entity, which must be inserted
     * @throws SQLException -
     */
    void insertSession(Connection connection, int movieId, Session session) throws SQLException;

    /**
     * Deleting session from DB.
     *
     * @param connection - Connection with DB
     * @param id - id of session, which must be deleted
     * @throws SQLException -
     */
    void deleteSessionById(Connection connection, int id) throws SQLException;

}
