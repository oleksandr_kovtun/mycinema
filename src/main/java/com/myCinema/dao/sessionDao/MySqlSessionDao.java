package com.myCinema.dao.sessionDao;

import com.myCinema.constants.JspPages;
import com.myCinema.model.Session;
import com.myCinema.utility.DBHelper;
import com.myCinema.utility.DateManager;

import static com.myCinema.constants.SqlQueryConstants.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Basic data access object implementation for MySQL.
 * Provided operations with Session object.
 *
 * @author Oleksandr Kovtun
 */
public class MySqlSessionDao implements SessionDao{

    /**
     * Finding session by id.
     * See also {@link SessionDao#findSessionById(Connection, int)}
     */
    @Override
    public Session findSessionById(Connection con, int sessionId) throws SQLException {

        Session session = null;

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(FIND_SESSION_BY_ID.getValue());
            stmt.setInt(1, sessionId);
            rs = stmt.executeQuery();
            if (rs.next()) {
                session = DBHelper.fillSession(rs);
            }
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }

        return session;
    }

    /**
     * Finding all session.
     * See also {@link SessionDao#findAllSessions(Connection)}
     */
    @Override
    public List<Session> findAllSessions(Connection connection) throws SQLException {
        List<Session> result = new ArrayList<>();

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement(FIND_ALL_ACTUAL_SESSIONS.getValue());
            stmt.setString(1, DateManager.currentDateForSql());
            rs = stmt.executeQuery();
            while (rs.next()) {
                result.add(DBHelper.fillSession(rs));
            }
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }
        return result;
    }

    /**
     * Finding all session by parts (pagination).
     * See also {@link SessionDao#findAllSessions(Connection, int, int, String)}
     */
    @Override
    public List<Session> findAllSessions(Connection connection, int limit, int offset, String orderBy) throws SQLException {
        String query = FIND_ALL_SESSIONS_PAGINATION.getValue().replace("^", orderBy);
        List<Session> result = new ArrayList<>();

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement(query);
            stmt.setInt(1, limit);
            stmt.setInt(2, offset);
            rs = stmt.executeQuery();
            while (rs.next()) {
                result.add(DBHelper.fillSession(rs));
            }
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }
        return result;
    }

    /**
     * Finding all actual session by parts (pagination).
     * See also {@link SessionDao#findAllActualSessions(Connection, int, int, String)}
     */
    @Override
    public List<Session> findAllActualSessions(Connection connection, int limit, int offset, String orderBy) throws SQLException {
        String query = FIND_ACTUAL_SESSIONS.getValue().replace("^", orderBy);
        List<Session> result = new ArrayList<>();

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement(query);
            stmt.setString(1, DateManager.currentDateForSql());
            stmt.setInt(2, limit);
            stmt.setInt(3, offset);
            rs = stmt.executeQuery();
            while (rs.next()) {
                result.add(DBHelper.fillSession(rs));
            }
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }
        return result;
    }

    /**
     * Finding all sessions, which relate to current movie.
     * See also {@link SessionDao#findSessionsByMovieId(Connection, int)}
     */
    @Override
    public List<Session> findSessionsByMovieId(Connection con, int movieId) throws SQLException {

        List<Session> result = new ArrayList<>();

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(FIND_SESSIONS_BY_MOVIE_ID.getValue());
            stmt.setInt(1, movieId);
            stmt.setString(2, DateManager.currentDateForSql());
            rs = stmt.executeQuery();
            while (rs.next()) {
                result.add(DBHelper.fillSessionWithoutMovie(rs));
            }
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }
        return result;
    }

    /**
     * Finding all sessions, which relate to current date.
     * See also {@link SessionDao#findSessionsByDate(Connection, String)}
     */
    @Override
    public List<Session> findSessionsByDate(Connection con, String sessionDate) throws SQLException {

        List<Session> result = new ArrayList<>();

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(FIND_SESSIONS_BY_DATE.getValue());
            stmt.setString(1, sessionDate);
            rs = stmt.executeQuery();
            while (rs.next()) {
                result.add(DBHelper.fillSession(rs));
            }
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }
        return result;
    }

    /**
     * Finding all sessions, which relate to current month.
     * See also {@link SessionDao#findSessionsByMonth(Connection, int, int)}
     */
    @Override
    public List<Session> findSessionsByMonth(Connection con, int year, int month) throws SQLException {
        List<Session> result = new ArrayList<>();

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(FIND_SESSIONS_BY_MONTH.getValue());
            stmt.setInt(1, year);
            stmt.setInt(2, month);
            rs = stmt.executeQuery();
            while (rs.next()) {
                result.add(DBHelper.fillSession(rs));
            }
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }
        return result;
    }

    /**
     * Finding count of actual sessions (date = today date or later).
     * See also {@link SessionDao#findCountOfSessions(Connection)}
     */
    @Override
    public int findCountOfSessions(Connection con) throws SQLException {

        int result;

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(FIND_COUNT_OF_SESSIONS.getValue());
            stmt.setString(1, DateManager.currentDateForSql());
            rs = stmt.executeQuery();
            rs.next();
            result = rs.getInt(1);
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }
        return result;
    }

    /**
     * Finding count of actual sessions (date = today date or later), 
     * by movie, which sessions must contain.
     * See also {@link SessionDao#findCountOfSessionsByMovieId(Connection, int)}
     */
    @Override
    public int findCountOfSessionsByMovieId(Connection con, int movieId) throws SQLException {

        int result;

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(FIND_COUNT_OF_SESSIONS_BY_MOVIE_ID.getValue());
            stmt.setString(1, DateManager.currentDateForSql());
            stmt.setInt(2, movieId);
            rs = stmt.executeQuery();
            rs.next();
            result = rs.getInt(1);
        } finally {
            DBHelper.closeResultSet(rs);
            DBHelper.closeStatement(stmt);
        }
        return result;
    }

    /**
     * Insertion new session into DB.
     * See also {@link SessionDao#insertSession(Connection, int, Session)}
     */
    @Override
    public void insertSession(Connection con, int movieId, Session session) throws SQLException {

        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(INSERT_SESSION.getValue());
            stmt.setInt(1, movieId);
            stmt.setString(2, session.getDate());
            stmt.setString(3, session.getTime());
            stmt.setInt(4, session.getTicketCost());
            stmt.executeUpdate();
        } finally {
            DBHelper.closeStatement(stmt);
        }
    }

    /**
     * Deleting session from DB.
     * See also {@link SessionDao#deleteSessionById(Connection, int)}
     */
    @Override
    public void deleteSessionById(Connection con, int id) throws SQLException {

        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(DELETE_SESSION_BY_ID.getValue());
            stmt.setInt(1, id);
            stmt.executeUpdate();
        } finally {
            DBHelper.closeStatement(stmt);
        }
    }
}
