package com.myCinema.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyTagHandler extends SimpleTagSupport {

    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();

        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm, dd.MM.yyyy");
        String currentDateTime = dateFormat.format(currentDate);

        out.print(currentDateTime);
    }

}
