
  -- -----------------------------------------------------
-- Fill table 'account_role' 
-- -----------------------------------------------------

INSERT account_role(id, role) VALUES (1, 'director'), (2, 'admin'), (3, 'customer');
   
-- -----------------------------------------------------
-- Fill table 'account'
-- -----------------------------------------------------
    
	INSERT account(login, password, email, role_id) VALUES ('boss', 'CEB8447CC4AB78D2EC34CD9F11E4BED2', 'bigBoss@cinema.com', 1);
INSERT account_info(account_id) SELECT id FROM account WHERE login='boss';
	INSERT account(login, password, email, role_id) VALUES ('admin', '21232F297A57A5A743894A0E4A801FC3', 'admin@cinema.com', 2);
INSERT account_info(account_id) SELECT id FROM account WHERE login='admin';
	INSERT account(login, password, email, role_id) VALUES ('ivan', '2C42E5CF1CDBAFEA04ED267018EF1511', 'ivan@email.com', 3);    
INSERT account_info(account_id) SELECT id FROM account WHERE login='ivan';
	INSERT account(login, password, email, role_id) VALUES ('petr', '2F0714F5365318775C8F50D720A307DC', 'petr@email.com', 3);    
INSERT account_info(account_id) SELECT id FROM account WHERE login='petr';
	INSERT account(login, password, email, role_id) VALUES ('jack', '4FF9FC6E4E5D5F590C4F2134A8CC96D1', 'jack@email.com', 3);    
INSERT account_info(account_id) SELECT id FROM account WHERE login='jack';
	INSERT account(login, password, email, role_id) VALUES ('bobby', 'A9C4CEF5735770E657B7C25B9DCB807B', 'bobby@email.com', 3);    
INSERT account_info(account_id) SELECT id FROM account WHERE login='bobby';
	INSERT account(login, password, email, role_id) VALUES ('bigfan', 'B26DC49D7A46BA40ABA8A889FB878671', 'bigfan@email.com', 3);    
INSERT account_info(account_id) SELECT id FROM account WHERE login='bigfan';

-- -----------------------------------------------------
-- Update table 'account_info'
-- -----------------------------------------------------

UPDATE account_info SET name='Quentin', surname='Tarantino', phone_number='+38067000000' WHERE account_id=(SELECT id FROM account WHERE login='boss');
UPDATE account_info SET name='Leonardo', surname='Dicaprio', phone_number='+38093111111' WHERE account_id=(SELECT id FROM account WHERE login='admin');
UPDATE account_info SET name='Rupert', surname='Grin' WHERE account_id=(SELECT id FROM account WHERE login='bigfan');

-- -----------------------------------------------------
-- Fill table 'movie'
-- -----------------------------------------------------

INSERT movie(name, poster, genre, voice_acting, filmed_in, duration, short_desc, description) 
	VALUES
     ('Terminator', 'Terminator.jpg', 'action', 'english', 'USA', 98,
     'The Terminator is a 1984 American science fiction film directed by James Cameron. It stars Arnold Schwarzenegger as the Terminator, a cyborg assassin sent back in time from 2029 to 1984 to kill Sarah Connor (Linda Hamilton), whose son will one day become a savior against machines in a post-apocalyptic future. Michael Biehn plays Kyle Reese, a soldier sent back in time to protect Sarah. The screenplay is credited to Cameron and producer Gale Anne Hurd, while co-writer William Wisher Jr. received a credit for additional dialogue. Executive producers John Daly and Derek Gibson of Hemdale Film Corporation were instrumental in financing and production.',
     '<p>In 1984 Los Angeles, a cyborg assassin known as a Terminator arrives from 2029, kills four people and steals guns and clothes. Kyle Reese, a human soldier sent back in time from the same year, arrives shortly afterwards. The Terminator begins systematically killing women named Sarah Connor, whose addresses it finds in the telephone directory. It tracks the last Sarah Connor to a nightclub, but Kyle rescues her. They steal a car and escape with the Terminator pursuing in a police car.
	 <p>As they hide in a parking lot, Kyle explains to Sarah that an artificial intelligence defense network, Skynet, created by Cyberdyne Systems, will become self-aware in the near future and initiate a nuclear holocaust. Sarahs future son John will rally the survivors and lead a resistance movement against Skynet and its army of machines. With the Resistance on the verge of victory, Skynet sent a Terminator back in time to kill Sarah before John is born to prevent the formation of the Resistance. The Terminator, a Cyberdyne Systems Model 101, is an unstoppable killing machine with a powerful metal endoskeleton and an external layer of living tissue that makes it appear human. When Sarah asks if Kyle can destroy the cyborg, he replies that he is uncertain.'),
   
   ('Pulp Fiction', 'PulpFiction.jpg', 'drama', 'english', 'USA', 128,
   'Pulp Fiction is a 1994 American neo-noir black comedy crime film written and directed by Quentin Tarantino, who conceived it with Roger Avary.[4] Starring John Travolta, Samuel L. Jackson, Bruce Willis, Tim Roth, Ving Rhames, and Uma Thurman, it tells several stories of criminal Los Angeles. The title refers to the pulp magazines and hardboiled crime novels popular during the mid-20th century, known for their graphic violence and punchy dialogue.',
   '<p>Hitmen Jules Winnfield and Vincent Vega arrive at an apartment to retrieve a briefcase for their boss, gangster Marsellus Wallace, from a business partner, Brett. After Vincent checks the contents of the briefcase, Jules shoots one of Bretts associates, then declaims a passage from the Bible before he and Vincent kill Brett for trying to double-cross Marsellus. They take the briefcase to Marsellus, but they have to wait while he bribes champion boxer Butch Coolidge to take a dive in his upcoming match.
	<p>The next day, Vincent purchases heroin from his drug dealer, Lance. He shoots up and then drives to meet Marselluss wife Mia, whom he had agreed to escort while Marsellus was out of town. They eat at a 1950s-themed restaurant and participate in a twist contest, then return home with the trophy. While Vincent is in the bathroom, Mia finds his heroin, mistakes it for cocaine, snorts it, and overdoses. Vincent rushes her to Lances house, where they revive her with an adrenaline shot to her heart.'),

   ('Назад в будущее', 'НазадВБудущее.jpg', 'fantasy', 'russian', 'USA', 116,
   'Подросток Марти с помощью машины времени, сооруженной его другом профессором доком Брауном, попадает из 80-х в далекие 50-е. Там он встречается со своими будущими родителями, еще подростками, и другом-профессором, совсем молодым.',
   '<p>Старшеклассник Марти Макфлай с помощью машины времени, которую создал его друг "Док" Браун, попадает из своих 80-х в далекие 50-е. Там Марти встречается со своими будущими родителями, еще подростками, и со своим другом-профессором, совсем молодым. Он нарушает естественный ход событий прошлого, чем вызывает массу смешных и драматических ситуаций. Собственная смекалка и хитроумная фантазия профессора-изобретателя помогают Марти преодолеть все препятствия и благополучно вернуться в свое время.'),
   
   ('Back to the Future', 'BackToTheFuture.png', 'fantasy', 'english', 'USA', 116,
   'Back to the Future is an American science fiction/comedy movie directed by Robert Zemeckis and released in 1985. It is about a young man named Marty McFly who accidentally travels into the past and jeopardizes his own future existence.',
   '<p>Marty McFlys life is a dump. His father, George, is constantly bullied by his supervisor Biff Tannen and his mother, Lorraine, is an overweight alcoholic. One day, Marty gets a call from his scientist friend Dr. "Doc" Emmet Brown telling Marty to meet him at Twin Pines Mall at 1:15 AM where Doc unveils a time machine that runs off of plutonium built into a DeLorean and demonstrates it to Marty. Marty accidentally activates the time machine, sending him back to 1955 where he accidentally gets in the way of his teenage parents meeting. Marty must find a way to convince Doc that he is from the future, reunite his parents, and ultimately get back to the future.'),

   ('Бойцовский клуб', 'FightClub.jpg', 'thriller', 'russian', 'USA', 139,
   'Терзаемый хронической бессонницей и отчаянно пытающийся вырваться из мучительно скучной жизни клерк встречает некоего Тайлера Дардена, харизматического торговца мылом с извращенной философией. Тайлер уверен, что самосовершенствование — удел слабых, а саморазрушение — единственное, ради чего стоит жить.',
   '<p>Сотрудник страховой компании страдает хронической бессонницей и отчаянно пытается вырваться из мучительно скучной жизни. Однажды в очередной командировке он встречает некоего Тайлера Дёрдена — харизматического торговца мылом с извращенной философией. Тайлер уверен, что самосовершенствование — удел слабых, а единственное, ради чего стоит жить — саморазрушение. Проходит немного времени, и вот уже новые друзья лупят друг друга почем зря на стоянке перед баром, и очищающий мордобой доставляет им высшее блаженство. Приобщая других мужчин к простым радостям физической жестокости, они основывают тайный Бойцовский клуб, который начинает пользоваться невероятной популярностью.'),
   
   ('Harry Potter and the Philosophers Stone', 'HarryPotter1.jpg', 'adventure', 'english', 'Britain', 152,
   'Harry Potter and the Philosophers Stone is an enthralling start to Harrys journey toward coming to terms with his past and facing his future. It was the first book written by Rowling, and she was praised for creating well-rounded characters and a fully realized wizard universe that coexisted with the present world.',
   '<p>Harry Potter is an average bespectacled eleven-year-old boy who has lived with the Dursley family ever since his parents died in a car crash. For some reason, the family has always mistreated him. On his eleventh birthday, a giant man named Rubeus Hagrid (Robbie Coltrane) hands him a letter telling him that he has been accepted as a student at the Hogwarts School of Witchcraft and Wizardry. Harry learns that his parents were wizards and were killed by the evil wizard Voldemort (Richard Bremmer), a truth that was hidden from him all these years. He embarks for his new life as a student, gathering two good friends Ron Weasley (Rupert Grint) and Hermione Granger (Emma Watson) along the way. They soon learn that something very valuable is hidden somewhere inside the school and Voldemort is very anxious to lay his hands on it.'),
   
	('Shrek', 'Shrek.jpg', 'comedy', 'english', 'USA', 95,
   'Shrek! Shrek is a 2001 American computer-animated comedy film loosely based on the 1990 fairy tale picture book of the same name by William Steig. ... In the story, an ogre called Shrek (Myers) finds his swamp overrun by fairy tale creatures who have been banished by the corrupt Lord Farquaad (Lithgow) aspiring to be king.',
   '<p>In a faraway land called Duloc, a heartless ruler, the diminutive Lord Farquaad, has banished all the fairy tale beings from the land so it can be as boring as he is. But there are three characters who will stand in his way. The first is a green, smelly ogre with a heart of gold named Shrek, his faithful steed, Donkey, who will do anything but shut up, and the beautiful but tough Princess Fiona whom Lord Farquaad wishes to make his wife so he can become king of Duloc. Whats to do in a screwy fairy tale like this?'),
 
	('007: Спектр', 'Spectre.jpg', 'action', 'russian', 'Britain', 135,
   'Зашифрованное послание из неизвестного источника выводит Бонда на след зловещей глобальной организации под кодовым названием СПЕКТР, в то время как М пытается спасти секретную разведывательную службу от ликвидации.',
   '<p>Джеймс Бонд получает загадочное послание из прошлого. Он отправляется на миссию в Мехико, а затем в Рим, где встречает Лючию - прекрасную вдову печально известного преступника. С ней лучше не связываться. Бонду удается проникнуть на тайную встречу и обнаружить, что существует глобальная организация СПЕКТР, которая занимается темными делами. В это же время в Лондоне, Макс Денби, новый глава Центра национальной безопасности, пытается понять действия Джеймса Бонда и ставит под вопрос актуальность существования МИ6. Шпиону Ее Величества удается заручиться поддержкой Мэнипенни и Кью, чтобы вместе найти дочь его старого врага, Мадлен Суонн. Именно она может стать ключом к разгадке СПЕКТРа.'),

	('Ford против Ferrari', 'FordVsFerrari.jpg', 'biography', 'russian', 'USA', 152,
   'Сюжет повествует о группе американских инженеров и дизайнеров. В 1960-х под руководством конструктора Кэрролла Шелби и при поддержке британского гонщика Кена Майлса они должны были с нуля сделать абсолютно новый спорткар, способный опередить Феррари — непобедимого чемпиона 24-часовой гонки на выносливость Ле-Ман.',
   '<p>Фильм Форд против Феррари представляет собой спортивную биографическую драму о противостоянии настоящих королей скоростного моторного дела - корпораций Генри Форде и его оппонента по части невероятной скорости в этом автомобильном деле Энцо Феррари. В центре истории взаимоотношения амбициозного конструктора и профессионального гонщика, чья цель сделать вместе лучший спорткар всех времён, способный конкурировать даже с доминирующими лидерами рынка стремительных гоночных машин.'),

	('Тарас Бульба', 'TarasBulba.jpg', 'history', 'ukrainian', 'Ukraine', 127,
   'Україна, кінець XVI століття[2]. Події фільму відбуваються у важкий період в історії козацтва, коли запорожці піднялися на боротьбу проти Речі Посполитої. У центрі подій опинилася родина поважного козака Тараса Бульби, який переживає глибоку особисту драму. Його син Андрій покохав дівчину з протилежного табору — польську аристократку і виступив з шаблею проти батька і всього козацтва. Тарас, який розривається між почуттями та обов\'язком, оголошує синові останню батьківську волю…',
   '<p>За мотивами одного з навідоміших творів Миколи Гоголя. Події фільму відсилають до важкого періоду в історії козацтва, коли запоріжці піднялися на боротьбу з Річчю Посполитою. В самому центрі політичних інтриг опинилася родина поважного козака Тараса Бульби, який переживає глибоку особисту драму. Його син Андрій покохав польську аристократку і хоче втекти з нею із Січі. Тарас, який розривається між почуттями та обов\'язком, оголошує синові останню батьківську волю...');

-- -----------------------------------------------------
-- Fill table 'session'
-- -----------------------------------------------------

INSERT session(movie_id, date, time, cost) 
	VALUES
    ((SELECT id FROM movie WHERE name='Terminator'), 20210215, 160000, 80),
    ((SELECT id FROM movie WHERE name='Terminator'), 20210215, 220000, 90),
    ((SELECT id FROM movie WHERE name='Terminator'), 20210217, 160000, 80),
    ((SELECT id FROM movie WHERE name='Terminator'), 20210217, 220000, 90),
	((SELECT id FROM movie WHERE name='Shrek'), 20210216, 120000, 80),
    ((SELECT id FROM movie WHERE name='Shrek'), 20210217, 120000, 90),
    ((SELECT id FROM movie WHERE name='Terminator'), 20210220, 160000, 80),
    ((SELECT id FROM movie WHERE name='Terminator'), 20210220, 220000, 90),
    ((SELECT id FROM movie WHERE name='Pulp Fiction'), 20210216, 160000, 95),
    ((SELECT id FROM movie WHERE name='Pulp Fiction'), 20210216, 220000, 110),
    ((SELECT id FROM movie WHERE name='Pulp Fiction'), 20210218, 160000, 95),
    ((SELECT id FROM movie WHERE name='Pulp Fiction'), 20210218, 220000, 110),
    ((SELECT id FROM movie WHERE name='Pulp Fiction'), 20210219, 160000, 95),
    ((SELECT id FROM movie WHERE name='Pulp Fiction'), 20210219, 220000, 110),
    ((SELECT id FROM movie WHERE name='Назад в будущее'), 20210219, 140000, 110),
    ((SELECT id FROM movie WHERE name='Назад в будущее'), 20210218, 140000, 110),
    ((SELECT id FROM movie WHERE name='Back to the Future'), 20210219, 120000, 100),
    ((SELECT id FROM movie WHERE name='Harry Potter and the Philosophers Stone'), 20210219, 100000, 80),
    ((SELECT id FROM movie WHERE name='Harry Potter and the Philosophers Stone'), 20210217, 100000, 80),
    ((SELECT id FROM movie WHERE name='Бойцовский клуб'), 20210219, 180000, 100),
    ((SELECT id FROM movie WHERE name='Бойцовский клуб'), 20210218, 180000, 100),
    ((SELECT id FROM movie WHERE name='Back to the Future'), 20210218, 120000, 100),
	((SELECT id FROM movie WHERE name='007: Спектр'), 20210215, 180000, 120),
	((SELECT id FROM movie WHERE name='007: Спектр'), 20210216, 100000, 100),
    ((SELECT id FROM movie WHERE name='Ford против Ferrari'), 20210220, 100000, 115),
    ((SELECT id FROM movie WHERE name='Ford против Ferrari'), 20210219, 200000, 135),
    
    ((SELECT id FROM movie WHERE name='Тарас Бульба'), 20210222, 120000, 110),
    ((SELECT id FROM movie WHERE name='Тарас Бульба'), 20210224, 120000, 110),
    ((SELECT id FROM movie WHERE name='Тарас Бульба'), 20210226, 120000, 110),
    ((SELECT id FROM movie WHERE name='Тарас Бульба'), 20210228, 120000, 130),
    ((SELECT id FROM movie WHERE name='Тарас Бульба'), 20210302, 120000, 110),
    ((SELECT id FROM movie WHERE name='Тарас Бульба'), 20210304, 120000, 110),
    ((SELECT id FROM movie WHERE name='Тарас Бульба'), 20210223, 120000, 110),
    ((SELECT id FROM movie WHERE name='Тарас Бульба'), 20210225, 120000, 110),
    ((SELECT id FROM movie WHERE name='Тарас Бульба'), 20210227, 120000, 130),
    ((SELECT id FROM movie WHERE name='Тарас Бульба'), 20210301, 120000, 130),
    ((SELECT id FROM movie WHERE name='Тарас Бульба'), 20210303, 120000, 110),
    ((SELECT id FROM movie WHERE name='Тарас Бульба'), 20210305, 120000, 110),
    
    ((SELECT id FROM movie WHERE name='Shrek'), 20210222, 100000, 90),
    ((SELECT id FROM movie WHERE name='Shrek'), 20210224, 100000, 90),
    ((SELECT id FROM movie WHERE name='Shrek'), 20210226, 100000, 90),
    ((SELECT id FROM movie WHERE name='Shrek'), 20210228, 100000, 100),
    ((SELECT id FROM movie WHERE name='Shrek'), 20210302, 100000, 90),
    ((SELECT id FROM movie WHERE name='Shrek'), 20210304, 100000, 90),
    ((SELECT id FROM movie WHERE name='Shrek'), 20210223, 100000, 90),
    ((SELECT id FROM movie WHERE name='Shrek'), 20210225, 100000, 90),
    ((SELECT id FROM movie WHERE name='Shrek'), 20210227, 100000, 90),
    ((SELECT id FROM movie WHERE name='Shrek'), 20210301, 100000, 100),
    ((SELECT id FROM movie WHERE name='Shrek'), 20210303, 100000, 90),
    ((SELECT id FROM movie WHERE name='Shrek'), 20210305, 100000, 90),
    
    ((SELECT id FROM movie WHERE name='Pulp Fiction'), 20210223, 183000, 150),
    ((SELECT id FROM movie WHERE name='Pulp Fiction'), 20210225, 183000, 150),
    ((SELECT id FROM movie WHERE name='Pulp Fiction'), 20210227, 183000, 185),
    ((SELECT id FROM movie WHERE name='Pulp Fiction'), 20210301, 183000, 150),
    ((SELECT id FROM movie WHERE name='Pulp Fiction'), 20210303, 183000, 150),
    ((SELECT id FROM movie WHERE name='Pulp Fiction'), 20210305, 183000, 150),
    
    ((SELECT id FROM movie WHERE name='Back to the Future'), 20210223, 160000, 130),
    ((SELECT id FROM movie WHERE name='Back to the Future'), 20210225, 160000, 130),
    ((SELECT id FROM movie WHERE name='Back to the Future'), 20210227, 160000, 160),
    ((SELECT id FROM movie WHERE name='Back to the Future'), 20210301, 160000, 130),
    ((SELECT id FROM movie WHERE name='Back to the Future'), 20210303, 160000, 130),
    ((SELECT id FROM movie WHERE name='Back to the Future'), 20210305, 160000, 130),
    
    ((SELECT id FROM movie WHERE name='Ford против Ferrari'), 20210223, 210000, 150),
    ((SELECT id FROM movie WHERE name='Ford против Ferrari'), 20210225, 210000, 150),
    ((SELECT id FROM movie WHERE name='Ford против Ferrari'), 20210227, 210000, 185),
    ((SELECT id FROM movie WHERE name='Ford против Ferrari'), 20210301, 210000, 150),
    ((SELECT id FROM movie WHERE name='Ford против Ferrari'), 20210303, 210000, 150),
    ((SELECT id FROM movie WHERE name='Ford против Ferrari'), 20210305, 210000, 150),
    
    ((SELECT id FROM movie WHERE name='Harry Potter and the Philosophers Stone'), 20210222, 210000, 100),
    ((SELECT id FROM movie WHERE name='Harry Potter and the Philosophers Stone'), 20210224, 210000, 100),
    ((SELECT id FROM movie WHERE name='Harry Potter and the Philosophers Stone'), 20210226, 210000, 100),
    ((SELECT id FROM movie WHERE name='Harry Potter and the Philosophers Stone'), 20210228, 210000, 130),
    ((SELECT id FROM movie WHERE name='Harry Potter and the Philosophers Stone'), 20210302, 210000, 100),
    ((SELECT id FROM movie WHERE name='Harry Potter and the Philosophers Stone'), 20210304, 210000, 100),
    
    ((SELECT id FROM movie WHERE name='Бойцовский клуб'), 20210222, 183000, 120),
    ((SELECT id FROM movie WHERE name='Бойцовский клуб'), 20210224, 183000, 120),
    ((SELECT id FROM movie WHERE name='Бойцовский клуб'), 20210226, 183000, 120),
    ((SELECT id FROM movie WHERE name='Бойцовский клуб'), 20210228, 183000, 150),
    ((SELECT id FROM movie WHERE name='Бойцовский клуб'), 20210302, 183000, 120),
    ((SELECT id FROM movie WHERE name='Бойцовский клуб'), 20210304, 183000, 120),
    
	((SELECT id FROM movie WHERE name='007: Спектр'), 20210222, 160000, 110),
    ((SELECT id FROM movie WHERE name='007: Спектр'), 20210224, 160000, 110),
    ((SELECT id FROM movie WHERE name='007: Спектр'), 20210226, 160000, 110),
    ((SELECT id FROM movie WHERE name='007: Спектр'), 20210228, 160000, 130),
    ((SELECT id FROM movie WHERE name='007: Спектр'), 20210302, 160000, 110),
    ((SELECT id FROM movie WHERE name='007: Спектр'), 20210304, 160000, 110),
    
    ((SELECT id FROM movie WHERE name='Назад в будущее'), 20210222, 140000, 130),
    ((SELECT id FROM movie WHERE name='Назад в будущее'), 20210224, 140000, 130),
    ((SELECT id FROM movie WHERE name='Назад в будущее'), 20210226, 140000, 130),
    ((SELECT id FROM movie WHERE name='Назад в будущее'), 20210228, 140000, 160),
    ((SELECT id FROM movie WHERE name='Назад в будущее'), 20210302, 140000, 130),
    ((SELECT id FROM movie WHERE name='Назад в будущее'), 20210304, 140000, 130),
    
    ((SELECT id FROM movie WHERE name='Terminator'), 20210222, 132000, 125),
    ((SELECT id FROM movie WHERE name='Terminator'), 20210224, 132000, 125),
    ((SELECT id FROM movie WHERE name='Terminator'), 20210226, 132000, 125),
    ((SELECT id FROM movie WHERE name='Terminator'), 20210228, 132000, 150),
    ((SELECT id FROM movie WHERE name='Terminator'), 20210302, 132000, 125),
    ((SELECT id FROM movie WHERE name='Terminator'), 20210304, 132000, 125);

    
    -- -----------------------------------------------------
-- Fill table 'account_has_session
-- -----------------------------------------------------
 
